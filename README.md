# README #

### What is this repository for? ###

* Quick summary

This repository contains source code of the Audits system.
The main functions of the system are:
- Management of Audit process on production sites;
- Management of Check-lists for audits;
- Management of users' roles and permissions;
- Email notification of the users on special occasions.

The main objectives of the Audits' system are:
- Reduction of losses when making / agreeing documents on paper, on purpose;
- Creation of a functionally complete mechanism for preparing, coordinating and storing audits' related documents / reports (when integrating with the SAP HR directory);
- Ensuring the completeness, reliability and timeliness of information for making managerial decisions;
- Increasing the efficiency of staff.

### How do I get set up? ###

* Requirements
- Visual Studio 2013+ with features for web development;
- .NET Framework 4.5.1.

### Contribution guidelines ###

* Writing tests
Source code should be accompanied by unit tests.

* Code review
Each push to the repository should be followed up by code review.