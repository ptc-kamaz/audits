using System;
using System.Collections.Generic;

namespace Audits.Models.Checklists
{
    internal static class ChecklistExportCache
    {
        private static readonly Dictionary<string, Record> _cache;

        static ChecklistExportCache()
        {
            _cache = new Dictionary<string, Record>();
        }

        public static byte[] Find(string checklistName)
        {
            lock (_cache)
            {
                Record record;
                if (!_cache.TryGetValue(checklistName, out record))
                {
                    return null;
                }

                var timeSpan = DateTime.Now - record.Timestamp;
                if (timeSpan.Minutes < 30)
                {
                    return record.Data;
                }

                _cache.Remove(checklistName);

                return null;
            }
        }

        public static void UpdateRecord(string checklistName, byte[] data)
        {
            lock (_cache)
            {
                InvalidateRecord(checklistName);

                _cache.Add(checklistName, new Record(data));
            }
        }

        public static void InvalidateRecord(string checklistName)
        {
            lock (_cache)
            {
                Record record;
                if (_cache.TryGetValue(checklistName, out record))
                {
                    _cache.Remove(checklistName);
                }
            }
        }

        private class Record
        {
            public Record(byte[] data)
            {
                Timestamp = DateTime.Now;
                Data = data;
            }

            public DateTime Timestamp { get; }
            public byte[] Data { get; }
        }
    }
}