﻿using System.Data.Entity;
using System.Linq;
using Audits.Models.Database_Manager;

namespace Audits.Models.Checklists
{
    public static class ChecklistService
    {
        public static int Create(Checklist checklist, DatabaseEntities db)
        {
            db.Checklists.Add(checklist);
            db.SaveChanges();
            return checklist.ID;
        }

        public static int Update(Checklist checklist, DatabaseEntities db)
        {
            var dbChecklist = db.Checklists.Find(checklist.ID);

            if (dbChecklist == null)
            {
                return -1;
            }

            ChecklistExportCache.InvalidateRecord(checklist.Name);

            dbChecklist.Name = checklist.Name;

            foreach (var newSection in checklist.Sections.Where(s => s.ID == 0 && s.Parameters.Count > 0).ToList())
            {
                newSection.ChecklistID = dbChecklist.ID;
                dbChecklist.Sections.Add(newSection);
            }

            foreach (var section in checklist.Sections.Where(s => s.ID > 0))
            {
                foreach (var parameter in section.Parameters)
                {
                    if (parameter.ID == 0)
                    {
                        parameter.SectionID = section.ID;
                        db.Parameters.Add(parameter);
                    }
                    else
                    {
                        db.Entry(parameter).State = EntityState.Modified;
                    }
                }
                dbChecklist.Sections.First(s => s.ID == section.ID).Name = section.Name;
            }

            var dbParametersId = dbChecklist.Sections
                .SelectMany(p => p.Parameters)
                .Select(p => p.ID);
            var formParametersId = checklist.Sections
                .Where(s => s.ID > 0)
                .SelectMany(p => p.Parameters)
                .Where(p => p.ID > 0)
                .Select(p => p.ID);

            //remove parameters from database which not contains in accepted model
            db.Parameters.RemoveRange(
                db.Parameters.Where(p => dbParametersId
                    .Except(formParametersId)
                    .Contains(p.ID))
            );

            var dbSectionsId = dbChecklist.Sections
                .Select(p => p.ID);
            var formSectionsId = checklist.Sections
                .Where(s => s.ID > 0)
                .Select(p => p.ID);

            //remove sections from database which not contains in accepted model
            db.Sections.RemoveRange(
                db.Sections.Where(p => dbSectionsId
                    .Except(formSectionsId)
                    .Contains(p.ID))
            );

            db.SaveChanges();

            return checklist.ID;
        }

        public static bool Delete(int checklistId, DatabaseEntities db)
        {
            var checklist = db.Checklists.Find(checklistId);
            if (checklist == null)
            {
                return false;
            }

            ChecklistExportCache.InvalidateRecord(checklist.Name);

            db.Parameters.RemoveRange(checklist.Sections.SelectMany(p => p.Parameters));
            db.Sections.RemoveRange(checklist.Sections);
            db.Checklists.Remove(checklist);
            db.SaveChanges();

            return true;
        }
    }
}