﻿using System.Linq;
using System.Web.Mvc;
using Audits.Models.Database_Manager;

namespace Audits.Models.ModelBinding
{
    public class EmployeeModelBinder : IModelBinder
    {
        private readonly DatabaseEntities db = new DatabaseEntities();

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var subdiv = int.Parse(bindingContext.ValueProvider.GetValue("SubdivisionID").AttemptedValue);
            var employeeId = 0;
            if (!string.IsNullOrEmpty(bindingContext.ValueProvider.GetValue("ID").AttemptedValue))
            {
                employeeId = int.Parse(bindingContext.ValueProvider.GetValue("ID").AttemptedValue);
            }
            var roleId = int.Parse(bindingContext.ValueProvider.GetValue("Role").AttemptedValue);

            var result = new Employee
            {
                ActualPosition = bindingContext.ValueProvider.GetValue("ActualPosition").AttemptedValue,
                AD_ID = bindingContext.ValueProvider.GetValue("AD_ID").AttemptedValue,
                Email = bindingContext.ValueProvider.GetValue("Email").AttemptedValue,
                ID = employeeId,
                EstablishedPosition = bindingContext.ValueProvider.GetValue("EstablishedPosition").AttemptedValue,
                EstablishedPositionID =
                    int.Parse(string.IsNullOrEmpty(bindingContext.ValueProvider.GetValue("EstablishedPositionID")
                        .AttemptedValue)
                        ? "-1"
                        : bindingContext.ValueProvider.GetValue("EstablishedPositionID").AttemptedValue),
                FirstName = bindingContext.ValueProvider.GetValue("FirstName").AttemptedValue,
                LastName = bindingContext.ValueProvider.GetValue("LastName").AttemptedValue,
                Patronymic = bindingContext.ValueProvider.GetValue("Patronymic").AttemptedValue,
                PersonnelNumber = bindingContext.ValueProvider.GetValue("PersonnelNumber").AttemptedValue,
                Phone = bindingContext.ValueProvider.GetValue("Phone").AttemptedValue,
                SubdivisionID = subdiv,
                Subdivision = db.Subdivisions.AsNoTracking().Single(e => e.ID == subdiv),
                Role = db.Roles.AsNoTracking().Where(e => e.ID == roleId).ToList()
            };
            return result;
        }
    }
}