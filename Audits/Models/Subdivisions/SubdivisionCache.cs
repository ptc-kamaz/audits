using System;
using System.Collections.Generic;
using System.Linq;
using Audits.Models.Database_Manager;

namespace Audits.Models.Subdivisions
{
    internal sealed class SubdivisionCache
    {
        private static SubdivisionCache _instance;

        private static readonly object SyncRoot = new object();

        private readonly Dictionary<string, Subdivision[]> _cache = new Dictionary<string, Subdivision[]>();
        private List<Subdivision> _subdivisions = Enumerable.Empty<Subdivision>().ToList();

        private SubdivisionCache()
        {
        }

        internal static SubdivisionCache GetInstance()
        {
            if (_instance == null)
            {
                lock (SyncRoot)
                {
                    if (_instance == null)
                    {
                        _instance = new SubdivisionCache();
                    }
                }
            }
            return _instance;
        }

        internal void LoadSubdivisions()
        {
            using (var db = new DatabaseEntities())
            {
                _subdivisions.Clear();
                _subdivisions = db.Subdivisions.ToList();
            }
        }

        internal void Discard()
        {
            _cache.Clear();
            _subdivisions.Clear();
        }

        internal void DiscardForEmployee(string employeeAdId)
        {
            if (employeeAdId != null)
            {
                _cache.Remove(employeeAdId);
            }
        }

        internal Subdivision[] GetOrCreate(string employeeAdId)
        {
            if (employeeAdId == null)
            {
                throw new ArgumentNullException(nameof(employeeAdId));
            }

            Subdivision[] subdivisions;
            if (_cache.TryGetValue(employeeAdId, out subdivisions))
            {
                return subdivisions;
            }

            if (_subdivisions.Count == 0)
            {
                LoadSubdivisions();
            }

            subdivisions = LoadListOfSubordiateSubdivisions(employeeAdId);
            _cache.Add(employeeAdId, subdivisions);

            return subdivisions;
        }

        private Subdivision[] LoadListOfSubordiateSubdivisions(string employeeAdId)
        {
            int employeeSubdivisionId;
            using (var db = new DatabaseEntities())
            {
                var employee = db.Employees.First(x => string.Equals(x.AD_ID, employeeAdId));
                employeeSubdivisionId = employee.SubdivisionID;
            }

            var subdivisions = LoadSubdivionChildrenRecursively(employeeSubdivisionId);
            subdivisions.Add(_subdivisions.First(x => x.ID == employeeSubdivisionId));

            return subdivisions.ToArray();
        }

        private ICollection<Subdivision> LoadSubdivionChildrenRecursively(int subdivisionId)
        {
            var list = new List<Subdivision>();
            var subdivisions = _subdivisions.Where(x => x.ParentID == subdivisionId).ToArray();

            list.AddRange(subdivisions);

            foreach (var subdivision in subdivisions)
            {
                var children = LoadSubdivionChildrenRecursively(subdivision.ID);
                list.AddRange(children);
            }

            return list;
        }
    }
}