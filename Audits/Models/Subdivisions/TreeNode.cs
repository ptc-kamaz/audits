﻿using System.Collections.Generic;

namespace Audits.Models.Subdivisions
{
    public class TreeNode<T>
    {
        public TreeNode()
        {
            Children = new List<TreeNode<T>>();
        }

        public TreeNode(int id, string text) : this(id, text, default(T))
        {
        }

        public TreeNode(int id, string text, T data)
        {
            Children = new List<TreeNode<T>>();
            ID = id;
            Text = text;
            Data = data;
        }

        public int ID { get; set; }
        public string Text { get; set; }
        public T Data { get; set; }

        public ICollection<TreeNode<T>> Children { get; }
    }
}