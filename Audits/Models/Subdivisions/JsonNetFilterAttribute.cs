﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Audits.Models.Subdivisions
{
    public sealed class JsonNetFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var result = filterContext.Result as JsonResult;
            if (result != null)
            {
                filterContext.Result = new CustomJsonResult(result);
            }
        }

        private class CustomJsonResult : JsonResult
        {
            public CustomJsonResult(JsonResult jsonResult)
            {
                ContentEncoding = jsonResult.ContentEncoding;
                ContentType = jsonResult.ContentType;
                Data = jsonResult.Data;
                JsonRequestBehavior = jsonResult.JsonRequestBehavior;
                MaxJsonLength = jsonResult.MaxJsonLength;
                RecursionLimit = jsonResult.RecursionLimit;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }
                // verify that GET methods are allowed
                if (!IsGetAllowed(context.HttpContext.Request.HttpMethod))
                {
                    throw new InvalidOperationException("GET is not allowed! Change JsonRequestBehavior to AllowGet.");
                }

                var response = context.HttpContext.Response;
                response.ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType;
                if (ContentEncoding != null)
                {
                    response.ContentEncoding = ContentEncoding;
                }
                if (Data != null)
                {
                    response.Write(SerializeData(Data));
                }
            }

            private bool IsGetAllowed(string httpMethod)
            {
                return JsonRequestBehavior != JsonRequestBehavior.DenyGet
                       && string.Equals(httpMethod, "GET", StringComparison.OrdinalIgnoreCase);
            }

            private static string SerializeData(object data)
            {
                return JsonConvert.SerializeObject(data, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    MaxDepth = 1,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
        }
    }
}