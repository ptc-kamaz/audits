﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Audits.Models.Database_Manager;

namespace Audits.Models.Subdivisions
{
    /// <summary>
    ///     Generates tree of subdivions based on provided list of subdivions, and vice versa.
    /// </summary>
    public static class TreeFactory
    {
        /// <summary>
        ///     Generates tree from a list of subdivions.
        ///     Provided data structure requried by jstree.
        /// </summary>
        /// <param name="subdivisions">Collection of subdivisions.</param>
        /// <returns>Root node of the tree.</returns>
        public static TreeNode<Subdivision> GenerateTree(IEnumerable<Subdivision> subdivisions)
        {
            TreeNode<Subdivision> root;
            var nodes = GenerateNodes(subdivisions, out root);
            LinkNodes(nodes);
            return root;
        }

        /// <summary>
        ///     Generates list of node based on the provided list of subdivisions.
        /// </summary>
        /// <param name="subdivisions">Collection of subdivisions</param>
        /// <param name="root">Root element of the tree.</param>
        /// <returns>List of unlinked tree nodes.</returns>
        private static IEnumerable<TreeNode<Subdivision>> GenerateNodes(IEnumerable<Subdivision> subdivisions,
            out TreeNode<Subdivision> root)
        {
            var nodes = new List<TreeNode<Subdivision>>();
            root = null;
            foreach (var item in subdivisions)
            {
                var node = new TreeNode<Subdivision>(item.ID, item.Name,
                    new Subdivision(item.ID, item.ParentID, item.Name, item.HeadID, item.SAP_ID));
                nodes.Add(node);
                if (item.Parent == null)
                {
                    if (root == null)
                    {
                        root = node;
                    }
                    else
                    {
                        throw new RootSurfeitException();
                    }
                }
            }
            if (root == null)
            {
                throw new RootAbsenceException();
            }
            return nodes;
        }

        /// <summary>
        ///     Converts collection of tree nodes into a tree of linked nodes with one root element.
        /// </summary>
        /// <param name="nodes"></param>
        private static void LinkNodes(IEnumerable<TreeNode<Subdivision>> nodes)
        {
            var treeNodes = nodes as TreeNode<Subdivision>[] ?? nodes.ToArray();
            foreach (var item in treeNodes)
            {
                var parent = treeNodes.FirstOrDefault(n => n.Data.ID == item.Data.ParentID);
                parent?.Children.Add(item);
            }
        }

        /// <summary>
        ///     Generates a list of nodes with correct mapping of child-parent relations based on
        ///     the provided tree structure.
        /// </summary>
        /// <param name="root">Root element of the tree.</param>
        /// <returns>List of all tree nodes with correct mapping of chlid-parent relations.</returns>
        public static List<Subdivision> GenerateSubdivisions(TreeNode<Subdivision> root)
        {
            var list = new List<Subdivision>();
            FlatternTree(list, root);
            return list;
        }

        /// <summary>
        ///     Recursive method which inserts into provided list a root element and
        ///     all its chlid nodes with corresponding assignment of child-parent relations.
        /// </summary>
        /// <param name="list">List of Subdivisions which will be appended with the root node and its children nodes.</param>
        /// <param name="root">Root node of a tree which will be flatterned.</param>
        private static void FlatternTree(ICollection<Subdivision> list, TreeNode<Subdivision> root)
        {
            var parent = root.Data;
            if (parent.ParentID == -1)
            {
                parent.ParentID = null;
            }
            if (parent.HeadID == -1)
            {
                parent.HeadID = null;
            }
            list.Add(parent);
            foreach (var item in root.Children)
            {
                var child = item.Data;
                child.Parent = parent;
                child.ParentID = parent.ID;
                parent.Children.Add(child);
                FlatternTree(list, item);
            }
        }
    }

    /// <summary>
    ///     Represents errors of structure's root absense.
    /// </summary>
    [Serializable]
    public class RootAbsenceException : Exception
    {
        public RootAbsenceException() : base("Root element was not discovered!")
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the RootAbsenceException class with a specified
        //     error message.
        //
        // Parameters:
        //   message:
        //     A message that describes the error.
        public RootAbsenceException(string message) : base(message)
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the RootAbsenceException class with a specified
        //     error message and a reference to the inner exception that is the cause of this
        //     exception.
        //
        // Parameters:
        //   message:
        //     The error message that explains the reason for the exception.
        //
        //   innerException:
        //     The exception that is the cause of the current exception. If the innerException
        //     parameter is not a null reference, the current exception is raised in a catch
        //     block that handles the inner exception.
        public RootAbsenceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the RootAbsenceException class with serialized
        //     data.
        //
        // Parameters:
        //   info:
        //     The object that holds the serialized object data.
        //
        //   context:
        //     The contextual information about the source or destination.
        protected RootAbsenceException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    /// <summary>
    ///     Represents errors that occur when structure has more than one root element.
    /// </summary>
    [Serializable]
    public class RootSurfeitException : Exception
    {
        public RootSurfeitException() : base("More than one root element was discovered!")
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the RootSurfeitException class with a specified
        //     error message.
        //
        // Parameters:
        //   message:
        //     A message that describes the error.

        public RootSurfeitException(string message) : base(message)
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the RootSurfeitException class with a specified
        //     error message and a reference to the inner exception that is the cause of this
        //     exception.
        //
        // Parameters:
        //   message:
        //     The error message that explains the reason for the exception.
        //
        //   innerException:
        //     The exception that is the cause of the current exception. If the innerException
        //     parameter is not a null reference, the current exception is raised in a catch
        //     block that handles the inner exception.
        public RootSurfeitException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        //
        // Summary:
        //     Initializes a new instance of the RootSurfeitException class with serialized
        //     data.
        //
        // Parameters:
        //   info:
        //     The object that holds the serialized object data.
        //
        //   context:
        //     The contextual information about the source or destination.
        protected RootSurfeitException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}