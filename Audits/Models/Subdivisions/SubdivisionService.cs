﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Audits.Models.Database_Manager;

namespace Audits.Models.Subdivisions
{
    public class SubdivisionService
    {
        public int SaveOrUpdate(IEnumerable<Subdivision> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            var subdivisions = collection as Subdivision[] ?? collection.ToArray();
            if (!subdivisions.Any())
            {
                return 0;
            }

            using (var db = new DatabaseEntities())
            {
                foreach (var item in subdivisions)
                    // If the item has ID greater than 0 then it is already in database and
                    // we should update it.
                {
                    if (item.ID > 0)
                    {
                        db.Subdivisions.Add(item);
                        db.Subdivisions.Attach(item);
                        db.Entry(item).State = EntityState.Modified;
                    }
                    // Otherwise we should add this item into the database.
                    else
                    {
                        db.Subdivisions.Add(item);
                    }
                }
                return db.SaveChanges();
            }
        }

        public int Remove(IEnumerable<Subdivision> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            var enumerable = collection as Subdivision[] ?? collection.ToArray();
            if (!enumerable.Any())
            {
                return 0;
            }

            using (var db = new DatabaseEntities())
            {
                // Select ids of the provided Subdivisions.
                var ids = enumerable.Select(s => s.ID).AsEnumerable();
                // Load from database all entities with the provided ids.
                var subdivisions = db.Subdivisions.Where(s => ids.Contains(s.ID));
                // Form a track list of this entities.
                var list = new List<Subdivision>(subdivisions);
                // Remove entities while the track list is not empty, i.e. while there are
                // entities which are not removed yet.
                while (list.Any())
                {
                    Remove(db, list, list.First());
                }
                return db.SaveChanges();
            }
        }

        private static void Remove(DatabaseEntities db, ICollection<Subdivision> list, Subdivision subdivision)
        {
            // Remove all children records first.
            while (subdivision.Children.Any())
            {
                Remove(db, list, subdivision.Children.First());
            }
            // Remove the entity itself from the database and the track list.
            db.Subdivisions.Remove(subdivision);
            list.Remove(subdivision);
        }

        public IReadOnlyCollection<Subdivision> LoadListOfSubordiateSubdivisions(string employeeAdId)
        {
            if (employeeAdId == null)
            {
                throw new ArgumentNullException(nameof(employeeAdId));
            }

            return SubdivisionCache.GetInstance().GetOrCreate(employeeAdId);
        }
    }
}