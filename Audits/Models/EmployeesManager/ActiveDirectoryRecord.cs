﻿namespace Audits.Models.EmployeesManager
{
    public class ActiveDirectoryRecord
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string samaccountname { get; set; }
    }
}