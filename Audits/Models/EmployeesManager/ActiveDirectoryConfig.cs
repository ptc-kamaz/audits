﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Audits.Models.EmployeesManager
{
    [Serializable]
    [XmlRoot("ActiveDirectoryConfig")]
    public class ActiveDirectoryConfig
    {
        [Required]
        [XmlElement("serverurl")]
        public string URL { get; set; }

        [Required]
        [XmlElement("ldapfilter")]
        public string filter { get; set; }
    }
}