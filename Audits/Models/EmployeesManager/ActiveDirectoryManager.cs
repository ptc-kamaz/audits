﻿using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Xml.Serialization;

namespace Audits.Models.EmployeesManager
{
    public class ActiveDirectoryManager
    {
        private readonly ActiveDirectoryConfig _adConfig;

        public ActiveDirectoryManager()
        {
            var configPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath,
                ConfigurationManager.AppSettings["ActiveDirectoryConfig"]);
            using (var fileStream = new FileStream(configPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var xRoot = new XmlRootAttribute
                {
                    ElementName = "activedirectory",
                    IsNullable = true
                };
                var xml = new XmlSerializer(typeof(ActiveDirectoryConfig), xRoot);
                _adConfig = (ActiveDirectoryConfig) xml.Deserialize(fileStream);
                fileStream.Close();
            }
        }

        public ICollection<ActiveDirectoryRecord> LoadAd(string name)
        {
            ICollection<ActiveDirectoryRecord> result = new List<ActiveDirectoryRecord>();
            var ldapConncetion = new DirectoryEntry($"LDAP://{_adConfig.URL}/{_adConfig.filter}");
            var ds = new DirectorySearcher(ldapConncetion)
            {
                PageSize = int.MaxValue,
                Filter = "(&(objectCategory=person)(objectClass=user)(displayName=*" + name + "*))"
            };
            ds.PropertiesToLoad.Add("givenName");
            ds.PropertiesToLoad.Add("samaccountname");
            ds.PropertiesToLoad.Add("mail");
            ds.PropertiesToLoad.Add("usergroup");
            ds.PropertiesToLoad.Add("displayname");
            var resultCol = ds.FindAll();

            for (var i = 0; i < resultCol.Count; i++)
            {
                var searchItem = new ActiveDirectoryRecord();
                var sr = resultCol[i];
                searchItem.Id = i;
                searchItem.samaccountname = (string) sr.Properties["samaccountname"]?[0];
                searchItem.Email = sr.Properties["mail"]?.Count > 0 ? (string) sr.Properties["mail"]?[0] : "";
                searchItem.DisplayName = (string) sr.Properties["displayname"]?[0];
                searchItem.FirstName = sr.Properties["givenname"]?.Count > 0
                    ? (string) sr.Properties["givenname"]?[0]
                    : "";
                result.Add(searchItem);
            }
            return result;
        }

        public string LoadGroups(ActiveDirectoryRecord selectedItem)
        {
            var pc = new PrincipalContext(ContextType.Domain, _adConfig.URL, _adConfig.filter);
            var selectedUser = new UserPrincipal(pc)
            {
                SamAccountName = selectedItem.samaccountname,
                EmailAddress = selectedItem.Email
            };
            var searcher = new PrincipalSearcher(selectedUser);
            var searchResult = searcher.FindAll();
            return searchResult.First().Sid.Value;
        }
    }
}