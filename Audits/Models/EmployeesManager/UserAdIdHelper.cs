﻿using System;
using System.Data;
using System.Security.Principal;
using System.Web;

namespace Audits.Models.EmployeesManager
{
    public static class UserAdIdHelper
    {
        public static string GetUserAdId(this HttpContextBase httpContextBase)
        {
            var identity = GetWindowsIdentity(httpContextBase);
            if (identity.User == null)
            {
                throw new NoNullAllowedException(nameof(identity.User));
            }

            return identity.User.Value;
        }

        public static WindowsIdentity GetWindowsIdentity(this HttpContextBase httpContextBase)
        {
            if (httpContextBase == null)
            {
                throw new ArgumentNullException(nameof(httpContextBase));
            }
            if (httpContextBase.User == null)
            {
                throw new NoNullAllowedException(nameof(httpContextBase.User));
            }

            var identity = httpContextBase.User.Identity as WindowsIdentity;
            if (identity == null)
            {
                throw new NoNullAllowedException(nameof(identity));
            }

            return identity;
        }
    }
}