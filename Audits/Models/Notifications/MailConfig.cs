﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Audits.Models.Notifications
{
    [Serializable]
    [XmlRoot("MailConfig")]
    public class MailConfig
    {
        [Required]
        [XmlElement("Address")]
        public string Address { get; set; }

        [Required]
        [XmlElement("Host")]
        public string Host { get; set; }

        [Required]
        [XmlElement("Port")]
        public int Port { get; set; }

        [Required]
        [XmlElement("UserName")]
        public string UserName { get; set; }

        [Required]
        [XmlElement("Password")]
        public string Password { get; set; }
    }
}