﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Xml.Serialization;

namespace Audits.Models.Notifications
{
    public class MailService
    {
        private readonly MailConfig _config;
        private readonly string _configPath;
        private readonly XmlSerializer _serializer;

        public MailService(string configPath)
        {
            _configPath = configPath;
            _serializer = new XmlSerializer(typeof(MailConfig));
            _config = LoadConfig();
        }

        public bool Send(Mail mail)
        {
            using (var mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(_config.Address);
                mailMessage.To.Add(mail.To);
                mailMessage.Subject = mail.Subject;
                mailMessage.Body = mail.Body;

                if (mail.CC.Count > 0)
                {
                    foreach (var address in mail.CC)
                    {
                        mailMessage.CC.Add(address);
                    }
                }

                using (var client = new SmtpClient())
                {
                    client.EnableSsl = true;
                    client.Host = _config.Host;
                    client.Port = _config.Port;
                    client.Credentials = new NetworkCredential(_config.UserName, _config.Password);
                    try
                    {
                        client.Send(mailMessage);
                        return true;
                    }
                    catch (SmtpFailedRecipientsException exc)
                    {
                        exc.GetBaseException();
                        return false;
                    }
                }
            }
        }

        public MailConfig LoadConfig()
        {
            MailConfig result;

            using (var fileStream = new FileStream(_configPath, FileMode.OpenOrCreate))
            {
                result = (MailConfig) _serializer.Deserialize(fileStream);
            }
            return result;
        }

        public bool SetConfig(MailConfig mailConfig)
        {
            using (var fileStream = new FileStream(_configPath, FileMode.Create))
            {
                try
                {
                    _serializer.Serialize(fileStream, mailConfig);
                    return true;
                }
                catch (Exception exc)
                {
                    exc.GetBaseException();
                    return false;
                }
            }
        }
    }
}