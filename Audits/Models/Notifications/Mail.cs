﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Audits.Models.Notifications
{
    public class Mail
    {
        public Mail()
        {
            CC = new List<string>();
        }

        [Required]
        public string To { get; set; }

        public ICollection<string> CC { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        public string Subject { get; set; }
    }
}