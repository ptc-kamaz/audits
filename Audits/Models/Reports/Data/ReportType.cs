﻿namespace Audits.Models.Reports.Data
{
    public enum ReportType
    {
        ComplianceByMonths,
        Compliance,
        Execution,
        Validation
    }
}