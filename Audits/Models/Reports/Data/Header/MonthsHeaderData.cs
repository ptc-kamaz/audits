﻿using System.Collections.Generic;
using System.Globalization;

namespace Audits.Models.Reports.Data.Header
{
    public class MonthsHeaderData : HeaderData
    {
        public MonthsHeaderData(Report report) : base(report)
        {
            ColumnNamesList = new List<string>();
            var culture = new CultureInfo("ru-RU");
            var months = new DateTimeRange(Report.StartDate, Report.EndDate).SplitIntoMonths();

            foreach (var range in months)
            {
                ColumnNamesList.Add(range.Start.ToString("MMMM", culture) +
                                    $" ({range.Start.Day}.{range.Start.Month} - {range.End.Day}.{range.End.Month})");
            }
        }

        public override string Title => "Уровень соответствия по месяцам, %";
    }
}