﻿namespace Audits.Models.Reports.Data.Header
{
    public class ValidationHeaderData : SectionsHeaderData
    {
        public ValidationHeaderData(Report report) : base(report)
        {
            ColumnNamesList.RemoveAt(ColumnNumber - 1);
        }

        public override string Title => "Процент выполнения по номинациям, %";
    }
}
