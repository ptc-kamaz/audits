﻿using System.Collections.Generic;

namespace Audits.Models.Reports.Data.Header
{
    public abstract class HeaderData
    {
        protected List<string> ColumnNamesList;

        protected HeaderData(Report report)
        {
            Report = report;
        }

        protected Report Report { get; }

        public abstract string Title { get; }

        public List<string> ColumnNames => ColumnNamesList;
        public int ColumnNumber => ColumnNamesList.Count;
    }
}