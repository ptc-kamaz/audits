﻿using System.Linq;

namespace Audits.Models.Reports.Data.Header
{
    public class SectionsHeaderData : HeaderData
    {
        public SectionsHeaderData(Report report) : base(report)
        {
            ColumnNamesList =
                Report.AvailableSections
                    .Select(s => s.Name)
                    .ToList();
            ColumnNamesList.Add("Сводная по всем номинациям");
        }

        public override string Title => "Уровень соответствия по номинациям, %";
    }
}