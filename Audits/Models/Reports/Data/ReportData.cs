﻿using System;
using System.Linq;
using Audits.Models.Reports.Data.Body;
using Audits.Models.Reports.Data.Chart;
using Audits.Models.Reports.Data.Header;

namespace Audits.Models.Reports.Data
{
    public class ReportData
    {
        public readonly BodyData Body;
        public readonly ChartData Chart;
        public readonly HeaderData Header;

        public ReportData(Report report, ReportType reportType, ChartData chartData = null)
        {
            Chart = chartData;
            switch (reportType)
            {
                case ReportType.ComplianceByMonths:
                    Header = new MonthsHeaderData(report);
                    Body = new ComplianceByMonthsBodyData(report);
                    break;
                case ReportType.Compliance:
                    Header = new SectionsHeaderData(report);
                    Body = new ComplianceBodyData(report);
                    break;
                case ReportType.Execution:
                    Header = new SectionsHeaderData(report);
                    Body = new ExecutionBodyData(report);
                    break;
                case ReportType.Validation:
                    Header = new ValidationHeaderData(report);
                    Body = new ValidationBodyData(report);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(reportType), reportType, null);
            }
            if (Chart != null)
            {
                Chart.DataIndex = FindChartSelectedIndex(report);
            }
        }

        private int FindChartSelectedIndex(Report report)
        {
            switch (Chart.Data)
            {
                case SelectedData.Column:
                    return report.AvailableSections.TakeWhile(section => section.ID != report.SelectedSection?.ID)
                        .Count();
                case SelectedData.Row:
                    return report.Subdivisions
                        .TakeWhile(subdivision => subdivision.ID != report.SelectedSubdivision?.ID).Count();
                default:
                    return 0;
            }
        }
    }
}