﻿using OfficeOpenXml.Drawing.Chart;

namespace Audits.Models.Reports.Data.Chart
{
    public class ChartData
    {
        public SelectedData Data;
        public int DataIndex;
        public string Title;
        public eChartType Type;

        public ChartData(Report report, eChartType type, SelectedData data, string title)
        {
            Report = report;
            Type = type;
            Data = data;
            Title = title;
        }

        protected Report Report { get; }
    }
}