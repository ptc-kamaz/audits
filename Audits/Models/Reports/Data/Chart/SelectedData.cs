﻿namespace Audits.Models.Reports.Data.Chart
{
    public enum SelectedData
    {
        Column,
        Row,
        Both
    }
}