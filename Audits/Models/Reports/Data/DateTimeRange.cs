﻿using System;
using System.Collections.Generic;

namespace Audits.Models.Reports.Data
{
    public class DateTimeRange
    {
        public DateTimeRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public List<DateTimeRange> SplitIntoMonths()
        {
            var tempStart = Start;
            DateTime tempEnd;
            var months = new List<DateTimeRange>();
            while ((tempEnd = tempStart.AddMonths(1).AddDays(1).AddMilliseconds(-1)) <= End)
            {
                months.Add(new DateTimeRange(tempStart, tempEnd));
                tempStart = tempEnd;
            }
            if (tempStart < End)
            {
                months.Add(new DateTimeRange(tempStart, End));
            }
            return months;
        }
    }
}