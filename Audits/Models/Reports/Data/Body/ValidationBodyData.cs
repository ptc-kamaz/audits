﻿namespace Audits.Models.Reports.Data.Body
{
    internal class ValidationBodyData : GenericBodyDataCalculator
    {
        public ValidationBodyData(Report report) : base(report)
        {
        }

        protected override void Initialize()
        {
            CalculateSeparateValue = ReportService.CalculateValidationLevel;
            CalculateOverallValues = null;
        }
    }
}
