﻿namespace Audits.Models.Reports.Data.Body
{
    internal class ComplianceBodyData : GenericBodyDataCalculator
    {
        public ComplianceBodyData(Report report) : base(report)
        {
        }

        protected override void Initialize()
        {
            CalculateSeparateValue = ReportService.CalculateLevelOfComplianceForSectionForPeriod;
            CalculateOverallValues = ReportService.CalculateOverallSectionsCompliance;
        }
    }
}