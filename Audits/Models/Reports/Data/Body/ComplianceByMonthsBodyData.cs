﻿using System.Collections.Generic;

namespace Audits.Models.Reports.Data.Body
{
    public class ComplianceByMonthsBodyData : BodyData
    {
        public ComplianceByMonthsBodyData(Report report) : base(report)
        {
            RowsList = new List<BodyRow>();

            var rowCounter = 1;
            foreach (var subdivision in Report.Subdivisions)
            {
                var row = new BodyRow
                {
                    Names = new List<string>
                    {
                        rowCounter++.ToString(),
                        subdivision.Parent != null ? subdivision.Parent.Name : "-",
                        Report.Subdivision.Name,
                        subdivision.Name
                    },
                    Values = new List<double>()
                };

                var months = new DateTimeRange(Report.StartDate, Report.EndDate).SplitIntoMonths();
                foreach (var range in months)
                {
                    var level = ReportService.CalculateLevelOfComplianceForSectionForPeriod(subdivision, range.Start,
                        range.End, Report.SelectedSection);
                    row.Values.Add(level >= 0 ? level : 0);
                }
                RowsList.Add(row);
            }
        }
    }
}