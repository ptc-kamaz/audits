﻿using System.Collections.Generic;
using System.Linq;

namespace Audits.Models.Reports.Data.Body
{
    public abstract class BodyData
    {
        protected List<BodyRow> RowsList;

        protected BodyData(Report report)
        {
            Report = report;
        }

        protected Report Report { get; }

        public List<BodyRow> Rows => RowsList;
        public int RowsNumber => RowsList.Count;
        public int NamesNumber => RowsList.First().Names.Count;
        public int ValuesNumber => RowsList.First().Values.Count;
    }
}