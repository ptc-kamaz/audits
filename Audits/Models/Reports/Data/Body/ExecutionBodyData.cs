﻿namespace Audits.Models.Reports.Data.Body
{
    internal class ExecutionBodyData : GenericBodyDataCalculator
    {
        public ExecutionBodyData(Report report) : base(report)
        {
        }

        protected override void Initialize()
        {
            CalculateSeparateValue = ReportService.CalculateExecutionLevel;
            CalculateOverallValues = ReportService.CalculateOverallExecutionLevel;
        }
    }
}