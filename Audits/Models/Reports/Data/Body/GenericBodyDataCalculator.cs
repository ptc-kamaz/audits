﻿using System;
using System.Collections.Generic;
using Audits.Models.Database_Manager;

namespace Audits.Models.Reports.Data.Body
{
    internal abstract class GenericBodyDataCalculator : BodyData
    {
        protected Func<Subdivision, DateTime, DateTime, double> CalculateOverallValues;
        protected Func<Subdivision, DateTime, DateTime, Section, double> CalculateSeparateValue;

        protected GenericBodyDataCalculator(Report report) : base(report)
        {
            Initialize();
            CalculateBodyData();
        }

        protected abstract void Initialize();

        private void CalculateBodyData()
        {
            RowsList = new List<BodyRow>();

            var rowCounter = 1;
            foreach (var subdivision in Report.Subdivisions)
            {
                var row = new BodyRow
                {
                    Names = new List<string>
                    {
                        rowCounter++.ToString(),
                        subdivision.Parent != null ? subdivision.Parent.Name : "-",
                        Report.Subdivision.Name,
                        subdivision.Name
                    },
                    Values = new List<double>()
                };

                double level;
                foreach (var section in Report.AvailableSections)
                {
                    level = CalculateSeparateValue(subdivision,
                        Report.StartDate,
                        Report.EndDate, section);
                    row.Values.Add(level >= 0 ? level : 0);
                }

                if (CalculateOverallValues != null)
                {
                    level = CalculateOverallValues(subdivision, Report.StartDate, Report.EndDate);
                    row.Values.Add(level >= 0 ? level : 0);
                }

                if (row.Values.Count == 0)
                {
                    row.Values.Add(0);
                }

                RowsList.Add(row);
            }
        }
    }
}
