﻿using System.Collections.Generic;

namespace Audits.Models.Reports.Data.Body
{
    public class BodyRow
    {
        public List<string> Names { get; set; }
        public List<double> Values { get; set; }
    }
}