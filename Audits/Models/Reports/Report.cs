﻿using System;
using System.Collections.Generic;
using Audits.Models.Database_Manager;

namespace Audits.Models.Reports
{
    public class Report
    {

        public Report(Subdivision subdivision, List<Subdivision> subdivisions, DateTime startDate, DateTime endDate, Subdivision selectedSubdivision, Section selectedSection)
        {
            Subdivision = subdivision;
            Subdivisions = subdivisions;
            StartDate = startDate;
            EndDate = endDate;
            SelectedSubdivision = selectedSubdivision;
            SelectedSection = selectedSection;
            AvailableSections = ReportService.GetAvailableSections(Subdivisions, startDate, endDate);
        }

        public Report(Subdivision subdivision, List<Subdivision> subdivisions, DateTime startDate, DateTime endDate,
            Section selectedSection) : this(subdivision, subdivisions, startDate, endDate, null, selectedSection)
        {
        }

        public List<Subdivision> Subdivisions { get; }
        public Section SelectedSection { get; }
        public DateTime StartDate { get; }
        public DateTime EndDate { get; }
        public Subdivision SelectedSubdivision { get; }
        public Subdivision Subdivision { get; }
        public List<Section> AvailableSections { get; }
    }
}