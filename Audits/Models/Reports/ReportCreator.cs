﻿using System;
using System.IO;
using Audits.Models.Database_Manager;
using Audits.Models.Reports.Data;
using Audits.Models.Reports.Data.Chart;
using OfficeOpenXml.Drawing.Chart;

namespace Audits.Models.Reports
{
    public class ReportCreator
    {
        private readonly Report _report;
        public bool SaveAsPdf { get; set; }

        public ReportCreator(Subdivision subdivision, DateTime startDateTime,
            DateTime endDateTime, Subdivision selectedSubdivision, Section selectedSection, string outputDir)
        {
            var subdivisionChildren = ReportService.GetSubdivisionChildren(subdivision);
            _report = new Report(subdivision, subdivisionChildren, startDateTime, endDateTime,
                selectedSubdivision, selectedSection);
        }

        public ReportCreator(Subdivision subdivision, DateTime startDateTime,
            DateTime endDateTime, Subdivision selectedSubdivision, string outputDir) : this(subdivision, startDateTime,
            endDateTime, selectedSubdivision, null, outputDir)
        {
        }

        public ReportCreator(Subdivision subdivision, DateTime startDateTime,
            DateTime endDateTime, Section selectedSection, string outputDir) : this(subdivision, startDateTime,
            endDateTime, null, selectedSection, outputDir)
        {
        }

        public ReportCreator(Subdivision subdivision, DateTime startDateTime,
            DateTime endDateTime, string outputDir) : this(subdivision, startDateTime,
            endDateTime, null, null, outputDir)
        {
        }

        public byte[] CreateExecutionScheduleBarReport()
        {
            var chartData = new ChartData(_report, eChartType.BarClustered, SelectedData.Row, "Динамика изменения");
            var reportData = new ReportData(_report, ReportType.Execution, chartData);

            return new ReportGenerator(reportData).Generate(SaveAsPdf);
        }

        public byte[] CreateExecutionSchedulePieReport()
        {
            var chartData = new ChartData(_report, eChartType.Pie3D, SelectedData.Row, "Динамика изменения");
            var reportData = new ReportData(_report, ReportType.Execution, chartData);

            return new ReportGenerator(reportData).Generate(SaveAsPdf);
        }

        public byte[] CreateComplianceForSectionByMonthsReport()
        {
            var chartData = new ChartData(_report, eChartType.LineMarkers, SelectedData.Both, "Динамика изменения");
            var reportData = new ReportData(_report, ReportType.ComplianceByMonths, chartData);

            return new ReportGenerator(reportData).Generate(SaveAsPdf);
        }

        public byte[] CreateComplianceLevelForSectionReport()
        {
            var chartData = new ChartData(_report, eChartType.BarClustered, SelectedData.Column, "Динамика изменения");
            var reportData = new ReportData(_report, ReportType.Compliance, chartData);

            return new ReportGenerator(reportData).Generate(SaveAsPdf);
        }

        public byte[] CreateComplianceForSubdivisionBarReport()
        {
            var chartData = new ChartData(_report, eChartType.BarClustered, SelectedData.Row, "Динамика изменения");
            var reportData = new ReportData(_report, ReportType.Compliance, chartData);

            return new ReportGenerator(reportData).Generate(SaveAsPdf);
        }

        public byte[] CreateComplianceForSubdivisionRadarReport()
        {
            var chartData = new ChartData(_report, eChartType.RadarMarkers, SelectedData.Row, "Динамика изменения");
            var reportData = new ReportData(_report, ReportType.Compliance, chartData);

            return new ReportGenerator(reportData).Generate(SaveAsPdf);
        }

        public byte[] CreateValidationLevelReport()
        {
            var reportData = new ReportData(_report, ReportType.Validation);

            return new ReportGenerator(reportData, true).Generate(SaveAsPdf);
        }
    }
}