﻿using Audits.Models.Reports.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Audits.Models.Reports.Render
{
    public static class HeaderRenderService
    {
        public static int RenderHead(ReportData reportData, ExcelWorksheet worksheet, int rowIndex)
        {
            var nextRow = rowIndex + 1;
            var startColumn = RenderStandartHeaderTitles(rowIndex, nextRow, worksheet) + 1;

            var columnCount = (reportData.Header.ColumnNumber > 0) ? reportData.Header.ColumnNumber - 1 : 0;
            worksheet.Cells[rowIndex, startColumn, rowIndex, startColumn + columnCount].Merge = true;
            worksheet.Cells[rowIndex, startColumn, rowIndex, startColumn + columnCount].Style
                .Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[rowIndex, startColumn, rowIndex, startColumn + columnCount].Style
                .WrapText = true;
            worksheet.Cells[rowIndex, startColumn, rowIndex, startColumn + columnCount].Style
                .HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[rowIndex, startColumn, rowIndex, startColumn + columnCount].Style
                .VerticalAlignment = ExcelVerticalAlignment.Center;

            worksheet.Cells[rowIndex, startColumn, rowIndex, startColumn + columnCount].Value =
                reportData.Header.Title;

            var columnCounter = startColumn;
            foreach (var columnName in reportData.Header.ColumnNames)
            {
                worksheet.Cells[nextRow, columnCounter].Value = columnName;
                worksheet.Cells[nextRow, columnCounter].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[nextRow, columnCounter].Style.WrapText = true;
                worksheet.Cells[nextRow, columnCounter].Style.HorizontalAlignment =
                    ExcelHorizontalAlignment.Center;
                worksheet.Cells[nextRow, columnCounter].Style.VerticalAlignment =
                    ExcelVerticalAlignment.Center;
                columnCounter++;
            }
            rowIndex = 3;
            return rowIndex;
        }

        private static int RenderStandartHeaderTitles(int currentRow, int nextRow, ExcelWorksheet worksheet)
        {
            worksheet.Cells[$"A{currentRow}:A{nextRow}"].Merge = true;
            worksheet.Cells[$"A{currentRow}:A{nextRow}"].Style.Border.BorderAround(
                ExcelBorderStyle.Thin);
            worksheet.Cells[$"A{currentRow}:A{nextRow}"].Style.WrapText = true;
            worksheet.Cells[$"A{currentRow}:A{nextRow}"].Style.HorizontalAlignment =
                ExcelHorizontalAlignment.Center;
            worksheet.Cells[$"A{currentRow}:A{nextRow}"].Style.VerticalAlignment =
                ExcelVerticalAlignment.Center;
            worksheet.Cells[$"A{currentRow}:A{nextRow}"].Value = "№";

            worksheet.Cells[$"B{currentRow}:B{nextRow}"].Merge = true;
            worksheet.Cells[$"B{currentRow}:B{nextRow}"].Style.Border.BorderAround(
                ExcelBorderStyle.Thin);
            worksheet.Cells[$"B{currentRow}:B{nextRow}"].Style.WrapText = true;
            worksheet.Cells[$"B{currentRow}:B{nextRow}"].Style.HorizontalAlignment =
                ExcelHorizontalAlignment.Center;
            worksheet.Cells[$"B{currentRow}:B{nextRow}"].Style.VerticalAlignment =
                ExcelVerticalAlignment.Center;
            worksheet.Cells[$"B{currentRow}:B{nextRow}"].Value = "Блок";

            worksheet.Cells[$"C{currentRow}:C{nextRow}"].Merge = true;
            worksheet.Cells[$"C{currentRow}:C{nextRow}"].Style.Border.BorderAround(
                ExcelBorderStyle.Thin);
            worksheet.Cells[$"C{currentRow}:C{nextRow}"].Style.WrapText = true;
            worksheet.Cells[$"C{currentRow}:C{nextRow}"].Style.HorizontalAlignment =
                ExcelHorizontalAlignment.Center;
            worksheet.Cells[$"C{currentRow}:C{nextRow}"].Style.VerticalAlignment =
                ExcelVerticalAlignment.Center;
            worksheet.Cells[$"C{currentRow}:C{nextRow}"].Value = "Подразделение";

            worksheet.Cells[$"D{currentRow}:D{nextRow}"].Merge = true;
            worksheet.Cells[$"D{currentRow}:D{nextRow}"].Style.Border.BorderAround(
                ExcelBorderStyle.Thin);
            worksheet.Cells[$"D{currentRow}:D{nextRow}"].Style.WrapText = true;
            worksheet.Cells[$"D{currentRow}:D{nextRow}"].Style.HorizontalAlignment =
                ExcelHorizontalAlignment.Center;
            worksheet.Cells[$"D{currentRow}:D{nextRow}"].Style.VerticalAlignment =
                ExcelVerticalAlignment.Center;
            worksheet.Cells[$"D{currentRow}:D{nextRow}"].Value = "Участок (цех/отдел)";

            //standart number of common columns 
            return 4;
        }
    }
}