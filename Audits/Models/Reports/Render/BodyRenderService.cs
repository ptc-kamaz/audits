﻿using System.Drawing;
using System.Linq;
using Audits.Models.Reports.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Audits.Models.Reports.Render
{
    public static class BodyRenderService
    {
        public static int RenderBody(ReportData reportData, ExcelWorksheet worksheet, int rowIndex, bool colorBodyCells)
        {
            var fromRow = rowIndex;
            var fromColumn = reportData.Body.Rows.First().Names.Count + 1;
            var toColumn = fromColumn + reportData.Body.Rows.First().Values.Count - 1;
            var toRow = fromRow + reportData.Body.RowsNumber - 1;

            foreach (var bodyRow in reportData.Body.Rows)
            {
                var columnIndex = 1;

                foreach (var name in bodyRow.Names)
                {
                    worksheet.Cells[rowIndex, columnIndex].Value = name;
                    worksheet.Cells[rowIndex, columnIndex].Style.WrapText = true;
                    worksheet.Cells[rowIndex, columnIndex++].Style.Border
                        .BorderAround(ExcelBorderStyle.Thin);
                }

                foreach (var value in bodyRow.Values)
                {
                    worksheet.Cells[rowIndex, columnIndex].Value = value;
                    worksheet.Cells[rowIndex, columnIndex].Style.HorizontalAlignment =
                        ExcelHorizontalAlignment.Center;
                    worksheet.Cells[rowIndex, columnIndex++].Style.Border
                        .BorderAround(ExcelBorderStyle.Thin);
                }

                rowIndex++;
            }

            worksheet.Cells[fromRow, fromColumn, toRow, toColumn]
                .Style
                .Numberformat
                .Format = "#0%";

            if (colorBodyCells)
            {
                var conditionalFormattingRule01 =
                    worksheet.ConditionalFormatting.AddExpression(worksheet.Cells[fromRow, fromColumn,
                        toRow, toColumn]);
                conditionalFormattingRule01.Formula = $"({ReportService.NumberToString(fromColumn, true)}{fromRow})>1";
                conditionalFormattingRule01.Style.Fill.BackgroundColor.Color = Color.LightGreen;

                var conditionalFormattingRule02 =
                    worksheet.ConditionalFormatting.AddExpression(worksheet.Cells[fromRow, fromColumn,
                        toRow, toColumn]);
                conditionalFormattingRule02.Formula = $"({ReportService.NumberToString(fromColumn, true)}{fromRow})<1";
                conditionalFormattingRule02.Style.Fill.BackgroundColor.Color = Color.LightCoral;
            }

            return rowIndex;
        }
    }
}