﻿using System;
using Audits.Models.Reports.Data;
using Audits.Models.Reports.Data.Chart;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;

namespace Audits.Models.Reports.Render
{
    public static class ChartRenderService
    {
        public static void RenderChart(ReportData reportData, ExcelWorksheet worksheet, int rowIndex)
        {
            var chart = worksheet.Drawings.AddChart("crtExtensionsSize", reportData.Chart.Type);
            SetChartData(chart, reportData, worksheet);

            chart.Title.Text = reportData.Chart.Title;
            chart.Title.Font.Size = 14;
            chart.Title.Font.Bold = true;
            chart.Border.Fill.Style = eFillStyle.NoFill;
            chart.DisplayBlanksAs = eDisplayBlanksAs.Gap;

            SetChartSizeAndPosition(rowIndex, chart, reportData);

            if (chart.YAxis != null)
                SetChartYAxisProperties(chart.YAxis);
            if (chart.XAxis != null)
                SetChartXAxisProperties(chart.XAxis);
        }

        private static void SetChartData(ExcelChart chart, ReportData data, ExcelWorksheet worksheet)
        {
            switch (data.Chart.Data)
            {
                case SelectedData.Column:
                    if (data.Chart.Type == eChartType.BarClustered || data.Chart.Type == eChartType.BarStacked)
                    {
                        SetColumnBarData(chart as ExcelBarChart, data, worksheet);
                    }
                    break;
                case SelectedData.Row:
                    if (data.Chart.Type == eChartType.BarClustered || data.Chart.Type == eChartType.BarStacked)
                    {
                        SetRowBarData(chart as ExcelBarChart, data, worksheet);
                    }
                    if (data.Chart.Type == eChartType.RadarMarkers || data.Chart.Type == eChartType.Radar)
                    {
                        SetRowRadarData(chart as ExcelRadarChart, data, worksheet);
                    }
                    if (data.Chart.Type == eChartType.Pie3D)
                    {
                        SetRowPieData(chart as ExcelPieChart, data, worksheet);
                    }
                    break;
                case SelectedData.Both:
                    SetAllData(chart, data, worksheet);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void SetRowBarData(ExcelBarChart chart, ReportData data, ExcelWorksheet worksheet)
        {
            var bodyData = data.Body;
            var startColumnIndex = bodyData.NamesNumber + 1;
            var endColumnIndex = startColumnIndex + bodyData.ValuesNumber - 1;
            var lastNameIndex = bodyData.NamesNumber;
            var rowIndex = data.Chart.DataIndex + 3;

            for (var i = startColumnIndex; i <= endColumnIndex; i++)
            {
                var serie = chart.Series.Add(
                    ExcelCellBase.GetAddress(rowIndex, i), //values
                    ExcelCellBase.GetAddress(rowIndex, lastNameIndex) //signs
                );
                serie.Header = worksheet.Cells[2, i].Value.ToString();
            }
        }

        private static void SetRowPieData(ExcelPieChart chart, ReportData data, ExcelWorksheet worksheet)
        {
            var bodyData = data.Body;
            var startColumnIndex = bodyData.NamesNumber + 1;
            var endColumnIndex = startColumnIndex + bodyData.ValuesNumber - 1;
            var lastNameIndex = bodyData.NamesNumber;
            var rowIndex = data.Chart.DataIndex + 3;

            for (var i = startColumnIndex; i <= endColumnIndex; i++)
            {
                var serie = chart.Series.Add(
                    ExcelCellBase.GetAddress(rowIndex, i), //values
                    ExcelCellBase.GetAddress(rowIndex, lastNameIndex) //signs
                );
                serie.Header = worksheet.Cells[rowIndex, lastNameIndex].Value.ToString();
            }
        }

        private static void SetColumnBarData(ExcelBarChart chart, ReportData data, ExcelWorksheet worksheet)
        {
            var bodyData = data.Body;
            var subdivisionNameColumnIndex = bodyData.NamesNumber;
            var lastNameIndex = data.Body.NamesNumber;
            var rowsNumber = bodyData.RowsNumber;
            var columnIndex = data.Chart.DataIndex + lastNameIndex + 1;

            for (var rowsCount = 0; rowsCount < rowsNumber; rowsCount++)
            {
                var curentPos = 3 + rowsCount;
                var serie = chart.Series.Add(
                    ExcelCellBase.GetAddress(curentPos, columnIndex), //values
                    ExcelCellBase.GetAddress(curentPos, lastNameIndex) //signs
                );
                serie.Header = worksheet.Cells[curentPos, subdivisionNameColumnIndex].Text;
            }
        }

        private static void SetRowRadarData(ExcelRadarChart chart, ReportData data, ExcelWorksheet worksheet)
        {
            var bodyData = data.Body;
            var startColumnIndex = bodyData.NamesNumber + 1;
            var endColumnIndex = startColumnIndex + bodyData.ValuesNumber - 1;
            var rowIndex = data.Chart.DataIndex + 3;

            var serie = chart.Series.Add(
                ExcelCellBase.GetAddress(rowIndex, startColumnIndex, rowIndex, endColumnIndex),
                ExcelCellBase.GetAddress(2, startColumnIndex, 2, endColumnIndex)
                );
            serie.Header = worksheet.Cells[rowIndex, data.Body.NamesNumber].Text;
        }

        private static void SetAllData(ExcelChart chart, ReportData data, ExcelWorksheet worksheet)
        {
            var bodyData = data.Body;
            var subdivisionNameColumnIndex = bodyData.NamesNumber;
            var startColumnIndex = subdivisionNameColumnIndex + 1;
            var endColumnIndex = subdivisionNameColumnIndex + bodyData.ValuesNumber;
            var rowsNumber = bodyData.RowsNumber;

            const int chartStartPosition = 3;


            var subdivisionCount = 0;
            for (var rowsCount = 0; rowsCount < rowsNumber; rowsCount++)
            {
                var curentPos = chartStartPosition + subdivisionCount++;
                var serie = chart.Series.Add(
                    ExcelCellBase.GetAddress(curentPos, startColumnIndex, curentPos, endColumnIndex), //values
                    ExcelCellBase.GetAddress(2, startColumnIndex, 2, endColumnIndex) //signs
                );
                serie.Header = worksheet.Cells[curentPos, subdivisionNameColumnIndex].Text;
            }
        }

        private static void SetChartXAxisProperties(ExcelChartAxis xAxis)
        {
            xAxis.Font.Size = 8;
            xAxis.Font.Bold = true;
        }

        private static void SetChartYAxisProperties(ExcelChartAxis yAxis)
        {
            yAxis.MaxValue = 1.0;
            yAxis.MinValue = 0;
            yAxis.MajorUnit = 0.1;
            yAxis.MinorUnit = 0.1;
            yAxis.MajorTickMark = eAxisTickMark.None;
            yAxis.MinorTickMark = eAxisTickMark.None;
            yAxis.Crosses = eCrosses.AutoZero;
            yAxis.CrossBetween = eCrossBetween.Between;
            yAxis.CrossesAt = 87114112;
            yAxis.Format = "#0%";
        }

        private static void SetChartSizeAndPosition(int rowIndex, ExcelChart chart, ReportData data)
        {
            chart.SetPosition(rowIndex, 0, 0, 0);

            var worksheet = chart.WorkSheet;

            var width = 0;
            int columnIndex;
            var columnNumber = data.Header.ColumnNumber + 4;
            for (columnIndex = 1; columnIndex <= columnNumber; columnIndex++)
            {
                width += ExcelHelper.ColumnWidth2Pixel(worksheet, worksheet.Column(columnIndex).Width);
            }

            var height = 0;
            var rowNumber = data.Body.RowsNumber + 2 + 7;
            for (rowIndex = 2; rowIndex <= rowNumber; rowIndex++)
            {
                height += ExcelHelper.RowHeight2Pixel(worksheet.Row(rowIndex).Height);
            }
            
            chart.SetSize(width, height);
        }
    }
}