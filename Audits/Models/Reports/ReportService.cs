﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Audits.Models.Database_Manager;

namespace Audits.Models.Reports
{
    public static class ReportService
    {
        public static double CalculateLevelOfComplianceForSectionForPeriod(Subdivision subdivision, DateTime startDate,
            DateTime endDate,
            Section section)
        {
            if (subdivision.Audits.Count == 0)
            {
                return -1;
            }

            var significanceFactor =
                subdivision.Audits.Where(a => a.Date >= startDate && a.Date <= endDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .Where(s => s.SectionID == section.ID)
                    .SelectMany(es => es.EvaluatedParameters)
                    .Select(ep => ep.Parameter.SignificanceFactor)
                    .Sum(ep => ep);

            var maxSignificanceFactor =
                subdivision.Audits.Where(a => a.Date >= startDate && a.Date <= endDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .Where(s => s.SectionID == section.ID)
                    .SelectMany(p => p.Section.Parameters)
                    .Select(p => p.SignificanceFactor)
                    .Sum(p => p);

            return (double) significanceFactor / maxSignificanceFactor;
        }

        public static List<Subdivision> GetSubdivisionChildren(Subdivision subdivision)
        {
            var subdivisions = new List<Subdivision>();
            if (subdivision.Children.Count <= 0)
            {
                return subdivisions;
            }

            foreach (var subdivisionChild in subdivision.Children)
            {
                subdivisions.AddRange(GetSubdivisionChildren(subdivisionChild));
            }
            subdivisions.AddRange(subdivision.Children);
            return subdivisions.Where(s => s.Children.Count == 0).ToList();
        }

        public static double CalculateValidationLevel(Subdivision subdivision, DateTime startDate, DateTime endDate,
            Section section)
        {
            if (subdivision.Audits.Count == 0)
            {
                return -1;
            }

            var grade =
                subdivision.Audits.Where(a => a.Date >= startDate && a.Date <= endDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .Where(s => s.SectionID == section.ID)
                    .SelectMany(es => es.EvaluatedParameters)
                    .Select(ep => ep.Value)
                    .Sum(ep => ep);

            var validatedGrade =
                subdivision.Audits
                    .Where(a => a.Date >= startDate && a.Date <= endDate)
                    .Select(a => a.ValidatedAudit)
                    .SelectMany(a => a.EvaluatedSections)
                    .Where(s => s.SectionID == section.ID)
                    .SelectMany(es => es.EvaluatedParameters)
                    .Select(ep => ep.Value)
                    .Sum(ep => ep);


            return (double) validatedGrade / grade;
        }

        public static double CalculateExecutionLevel(Subdivision subdivision, DateTime startDate, DateTime endDate,
            Section section)
        {
            if (subdivision.Audits.Count == 0)
            {
                return -1;
            }

            var executed =
                subdivision.Audits.Where(a => a.InputDate <= endDate && a.InputDate >= startDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .Count(es => es.SectionID == section.ID);

            var planned =
                subdivision.Audits.Where(a => a.Date <= endDate && a.Date >= startDate)
                    .SelectMany(a => a.Checklist.Sections)
                    .Count(s => s.ID == section.ID);

            return (double) planned / executed;
        }

        public static double CalculateOverallExecutionLevel(Subdivision subdivision, DateTime startDate,
            DateTime endDate)
        {
            if (subdivision.Audits.Count == 0)
            {
                return -1;
            }

            var executed =
                subdivision.Audits.Where(a => a.InputDate <= endDate && a.InputDate >= startDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .Count();

            var planned =
                subdivision.Audits.Where(a => a.Date <= endDate && a.Date >= startDate)
                    .SelectMany(a => a.Checklist.Sections)
                    .Count();

            return (double) planned / executed;
        }

        public static double CalculateOverallSectionsCompliance(Subdivision subdivision, DateTime startDate,
            DateTime endDate)
        {
            if (subdivision.Audits.Count == 0)
            {
                return -1;
            }

            var significanceFactor =
                subdivision.Audits.Where(a => a.Date >= startDate && a.Date <= endDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .SelectMany(es => es.EvaluatedParameters)
                    .Select(p => p.Parameter.SignificanceFactor)
                    .Sum(p => p);

            var maxSignificanceFactor =
                subdivision.Audits
                    .Where(a => a.Date >= startDate && a.Date <= endDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .Select(es => es.Section)
                    .SelectMany(s => s.Parameters)
                    .Select(p => p.SignificanceFactor)
                    .Sum(ep => ep);

            return (double) significanceFactor / maxSignificanceFactor;
        }

        public static List<Section> GetAvailableSections(IEnumerable<Subdivision> subdivisions, DateTime startDate,
            DateTime endDate)
        {
            return
                subdivisions.SelectMany(c => c.Audits)
                    .Where(a => a.Date >= startDate && a.Date <= endDate)
                    .SelectMany(a => a.EvaluatedSections)
                    .Select(es => es.Section)
                    .GroupBy(s => s.ID)
                    .ToDictionary(s => s.Key.ToString(), s => s.First())
                    .Select(d => d.Value)
                    .ToList();
        }

        public static string NumberToString(int number, bool isCaps)
        {
            var c = (char) ((isCaps ? 65 : 97) + (number - 1));
            return c.ToString();
        }

        public static string GetReportFileName(bool asPdf)
        {
            return "Отчет_" + GetFileSuffix(asPdf);
        }

        public static string GetFileSuffix(bool asPdf)
        {
            var ext = (!asPdf) ? ".xlsx" : ".pdf";
            return DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss.fff") + ext;
        }

        public static string GetFileSuffix(FileInfo file = null)
        {
            var ext = (file == null) ? ".xlsx" : Path.GetExtension(file.Name);
            return DateTime.Now.ToString("yyyy-MM-dd_HH.mm.ss.fff") + ext;
        }
    }
}