using System;
using System.IO;
using Audits.Models.Reports.Data;
using Audits.Models.Reports.Render;
using OfficeOpenXml;
using Spire.Xls;

namespace Audits.Models.Reports
{
    public class ReportGenerator
    {
        public int CurrentRow { get; set; } = 1;
        public bool ColorBodyCells { get; set; }
        private ExcelWorksheet Worksheet { get; set; }
        private ReportData ReportData { get; }

        public ReportGenerator(ReportData reportData, bool colorBodyCells = false)
        {
            ReportData = reportData;
            ColorBodyCells = colorBodyCells;
        }

        public byte[] Generate(bool asPdf = false)
        {
            
            var stream = new MemoryStream();
            using (var package = new ExcelPackage(stream))
            {
                Worksheet =
                    package.Workbook.Worksheets.Add($"{ReportData.Header.Title} - " + DateTime.Now.ToShortDateString());

                CurrentRow = HeaderRenderService.RenderHead(ReportData, Worksheet, CurrentRow);
                CurrentRow = BodyRenderService.RenderBody(ReportData, Worksheet, CurrentRow, ColorBodyCells);
                if (ReportData.Chart != null)
                {
                    ChartRenderService.RenderChart(ReportData, Worksheet, CurrentRow);
                }

                InitSettings();
                Worksheet.PrinterSettings.FitToPage = true;
                package.Save();
            }
            var bytes = stream.ToArray();
            return asPdf ? SaveAsPdf(bytes) : bytes;
        }

        private void InitSettings()
        {
            Worksheet.DefaultRowHeight = 13.8;
            Worksheet.DefaultColWidth = 9.109375;

            Worksheet.Column(1).Width = 4;
            Worksheet.Column(2).Width = 30;
            Worksheet.Column(3).Width = 30;
            Worksheet.Column(4).Width = 30;

            var lastColumn = ReportData.Header.ColumnNumber + 4;
            for (var column = 5; column <= lastColumn; column++)
            {
                Worksheet.Column(column).Width = 16.6;
            }

            var rowCount = 2 + ReportData.Body.RowsNumber;
            for (var row = 1; row <= rowCount; row++)
            {
                Worksheet.Row(row).CustomHeight = false;
            }
        }

        private static byte[] SaveAsPdf(byte[] bytes)
        {
            try
            {
                var workbook = new Workbook();
                var stream = new MemoryStream(bytes);
                workbook.LoadFromStream(stream);
                var pdfStream = new MemoryStream();
                workbook.SaveToStream(pdfStream, FileFormat.PDF);
                return pdfStream.ToArray();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}