﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Audits.Models.Database_Manager
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DatabaseEntities : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<EvaluatedParameter> EvaluatedParameters { get; set; }
        public virtual DbSet<Parameter> Parameters { get; set; }
        public virtual DbSet<Subdivision> Subdivisions { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<Section> Sections { get; set; }
        public virtual DbSet<EvaluatedSection> EvaluatedSections { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Checklist> Checklists { get; set; }
        public virtual DbSet<Audit> Audits { get; set; }
        public virtual DbSet<TimetableRecord> TimetableRecords { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
    }
}
