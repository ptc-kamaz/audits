﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Audits.Models.Subdivisions;

namespace Audits.Models.Database_Manager
{
    public partial class DatabaseEntities
    {
        public DatabaseEntities()
            : base("name=DatabaseEntities")
        {
            Subdivisions.Local.CollectionChanged += SubdivisionsCollectionChanged;
            Employees.Local.CollectionChanged += EmployeesCollectionChanged;
        }

        private void EmployeesCollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var subdivisionCache = SubdivisionCache.GetInstance();
            if (e != null && e.OldItems != null)
            {
                foreach (var oldItem in e.OldItems)
                {
                    var item = oldItem as Employee;
                    if (item != null)
                    {
                        subdivisionCache.DiscardForEmployee(item.AD_ID);
                    }
                }
            }
        }

        private void SubdivisionsCollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            var subdivisionCache = SubdivisionCache.GetInstance();
            subdivisionCache.Discard();
        }
    }

    public partial class Audit : IEquatable<Audit>
    {
        public bool Equals(Audit other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(ChecklistID, other.ChecklistID)
                   && Equals(Date, other.Date)
                   && Equals(InputDate, other.InputDate)
                   && Equals(AuthorID, other.AuthorID)
                   && Equals(SubdivisionID, other.SubdivisionID)
                   && Equals(IsEditable, other.IsEditable)
                   && Equals(ValidatedAuditID, other.ValidatedAuditID)
                   && Equals(TimetableRecordID, other.TimetableRecordID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Audit);
        }
    }

    public partial class Checklist : IEquatable<Checklist>
    {
        public bool Equals(Checklist other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(Name, other.Name);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Checklist);
        }
    }

    public partial class Employee : IEquatable<Employee>
    {
        public bool Equals(Employee other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(LastName, other.LastName)
                   && Equals(FirstName, other.FirstName)
                   && Equals(Patronymic, other.Patronymic)
                   && Equals(SubdivisionID, other.SubdivisionID)
                   && Equals(EstablishedPositionID, other.EstablishedPositionID)
                   && Equals(EstablishedPosition, other.EstablishedPosition)
                   && Equals(ActualPosition, other.ActualPosition)
                   && Equals(Email, other.Email)
                   && Equals(Phone, other.Phone)
                   && Equals(PersonnelNumber, other.PersonnelNumber)
                   && Equals(AD_ID, other.AD_ID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Employee);
        }
    }

    public partial class EvaluatedParameter : IEquatable<EvaluatedParameter>
    {
        public bool Equals(EvaluatedParameter other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(Value, other.Value)
                   && Equals(EvaluatedSectionID, other.EvaluatedSectionID)
                   && Equals(ParameterID, other.ParameterID)
                   && Equals(Actions, other.Actions);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as EvaluatedParameter);
        }
    }

    public partial class EvaluatedSection : IEquatable<EvaluatedSection>
    {
        public bool Equals(EvaluatedSection other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(AuditID, other.AuditID)
                   && Equals(Grade, other.Grade)
                   && Equals(MaxGrade, other.MaxGrade)
                   && Equals(SectionID, other.SectionID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as EvaluatedSection);
        }
    }

    public partial class Parameter : IEquatable<Parameter>
    {
        public bool Equals(Parameter other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(Name, other.Name)
                   && Equals(OrdinalNumber, other.OrdinalNumber)
                   && Equals(SectionID, other.SectionID)
                   && Equals(MaxValue, other.MaxValue)
                   && Equals(SignificanceFactor, other.SignificanceFactor);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Parameter);
        }
    }

    public partial class Permission : IEquatable<Permission>
    {
        public bool Equals(Permission other)
        {
            if (ReferenceEquals(other, this))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID) && Equals(Name, other.Name);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Permission);
        }
    }

    public partial class Photo : IEquatable<Photo>
    {
        public bool Equals(Photo other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return ID == other.ID
                   && Equals(Location, other.Location)
                   && SectionID == other.SectionID;
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Photo);
        }
    }

    public partial class Role : IEquatable<Role>
    {
        public bool Equals(Role other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(Name, other.Name)
                   && Permissions.SequenceEqual(other.Permissions);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Role);
        }
    }

    public partial class Section : IEquatable<Section>
    {
        public bool Equals(Section other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(Name, other.Name)
                   && Equals(OrdinalNumber, other.OrdinalNumber)
                   && Equals(ChecklistID, other.ChecklistID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Section);
        }
    }


    public partial class Subdivision : IEquatable<Subdivision>
    {
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Subdivision(int id, int? parentId, string name, int? headId, string sapId) : this()
        {
            ID = id;
            ParentID = parentId;
            Name = name;
            HeadID = headId;
            SAP_ID = sapId;
        }

        public bool Equals(Subdivision other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(Name, other.Name)
                   && Equals(Head, other.Head)
                   && Equals(Parent, other.Parent)
                   && Equals(SAP_ID, other.SAP_ID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Subdivision);
        }
    }

    public partial class TimetableRecord : IEquatable<TimetableRecord>
    {
        public bool Equals(TimetableRecord other)
        {
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            if (other == null)
            {
                return false;
            }
            return Equals(ID, other.ID)
                   && Equals(Date, other.Date)
                   && Equals(ChecklistID, other.ChecklistID)
                   && Equals(SubdivisionID, other.SubdivisionID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TimetableRecord);
        }
    }
}