﻿using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Audits.Models.EmployeesManager;
using NLog;

namespace Audits.Models.Security
{
    /// <summary>
    ///     HasPermissionAttribute validates whether provided user has access
    ///     rights to the method or not.
    /// </summary>
    public sealed class HasPermissionAttribute : AuthorizeAttribute
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public string Permission { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var identity = httpContext.User.Identity as WindowsIdentity;
            var hasPermission = RoleDepository.HasPermission(identity, Permission);
            if (!hasPermission)
            {
                Logger.Error("User {" + httpContext.GetUserAdId() + "} tried to access resources with '"
                    + Permission + "' permission, which he/she does not possess.");
            }

            return hasPermission;
        }
    }
}