﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Audits.Models.Database_Manager;

namespace Audits.Models.Security
{
    /// <summary>
    ///     RoleDepository contains uploaded roles and assigned permissions in memory
    ///     in order to reduce amount of requests to the database.
    /// </summary>
    public static class RoleDepository
    {
        private static readonly object LockObj = new object();
        public static ConcurrentDictionary<string, ISet<string>> _permissions;

        static RoleDepository()
        {
            LoadRoles();
        }

        public static void LoadRoles()
        {
            lock (LockObj)
            {
                _permissions?.Clear();
                _permissions = new ConcurrentDictionary<string, ISet<string>>();
                using (var db = new DatabaseEntities())
                {
                    foreach (var role in db.Roles)
                    {
                        _permissions.TryAdd(role.Name,
                            new HashSet<string>(role.Permissions.Select(p => p.Name.ToLower())));
                    }
                }
            }
        }

        public static bool HasPermission(WindowsIdentity identity, string permission)
        {
            if (identity == null || permission == null)
            {
                return false;
            }
            var role = GetUserRole(identity);
            return HasPermission(role, permission);
        }

        public static bool HasPermission(Role role, string permission)
        {
            if (role == null || permission == null || !_permissions.ContainsKey(role.Name))
            {
                return false;
            }
            return _permissions[role.Name].Contains(permission.ToLower());
        }

        public static bool HasPermision(int id, string permission)
        {
            using (var db = new DatabaseEntities())
            {
                var employee = db.Employees.Include("Role.Permissions").FirstOrDefault(e => e.ID == id);
                if (employee == null)
                    return false;
                var role = employee?.Role.FirstOrDefault();
                if (role == null)
                    return false;
                return HasPermission(role, permission);
            }
        }

        public static Role GetUserRole(WindowsIdentity identity)
        {
            if (identity == null || identity.User == null)
            {
                return null;
            }
            using (var db = new DatabaseEntities())
            {
                var employee = db.Employees.Include("Role.Permissions")
                    .FirstOrDefault(e => string.Equals(e.AD_ID, identity.User.Value));
                var role = employee?.Role.FirstOrDefault();
                return role;
            }
        }
    }
}