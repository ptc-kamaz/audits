﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Audits.Models.Database_Manager;

namespace Audits.Models.Timetable
{
    public class TimetableService
    {
        private readonly HashSet<int> _subdivisionIds;

        public TimetableService(IEnumerable<int> subdivisionIds)
        {
            if (subdivisionIds == null)
            {
                throw new ArgumentNullException(nameof(subdivisionIds));
            }

            _subdivisionIds = new HashSet<int>(subdivisionIds);
        }

        public TimetableRecord[] LoadRecords(int year, int month, int day)
        {
            if (year < 1900 || year > 3000)
            {
                throw new ArgumentOutOfRangeException(nameof(year));
            }
            if (month < 1 || month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(year));
            }
            if (day < 1 || day > DateTime.DaysInMonth(year, month))
            {
                throw new ArgumentOutOfRangeException(nameof(year));
            }

            using (var db = new DatabaseEntities())
            {
                var records = db
                    .TimetableRecords
                    .Where(x => x.Date.Year == year
                                && x.Date.Month == month
                                && x.Date.Day == day)
                    .Include(x => x.Subdivision)
                    .Include(x => x.Checklist)
                    .ToArray();

                return records
                    .Where(x => _subdivisionIds.Contains(x.SubdivisionID))
                    .ToArray();
            }
        }

        public bool[] DaysWithRecords(int year, int month)
        {
            if (year < 1900 || year > 3000)
            {
                throw new ArgumentOutOfRangeException(nameof(year));
            }
            if (month < 1 || month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(year));
            }

            var daysInMonth = DateTime.DaysInMonth(year, month);
            var result = new bool[daysInMonth];

            using (var db = new DatabaseEntities())
            {
                var monthlyRecords = db.TimetableRecords
                    .Where(x => x.Date.Year == year && x.Date.Month == month)
                    .ToArray();

                var timetableRecords = monthlyRecords
                    .Where(x => _subdivisionIds.Contains(x.SubdivisionID))
                    .ToArray();

                for (var i = 1; i <= daysInMonth; i++)
                {
                    result[i - 1] = timetableRecords.Any(x => x.Date.Day == i);
                }
            }

            return result;
        }
    }
}