using System;
using System.Collections.Generic;

namespace Audits.Models.Audits
{
    internal static class AuditExportCache
    {
        private static readonly Dictionary<string, Record> _cache;

        static AuditExportCache()
        {
            _cache = new Dictionary<string, Record>();
        }

        public static byte[] Find(string auditName)
        {
            lock (_cache)
            {
                Record record;
                if (!_cache.TryGetValue(auditName, out record))
                {
                    return null;
                }

                var timeSpan = DateTime.Now - record.Timestamp;
                if (timeSpan.Minutes < 30)
                {
                    return record.Data;
                }

                _cache.Remove(auditName);

                return null;
            }
        }

        public static void UpdateRecord(string auditName, byte[] data)
        {
            lock (_cache)
            {
                InvalidateRecord(auditName);

                _cache.Add(auditName, new Record(data));
            }
        }

        public static void InvalidateRecord(string checklistName)
        {
            lock (_cache)
            {
                Record record;
                if (_cache.TryGetValue(checklistName, out record))
                {
                    _cache.Remove(checklistName);
                }
            }
        }

        private class Record
        {
            public Record(byte[] data)
            {
                Timestamp = DateTime.Now;
                Data = data;
            }

            public DateTime Timestamp { get; }
            public byte[] Data { get; }
        }
    }
}