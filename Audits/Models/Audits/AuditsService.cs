﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Data.Entity;
using Audits.Models.Database_Manager;
using System.Web.Mvc;
using System.Web;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Text.RegularExpressions;
using Audits.Models.Subdivisions;
using Audits.Models.EmployeesManager;
using System;
using System.Data;
using System.Security.Principal;
using System.Web;
using Audits.Models.Security;

namespace Audits.Models.Audits
{
    public class AuditsService
    {
        private readonly DatabaseEntities _db;

        // Audit db model
        public IEnumerable<Audit> Model { get; set; }

        // Paging data
        public int CurrPage { get; set; }       // current number of page
        public int PageSize { get; set; }       // number of entities on the page
        public int TotalPages { get; set; }     // max number of pages within pagination pane
        public int PagesInRange { get; set; }   // number of pages you can jump-on on pagination panel
        public int TotalEntities { get; set; }  // total audit entities in db
        public int PaginationRange { get; set; }// range of page buttons shown on paging panel

        // Map of validated audits
        public HashSet<int> ValidatedAudits { get; set; }

        // Contructor
        public AuditsService()
        {
            _db = new DatabaseEntities();
            this.ValidatedAudits = new HashSet<int>();
            this.PageSize = 15;
            this.PagesInRange = 10;
        }

        public Audit GetDetails(int? ID)
        {
            if (ID.HasValue)
            {
                return _db.Audits.Find(ID);
            }
            return null;
        }

        public Audit ValidationPrepare(int id)
        {
            var audit = GetDetails(id);
            if (audit != null)
            {
                audit.Date = DateTime.Now;
            }
            else
            {
                return null;
            }

            return audit;
        }

        public bool Validate(Audit audit, AuditValidate values)
        {
            var currentAudit = audit;
            if (currentAudit != null && values.EvaluatedValues.Count > 0)
            {
                try
                {
                    //Find current user info
                    var currentUser = GetUserDetails(WindowsIdentity.GetCurrent());
                    var currentAuditor = _db.Employees.FirstOrDefault(a => a.ID == currentUser.ID);
                    //Fill base fields of validated audit
                    //create validated audit without evaluated sections
                    currentAudit.ValidatedAudit = new Audit
                    {
                        AuthorID = currentAuditor.ID,
                        ChecklistID = currentAudit.ChecklistID,
                        InputDate = currentAudit.InputDate,
                        Date = values.DateTime,
                        SubdivisionID = currentAuditor.SubdivisionID,
                        IsEditable = currentAudit.IsEditable,
                        Attendees = values.Attendees
                    };
                    _db.SaveChanges();
                    //fill new validated audit with evaluated sections
                    currentAudit = GetDetails(audit.ID);
                    currentAudit.ValidatedAudit.EvaluatedSections = new List<EvaluatedSection>();
                    foreach (var section in currentAudit.EvaluatedSections)
                    {
                        var newGrade = 0;
                        var evaParams = new List<EvaluatedParameter>();
                        foreach (var parameter in section.EvaluatedParameters)
                        {
                            foreach (var value in values.EvaluatedValues)
                            {
                                if (value.ParameterID == parameter.ParameterID)
                                {
                                    //Check for null value
                                    if (
                                        !value.ActualValue.HasValue
                                        || value.ActualValue > parameter.Parameter.MaxValue
                                        )
                                    {
                                        return false;
                                    }

                                    //Add if not null
                                    evaParams.Add(new EvaluatedParameter
                                    {
                                        Actions = value.Actions,
                                        ParameterID = value.ParameterID,
                                        Value = (byte)value.ActualValue,
                                        EvaluatedSectionID = value.SectionID
                                    });

                                    newGrade += (int)value.ActualValue;
                                    break;
                                }
                            }
                        }
                        var evaSec = new EvaluatedSection
                        {
                            AuditID = (int)currentAudit.ValidatedAuditID,
                            SectionID = section.SectionID,
                            MaxGrade = section.MaxGrade,
                            Grade = newGrade,
                            EvaluatedParameters = evaParams
                        };
                        currentAudit.ValidatedAudit.EvaluatedSections.Add(evaSec);
                    }
                    _db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        public static Employee GetUserDetails(WindowsIdentity identity)
        {
            if (identity == null || identity.User == null)
            {
                return null;
            }
            using (var db = new DatabaseEntities())
            {
                var employee = db.Employees.FirstOrDefault(e => string.Equals(e.AD_ID, identity.User.Value));
                return employee;
            }
        }

        public bool FilterKrpsAudits(Audit a)
        {
            if (RoleDepository.HasPermision(a.AuthorID, "ReadAuditsKrps"))
                return false;
            else
                return true;
        }

        public IQueryable<Audit> GetAuditsByPermission(WindowsIdentity identity, string AD_Id)
        {
            var user = GetUserDetails(identity);
            // get all audits
            var data = _db.Audits 
                .Include(a => a.Checklist)
                .Include(a => a.Subdivision)
                .Include(a => a.Auditor);

            // get child subdivisions of current identity
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions(AD_Id).AsEnumerable();
            var subdivisionsIds = subdivisions.Select(s => s.ID);

            // "Auditor" role
            if (!RoleDepository.HasPermission(identity, "ReadAuditsTrusted") && !RoleDepository.HasPermission(identity, "ReadAuditsKrps"))
            {
                return data
                    .Where(a => subdivisionsIds.Contains(a.SubdivisionID)) // show only subordiate divisions
                    .Where(a => a.AuthorID == user.ID); // created only by the user
            }

            // "Trusted Auditor" role
            if (RoleDepository.HasPermission(identity, "ReadAuditsTrusted") && !RoleDepository.HasPermission(identity, "ReadAuditsKrps"))
            {
                var dataList = data.Where(a => subdivisionsIds.Contains(a.SubdivisionID)).ToList(); // get list of only subordiate divisions
                return dataList.FindAll(FilterKrpsAudits).AsQueryable(); // exclude audits held by KRPS auditors 
            }

            // "KRPS Auditor" role
            if (RoleDepository.HasPermission(identity, "ReadAuditsTrusted") && RoleDepository.HasPermission(identity, "ReadAuditsKrps"))
            {
                return data;
            }

            // else error
            return null;
        }

        public IQueryable<Audit> ExcludeValidatedAudits(IQueryable<Audit> data)
        {
            // form list of "child" audits validated by "parent" audit
            var toExclude = data.Where(a => a.ValidatedAuditID.HasValue);

            // form list of validated audits to detect them in View
            // ID:1 -> ID:2, ID:2 -> ID:3, where 1 should be displayed, 2 & 3 excluded
            var toExcludeList = new List<int>();
            foreach (var a in toExclude)
            {
                if (a.ValidatedAuditID != null)
                {
                    toExcludeList.Add((int)a.ValidatedAuditID);
                }
                ValidatedAudits.Add(a.ID);
            }

            // store to property
            this.ValidatedAudits.ExceptWith(toExcludeList);

            // exclude "child" audits
            return data.Where(a => !toExcludeList.Contains(a.ID)); //data = 
        }

        public IQueryable<Audit> SortAudits(IQueryable<Audit> data, string by)
        {
            // pre-sort
            switch (by)
            {
                // checklist name
                case "cname":
                    data = data.OrderBy(a => a.Checklist.Name);
                    break;
                case "cname_desc":
                    data = data.OrderByDescending(a => a.Checklist.Name);
                    break;

                // division name
                case "div":
                    data = data.OrderBy(a => a.Subdivision.Name);
                    break;
                case "div_desc":
                    data = data.OrderByDescending(a => a.Subdivision.Name);
                    break;

                // ФИО
                case "fname":
                    data = data.OrderBy(a => a.Auditor.FirstName);
                    break;
                case "fname_desc":
                    data = data.OrderByDescending(a => a.Auditor.FirstName);
                    break;

                case "lname":
                    data = data.OrderBy(a => a.Auditor.LastName);
                    break;
                case "lname_desc":
                    data = data.OrderByDescending(a => a.Auditor.LastName);
                    break;

                case "pname":
                    data = data.OrderBy(a => a.Auditor.Patronymic);
                    break;
                case "pname_desc":
                    data = data.OrderByDescending(a => a.Auditor.Patronymic);
                    break;

                // date filled
                case "date":
                    data = data.OrderBy(a => a.InputDate);
                    break;
                case "date_desc":
                    data = data.OrderByDescending(a => a.InputDate);
                    break;

                // validated or not
                case "valid":
                    data = data.OrderBy(a => !this.ValidatedAudits.Contains(a.ID)).ThenBy(a => a.ID);
                    break;
                case "valid_desc":
                    data = data.OrderByDescending(a => !this.ValidatedAudits.Contains(a.ID)).ThenBy(a => a.ID);
                    break;

                // just id
                case "id_desc":
                    data = data.OrderByDescending(a => a.ID);
                    break;
                default: // "id"
                    data = data.OrderBy(a => a.ID);
                    break;
            }
            return data;
        }

        public IEnumerable<Audit> ToPagedList(IQueryable<Audit> data, int currPage)
        {
            // count num of pages & store the data
            double totalEntities = data.Count();
            this.TotalPages = (int)Math.Ceiling(totalEntities / this.PageSize);
            this.CurrPage = currPage;
            this.TotalEntities = (int)totalEntities;
            return data
                .Skip((currPage - 1) * this.PageSize) // -> skip etities * TotalPages 
                .Take(this.PageSize) // -> take entities amount of PageSize         
                .ToList(); // was audits
        }

        public void LoadAudits(int currPage, string sortType, int pagiRange, string ADID, WindowsIdentity identity)
        {
            var data = GetAuditsByPermission(identity, ADID);
            data = ExcludeValidatedAudits(data);
            data = SortAudits(data, sortType);
            // store final result for passing into the view
            this.Model = ToPagedList(data, currPage);
            this.PaginationRange = pagiRange;
        }

        // Underlaying method to delete Audit by `id`
        public bool DeleteById(int id)
        {
            var audit = _db.Audits.Find(id);
            if (audit == null)
                return false;
            _db.EvaluatedParameters.RemoveRange(audit.EvaluatedSections.SelectMany(p => p.EvaluatedParameters));
            _db.EvaluatedSections.RemoveRange(audit.EvaluatedSections);
            _db.Audits.Remove(audit);
            _db.SaveChanges();
            return true;
        }

        public bool Save(DatabaseEntities _db, Audit audit)
        {
            try
            {
                if (audit.ID == 0)  // create only
                {
                    audit.Auditor = null;
                    _db.Audits.Add(audit);
                }
                else                // update
                {
                    var original = _db.Audits.Find(audit.ID); // original record from DB
                    // update its properties from altered model
                    original.InputDate = audit.InputDate;
                    original.Date = audit.Date;
                    original.Attendees = audit.Attendees;
                    original.IsEditable = audit.IsEditable;
                    original.SubdivisionID = audit.SubdivisionID;
                    original.TimetableRecordID = audit.TimetableRecordID;
                    if (original.ChecklistID != audit.ChecklistID)
                    {
                        original.ChecklistID = audit.ChecklistID;
                        _db.EvaluatedParameters.RemoveRange(original.EvaluatedSections.SelectMany(p => p.EvaluatedParameters));
                        _db.EvaluatedSections.RemoveRange(original.EvaluatedSections);
                        original.EvaluatedSections = new List<EvaluatedSection>(audit.EvaluatedSections);
                        //_db.Entry(original.EvaluatedSections).State = EntityState.Modified;
                    }
                    else
                    {
                        // go through each section [each parameter] and update values taken from POST
                        foreach (var section in original.EvaluatedSections)
                        {
                            foreach (var parameter in section.EvaluatedParameters)
                            {
                                var pp = audit.EvaluatedSections.Single(s => s.SectionID == section.SectionID)
                                    .EvaluatedParameters.Single(p => p.ParameterID == parameter.ParameterID);
                                parameter.Actions = pp.Actions;
                                parameter.Value = pp.Value;
                                _db.Entry(parameter).State = EntityState.Modified;
                            }
                            var ss = audit.EvaluatedSections.Single(s => s.SectionID == section.SectionID);
                            section.MaxGrade = ss.MaxGrade;
                            section.Grade = ss.Grade;
                            _db.Entry(section).State = EntityState.Modified;
                        }
                    }
                }
                _db.SaveChanges();  
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }

        // AddEvaluatedSections append EvaluatedSections model to `audit` from `Request`s POST data.
        // If something wrong error messages passed into `ModelState`
        public void AddEvaluatedSections(Audit audit, HttpRequestBase Request, ModelStateDictionary ModelState)
        {
            // if POST[NumberOfSections] exists - it specifies number of sections to process
            var tmpNOS = Request.Form.Get("NumberOfSections");
            if (tmpNOS == null)
            {
                ModelState.AddModelError("_", "Не удалось распознать количество секции в чек-листе (код 10)."); // error is hidden
                return;
            }

            // try parse NumberOfSections number
            int numOfSections;
            if (!int.TryParse(tmpNOS, out numOfSections))
            {
                ModelState.AddModelError("_Summary", "Не удалось распознать количество секции в чек-листе (код 11).");
                return;
            }

            // check if NumberOfSections is not 0
            if (numOfSections == 0)
            {
                ModelState.AddModelError("_Summary", "Чек-лист пуст. Количество секции должно быть больше нуля (код 12).");
                return;
            }

            // cut POST's array keys to get parameter values only
            var keys = Request.Form.AllKeys;
            keys = keys.Skip(Array.IndexOf(keys, "NumberOfSections") + 1).ToArray();

            // if form parameters are 0 or less than the number of sections itself
            if (keys.Length == 0 || keys.Length < numOfSections)
            {
                ModelState.AddModelError("_Summary", "Данные ввода чек-листа не получены (код 20).");
                return;
            }


            // go through each section of checklist
            // format:  POST["NumberOfParams.<sectionID>"] - specify number of params in the section to process
            //          POST["Value.<sectionID>.<parameterID>"] - specify EvaluatedParameter.Value
            //          POST["Action.<sectionID>.<parameterID>"] - specify EvaluatedParameter.Actions
            //          ...
            for (var i = 0; i < numOfSections; i++)
            {
                var s = new EvaluatedSection() { };
                s.EvaluatedParameters = new List<EvaluatedParameter>();

                // split "NumberOfParams.<sectionID>"
                var pKey = keys.ElementAt(0);
                string[] split = pKey.Split(new char[] { '.' });
                if (split.Length != 2)
                {
                    ModelState.AddModelError("_Summary", "Ошибка обработки данных чек-листа (код 30).");
                    break;
                }

                int SectionID = 0;
                int numberOfParams = 0;
                // parse <sectionID>
                var get = Request.Form.Get(pKey);
                if (!int.TryParse(split.ElementAt(1), out SectionID))
                {
                    ModelState.AddModelError("_Summary", "Ошибка обработки данных чек-листа (код 31).");
                    break;
                }

                // parse content of POST["NumberOfParams.<sectionID>"]
                if (!int.TryParse(get, out numberOfParams))
                {
                    ModelState.AddModelError("_Summary", "Ошибка обработки данных чек-листа (код 32).");
                    break;
                }

                // go through each parameter within the section
                for (var j = 1; j <= (numberOfParams * 2); j = j + 2)
                {
                    var p = new EvaluatedParameter() { };
                    // get POST["Value.<sectionID>.<parameterID>"] and parse 
                    var valueKey = Request.Form[keys[j]];
                    byte value;
                    if (!byte.TryParse(valueKey, out value))
                    {
                        ModelState.AddModelError("_Summary", "Ошибка обработки данных чек-листа (код 33).");
                        break;
                    }
                    // get POST["Actions.<sectionID>.<parameterID>"]
                    var act = Request.Form[keys[j + 1]];

                    // get <parameterID> from "Value.<sectionID>.<parameterID>"
                    pKey = keys[j];
                    string[] split2 = pKey.Split(new char[] { '.' });
                    int parameterID = 0;
                    if (split2.Length != 3)
                    {
                        ModelState.AddModelError("_Summary", "Ошибка обработки данных чек-листа (код 34).");
                        break;
                    }
                    // try parse <parameterID>
                    if (!int.TryParse(split2.ElementAt(2), out parameterID))
                    {
                        ModelState.AddModelError("_Summary", "Ошибка обработки данных чек-листа (код 35).");
                        break;
                    }


                    // add each formulated Parameter to model if exists
                    var dbParameter = _db.Parameters.Find(parameterID);
                    if (dbParameter == null)
                    {
                        ModelState.AddModelError("_Summary", "Ошибка обработки данных чек-листа (код 36).");
                        break;
                    }

                    // store MaxValue value to EvaluatedParameter if it's greater
                    p.Value = value > dbParameter.MaxValue ? dbParameter.MaxValue : value;
                    s.MaxGrade += dbParameter.MaxValue; // sum of max grades in the section
                    s.Grade += value; // accumulate total grades in the section
                    p.Actions = act;
                    p.ParameterID = parameterID;
                    p.EvaluatedSectionID = SectionID;
                    s.EvaluatedParameters.Add(p);
                }
                // add section
                s.SectionID = SectionID;
                audit.EvaluatedSections.Add(s);

                // remove one processed section
                keys = keys.Skip(numberOfParams * 2 + 1).ToArray();
            }
        }

        public void AddAttendees(Audit audit, HttpRequestBase Request, ModelStateDictionary ModelState)
        {
            // get POST[Attendees]
            var attends = Request.Form.Get("Attendees");
            // try parse otherwise show a error
            if (attends != null && attends != "" && !Regex.IsMatch(attends,
                @"^[А-ЯЁ][а-яё]+ [А-ЯЁ][.][А-ЯЁ][.] \(.{3,}[^\s]\)([,] [А-ЯЁ][а-яё]+ [А-ЯЁ][.][А-ЯЁ][.] \(.{3,}[^\s]\))*$"))
                ModelState.AddModelError("Attendees", "Неверный формат. Пример: \"Фамилия И.О. (Роль)\", добавляя новых через запятую.");
        }

        public void AddDates(Audit audit, HttpRequestBase Request, ModelStateDictionary ModelState)
        {
            // InputDate always server's "now" time
            audit.InputDate = DateTime.Now;

            // get POST[Date]
            var date = Request.Form.Get("Date");

            // try parse and store result into tmp according to formats
            DateTime tmpDate;
            string[] formats = { "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH:mm" };
            if (!DateTime.TryParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out tmpDate))
                ModelState.AddModelError("_Date", "Неверный формат ввода.");

            // check if POST[Date] of conduct less than actual entering InputDate
            if (audit.Date > audit.InputDate)
            {
                audit.Date = DateTime.Now;
                ModelState.AddModelError("_Date", "Дата проведения должна быть раньше чем \"Дата ввода\" данных.");
            }
            else
            {
                audit.Date = tmpDate;
            }
        }
    }
}