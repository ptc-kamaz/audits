﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Audits.Models.Database_Manager;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using OfficeOpenXml.Style.XmlAccess;

namespace Audits.Models.Audits
{
    public class AuditExportService
    {
        private const string Position = "Должность";
        private const string FullName = "ФИО";
        private const double SectionTableDefaulRowHeight = 24.9;
        private const double BottomTableRowHeight = 30;
        private readonly Audit _audit;
        private ExcelNamedStyleXml _checklistHeaderStyle;

        private ExcelNamedStyleXml _documentHeaderStyle;
        private ExcelNamedStyleXml _numbersTableStyle;
        private ExcelNamedStyleXml _parametersTableStyle;
        private ExcelNamedStyleXml _resposiblePersonsStyle;
        private ExcelNamedStyleXml _signsStyle;
        private ExcelNamedStyleXml _totalsSectionStyle;

        public AuditExportService(Audit audit)
        {
            _audit = audit;
        }

        public byte[] GenerateXlsxReport()
        {

            var memoryStream = new MemoryStream();
            using (var package = new ExcelPackage(memoryStream))
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add("Проведенный Аудит - " + DateTime.Now.ToShortDateString());
                worksheet.DefaultRowHeight = 13.8;
                worksheet.DefaultColWidth = 9.109375;
                InitExcelStyles(worksheet);
                InitColumnsWidth(worksheet);

                var currentRow = 0;
                RenderContent(worksheet, ref currentRow);

                worksheet.PrinterSettings.FitToPage = true;
                package.Save();
            }

            var data = memoryStream.ToArray();

            return data;
        }

        public byte[] GenerateXlsxActionsReports()
        {
            var memoryStream = new MemoryStream();
            using (var package = new ExcelPackage(memoryStream))
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add("Мероприятия по Аудиту - " + DateTime.Now.ToShortDateString());
                worksheet.DefaultRowHeight = 13.8;
                worksheet.DefaultColWidth = 9.109375;
                InitExcelStyles(worksheet);
                InitColumnsWidth(worksheet);

                var currentRow = 0;
                RenderActionsContent(worksheet, ref currentRow);

                worksheet.PrinterSettings.FitToPage = true;
                package.Save();
            }

            var data = memoryStream.ToArray();

            return data;
        }

        private void RenderActionsContent(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            RenderHeader(excelWorksheet, ref currentRow);
            //Added type for main table
            RenderMainTable(excelWorksheet, ref currentRow, false);
        }

        private void RenderContent(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            RenderHeader(excelWorksheet, ref currentRow);
            RenderMainTable(excelWorksheet, ref currentRow, true);
            RenderBottomTable(excelWorksheet, ref currentRow);
            RenderFooter(excelWorksheet, ref currentRow);
        }

        //Type for maintable uses for determine which kind of report need to render (true is audits, false is actions)
        private void RenderMainTable(ExcelWorksheet excelWorksheet, ref int currentRow, bool type)
        {
            RenderMainTableHead(excelWorksheet, ref currentRow, type);
            RenderMainTableSections(excelWorksheet, ref currentRow, type);
            ++currentRow;
        }

        private void RenderFooter(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            currentRow++;
            excelWorksheet.Row(currentRow).Height = 20.1;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Value = "Аудитор:";
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].StyleName = _resposiblePersonsStyle.Name;

            currentRow++;
            excelWorksheet.Row(currentRow).Height = 9.9;
            excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Value = Position;
            excelWorksheet.Cells[$"H{currentRow}"].Value = FullName;
            excelWorksheet.Cells[$"I{currentRow}:J{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"I{currentRow}:J{currentRow}"].Value = "Подпись";
            excelWorksheet.Cells[$"E{currentRow}:J{currentRow}"].StyleName = _signsStyle.Name;

            excelWorksheet.Cells[$"A1:J{currentRow}"].Style.Font.Name = "Arial Narrow";
            excelWorksheet.Cells[$"A1:J{currentRow}"].Style.Font.Family = 2;
        }

        private void RenderBottomTable(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            RenderBottomTableHeader(excelWorksheet, ref currentRow);
            RendertRadarChart(excelWorksheet, currentRow + 1);
            RenderBottomTableSections(excelWorksheet, ref currentRow);
            RenderBottomTableFooter(excelWorksheet, ref currentRow);
        }

        private void RenderBottomTableFooter(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            currentRow++;
            InitBottomTableSectionStyle(excelWorksheet, ref currentRow);
            excelWorksheet.Row(currentRow).Height = BottomTableRowHeight;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Value = "Средняя оценка по заводу";
            //Sum of grades
            double sumGrade = 0.0;
            foreach (var section in _audit.EvaluatedSections)
            {
                sumGrade += ((double)section.Grade / section.MaxGrade);
            }
            // Get average grade for manufactory as sum of all grade divide to count of sections
            excelWorksheet.Cells[$"E{currentRow}"].Value = (sumGrade / _audit.EvaluatedSections.Count);
            excelWorksheet.Cells[$"I{currentRow}:J{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"G{currentRow}:J{currentRow}"].Style.Font.Size = 14;
            excelWorksheet.Cells[$"G{currentRow}:J{currentRow}"].Style.VerticalAlignment =
                ExcelVerticalAlignment.Center;
            excelWorksheet.Cells[$"G{currentRow}:J{currentRow}"].Style.HorizontalAlignment =
                ExcelHorizontalAlignment.Center;
            excelWorksheet.Cells[$"G{currentRow}:J{currentRow}"].Style.Font.Bold = true;
            excelWorksheet.Cells[$"G{currentRow}:J{currentRow}"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            excelWorksheet.Row(currentRow).Style.WrapText = true;
            currentRow++;
            excelWorksheet.Row(currentRow).Height = 20.1;
        }

        private void RendertRadarChart(ExcelWorksheet excelWorksheet, int chartStartPosition)
        {
            var radarChart =
                excelWorksheet.Drawings.AddChart("crtExtensionsSize", eChartType.RadarMarkers) as ExcelRadarChart;
            if (radarChart == null)
            {
                return;
            }

            radarChart.SetPosition(chartStartPosition - 3, 0, 7, 0);
            radarChart.SetSize(360, 360);

            var chartEndPosition = chartStartPosition + _audit.EvaluatedSections.Count - 1;
            radarChart.Series.Add(
                ExcelCellBase.GetAddress(chartStartPosition, 5, chartEndPosition, 5),
                ExcelCellBase.GetAddress(chartStartPosition, 1, chartEndPosition, 1)
            );

            radarChart.Title.Text = "Сводная оценка";
            radarChart.Title.Font.Size = 14;
            radarChart.Title.Font.Bold = true;

            radarChart.Border.Fill.Style = eFillStyle.NoFill;

            radarChart.DataLabel.ShowLegendKey = false;
            radarChart.DataLabel.ShowValue = false;
            radarChart.DataLabel.ShowCategory = false;
            radarChart.DataLabel.ShowSeriesName = false;
            radarChart.DataLabel.ShowPercent = false;
            radarChart.DataLabel.ShowBubbleSize = false;
            radarChart.DataLabel.ShowLeaderLines = true;

            radarChart.DisplayBlanksAs = eDisplayBlanksAs.Gap;

            radarChart.YAxis.MaxValue = 1.0;
            radarChart.YAxis.MinValue = 0;
            radarChart.YAxis.MajorUnit = 0.25;
            radarChart.YAxis.MajorTickMark = eAxisTickMark.None;
            radarChart.YAxis.MinorTickMark = eAxisTickMark.None;
            radarChart.YAxis.Crosses = eCrosses.AutoZero;
            radarChart.YAxis.CrossBetween = eCrossBetween.Between;
            radarChart.YAxis.CrossesAt = 87114112;

            radarChart.XAxis.Font.Size = 8;
            radarChart.XAxis.Font.Bold = true;

            radarChart.DataLabel.Font.Size = 10;
            radarChart.DataLabel.Font.Bold = true;

            var chartXml = radarChart.ChartXml;
            if (chartXml.DocumentElement != null)
            {
                var nsuri = chartXml.DocumentElement.NamespaceURI;
                var nsm = new XmlNamespaceManager(chartXml.NameTable);
                nsm.AddNamespace("c", nsuri);

                var valAxisNodes = chartXml.SelectNodes("c:chartSpace/c:chart/c:plotArea/c:valAx", nsm);
                if (valAxisNodes != null && valAxisNodes.Count > 0)
                {
                    foreach (XmlNode valAxisNode in valAxisNodes)
                    {
                        var major = valAxisNode.SelectSingleNode("c:majorGridlines", nsm);
                        //Needed to make chart's lines dashed
                        if (major != null)
                        {
                            major.InnerXml =
                                "<c:spPr><a:ln w=\"6350\"><a:solidFill><a:schemeClr val=\"tx1\"><a:lumMod val=\"75000\"/><a:lumOff val=\"25000\"/></a:schemeClr></a:solidFill><a:prstDash val=\"dash\"/></a:ln></c:spPr>";
                        }
                    }
                }
            }

            radarChart.Legend.Remove();
        }

        //Done
        private void RenderBottomTableSections(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            foreach (var section in _audit.EvaluatedSections)
            {
                currentRow++;
                InitBottomTableSectionStyle(excelWorksheet, ref currentRow);
                excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Value = section.Section.Name;
                // Real grade
                if (section.MaxGrade != 0)
                {
                    excelWorksheet.Cells[$"E{currentRow}"].Value = ((double)section.Grade / section.MaxGrade);
                }
                else
                {
                    excelWorksheet.Cells[$"E{currentRow}"].Value = 0;
                }
            }

            //If Sections.Count less than 7, chart overlaps bottom elements
            if (_audit.EvaluatedSections.Count >= 7)
            {
                return;
            }

            for (var i = 7 - _audit.EvaluatedSections.Count; i > 0; i--)
            {
                currentRow++;
                InitBottomTableSectionStyle(excelWorksheet, ref currentRow);
            }
        }


        private void InitBottomTableSectionStyle(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            excelWorksheet.Cells[$"A{currentRow}:E{currentRow}"].StyleName = _totalsSectionStyle.Name;
            excelWorksheet.Row(currentRow).Height = BottomTableRowHeight;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Style.HorizontalAlignment =
                ExcelHorizontalAlignment.Left;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Style.Indent = 1;
            excelWorksheet.Cells[$"E{currentRow}"].Style.Numberformat.Format = "#0.00%";
            excelWorksheet.Row(currentRow).Style.WrapText = true;
        }

        private void RenderBottomTableHeader(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            excelWorksheet.Row(currentRow).Height = BottomTableRowHeight;
            currentRow++;
            excelWorksheet.Cells[$"A{currentRow}:E{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"A{currentRow}:E{currentRow}"].Value = "Оценка";
            excelWorksheet.Cells[$"A{currentRow}:E{currentRow}"].StyleName = _totalsSectionStyle.Name;
            excelWorksheet.Row(currentRow).Height = BottomTableRowHeight;
            currentRow++;

            excelWorksheet.Row(currentRow).Height = BottomTableRowHeight;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Value = "Раздел";
            excelWorksheet.Cells[$"E{currentRow}"].Value = "Пред.";
            excelWorksheet.Cells[$"A{currentRow}:E{currentRow}"].StyleName = _totalsSectionStyle.Name;
        }

        private void RenderMainTableSections(ExcelWorksheet excelWorksheet, ref int currentRow, bool type)
        {
            foreach (var section in _audit.EvaluatedSections)
            {
                RenderMainTableSectionBlock(excelWorksheet, section.Section, ref currentRow, type);
                RenderSectionParameters(excelWorksheet, section, ref currentRow, type);
                if (type)
                {
                    RenderSectionFooter(excelWorksheet, ref currentRow, section);
                }
            }
        }

        private void RenderSectionFooter(ExcelWorksheet excelWorksheet, ref int currentRow, EvaluatedSection section)
        {
            excelWorksheet.Row(currentRow).Height = SectionTableDefaulRowHeight;
            excelWorksheet.Row(currentRow).Style.WrapText = true;
            excelWorksheet.Cells[$"C{currentRow}:H{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"C{currentRow}:H{currentRow}"].Value = "Оцениваемых пунктов";
            excelWorksheet.Cells[$"C{currentRow}:I{currentRow}"].StyleName = _totalsSectionStyle.Name;
            // Count of parameters
            excelWorksheet.Cells[$"I{currentRow}"].Value = section.EvaluatedParameters.Count;

            currentRow++;
            excelWorksheet.Row(currentRow).Height = SectionTableDefaulRowHeight;
            excelWorksheet.Cells[$"C{currentRow}:H{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"C{currentRow}:H{currentRow}"].Value = "Итого баллов по разделу";
            excelWorksheet.Cells[$"C{currentRow}:I{currentRow}"].StyleName = _totalsSectionStyle.Name;
            // Total grade for section
            excelWorksheet.Cells[$"I{currentRow}"].Value = section.Grade;

        }

        private void RenderMainTableSectionBlock(ExcelWorksheet excelWorksheet, Section section, ref int currentRow, bool type)
        {
            currentRow++;
            var endOfSectionRow = 0;
            if (type)
            {
                endOfSectionRow = currentRow + section.Parameters.Count + 1;
            }
            else
            {
                endOfSectionRow = currentRow + section.Parameters.Count - 1;
            }
            excelWorksheet.Cells[$"A{currentRow}:B{endOfSectionRow}"].Merge = true;
            excelWorksheet.Cells[$"A{currentRow}:B{endOfSectionRow}"].Value = section.Name;
            excelWorksheet.Cells[$"A{currentRow}:B{endOfSectionRow}"].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            excelWorksheet.Cells[$"A{currentRow}:B{endOfSectionRow}"].Style.TextRotation = 90;
            excelWorksheet.Cells[$"A{currentRow}:B{endOfSectionRow}"].Style.Font.Bold = true;
            excelWorksheet.Cells[$"A{currentRow}:B{endOfSectionRow}"].Style.VerticalAlignment =
                ExcelVerticalAlignment.Center;
            excelWorksheet.Cells[$"A{currentRow}:B{endOfSectionRow}"].Style.HorizontalAlignment =
                ExcelHorizontalAlignment.Center;
            excelWorksheet.Cells[$"A{currentRow}:H{currentRow}"].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            excelWorksheet.Row(currentRow).Style.WrapText = true;
        }

        private void RenderSectionParameters(ExcelWorksheet excelWorksheet, EvaluatedSection section, ref int currentRow, bool type)
        {
            var parameterCount = 1;
            foreach (var parameter in section.EvaluatedParameters)
            {
                excelWorksheet.Row(currentRow).Height = SectionTableDefaulRowHeight;
                excelWorksheet.Row(currentRow).Style.WrapText = true;
                excelWorksheet.Cells[$"C{currentRow}"].Value = parameterCount++;
                excelWorksheet.Cells[$"C{currentRow}"].StyleName = _numbersTableStyle.Name;

                excelWorksheet.Cells[$"D{currentRow}:H{currentRow}"].Merge = true;
                excelWorksheet.Cells[$"D{currentRow}:H{currentRow}"].Value = parameter.Parameter.Name;
                excelWorksheet.Cells[$"D{currentRow}:H{currentRow}"].StyleName = _parametersTableStyle.Name;
                if (type)
                {
                    //added evaluation for parameters
                    excelWorksheet.Cells[$"I{currentRow}"].StyleName = _numbersTableStyle.Name;
                    excelWorksheet.Cells[$"I{currentRow}"].Value = parameter.Value;
                }
                else
                {
                    //added actions for parameter
                    excelWorksheet.Cells[$"I{currentRow}:O{currentRow}"].Merge = true;
                    excelWorksheet.Cells[$"I{currentRow}:O{currentRow}"].Style.WrapText = true;
                    excelWorksheet.Cells[$"I{currentRow}:O{currentRow}"].StyleName = _parametersTableStyle.Name;
                    excelWorksheet.Cells[$"I{currentRow}:O{currentRow}"].Value = parameter.Actions;
                }

                currentRow++;
            }
        }

        private void RenderHeader(ExcelWorksheet excelWorksheet, ref int currentRow)
        {
            var personNames = new List<string> { "Аудитор:", "Руководитель:", "Сопровождающие:", "" };

            excelWorksheet.Cells["A1:I1"].Merge = true;
            excelWorksheet.Cells["A1:I1"].Value = _audit.Checklist.Name;
            excelWorksheet.Cells["A1:I1"].StyleName = _documentHeaderStyle.Name;
            excelWorksheet.Row(1).Height = 24.9;

            excelWorksheet.Cells["A2:D2"].Merge = true;
            excelWorksheet.Cells["A2:D2"].Value = "Подразделение:";
            excelWorksheet.Cells["E2:H2"].Merge = true;
            excelWorksheet.Cells["A2:I2"].StyleName = _resposiblePersonsStyle.Name;
            excelWorksheet.Row(2).Height = 20.1;
            //Add department head position
            excelWorksheet.Cells["E2:H2"].Value = _audit.Subdivision.Name;


            excelWorksheet.Cells["E3:G3"].Merge = true;
            excelWorksheet.Cells["E3:G3"].Value = "Завод";
            excelWorksheet.Cells["I3"].Value = "Дата";
            excelWorksheet.Cells["E3:I3"].StyleName = _signsStyle.Name;
            excelWorksheet.Row(3).Height = 9.9;

            currentRow = 3;
            foreach (var personName in personNames)
            {
                currentRow++;
                RenderResponsiblePerson(excelWorksheet, personName, ref currentRow);
            }
        }

        private void RenderMainTableHead(ExcelWorksheet excelWorksheet, ref int currentRow, bool type)
        {
            excelWorksheet.Row(currentRow).Height = 9.9;

            excelWorksheet.Cells["A14:B17"].Merge = true;
            excelWorksheet.Cells["A14:B17"].Value = "Раздел";
            excelWorksheet.Cells["A14:B17"].StyleName = _checklistHeaderStyle.Name;

            excelWorksheet.Cells["C14:C17"].Merge = true;
            excelWorksheet.Cells["C14:C17"].Value = "№";
            excelWorksheet.Cells["C14:C17"].StyleName = _checklistHeaderStyle.Name;

            excelWorksheet.Cells["D14:H17"].Merge = true;
            excelWorksheet.Cells["D14:H17"].Value = "Параметры проверки";
            excelWorksheet.Cells["D14:H17"].StyleName = _checklistHeaderStyle.Name;
            excelWorksheet.SelectedRange["D14:H17"].StyleName = _checklistHeaderStyle.Name;

            if (type)
            {
                excelWorksheet.Cells["I14"].Value = "Оценка";
                excelWorksheet.Cells["I15"].Value = "да - 3";
                excelWorksheet.Cells["I16"].Value = "частично - 2";
                excelWorksheet.Cells["I17"].Value = "нет - 1";

                excelWorksheet.Cells["I14:I17"].Style.Font.Bold = true;
                excelWorksheet.Cells["I14:I17"].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                excelWorksheet.Cells["I15:I17"].Style.Font.Size = 8;
                excelWorksheet.Cells["I14"].Style.Font.Size = 10;
                excelWorksheet.Cells["I14:I17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            }
            else
            {
                excelWorksheet.Cells["I14:O17"].Merge = true;
                excelWorksheet.Cells["I14:O17"].Value = "План Мероприятий";
                excelWorksheet.Cells["I14:O17"].StyleName = _checklistHeaderStyle.Name;
                excelWorksheet.SelectedRange["I14:O17"].StyleName = _checklistHeaderStyle.Name;
            }

            for (var i = 14; i <= 17; i++)
            {
                excelWorksheet.Row(i).Height = 15;
            }
            currentRow = 17;
        }

        private void RenderResponsiblePerson(ExcelWorksheet excelWorksheet, string header, ref int currentRow)
        {
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"A{currentRow}:D{currentRow}"].Value = header;
            excelWorksheet.Cells[$"A{currentRow}:H{currentRow}"].StyleName = _resposiblePersonsStyle.Name;

            if (string.Equals(header, "Аудитор:"))
            {
                excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Merge = true;
                //Add auditor position
                excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Value = _audit.Auditor.ActualPosition;
                //Add auditor info
                excelWorksheet.Cells[$"H{currentRow}"].Value = (_audit.Auditor.LastName + " " + _audit.Auditor.FirstName + " " + _audit.Auditor.Patronymic);

            }
            else if (string.Equals(header, "Руководитель:"))
            {
                excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Merge = true;
                //Add head position
                excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Value = _audit.Subdivision.Head.ActualPosition;
                //Add head info
                excelWorksheet.Cells[$"H{currentRow}"].Value = (_audit.Subdivision.Head.LastName + " " + _audit.Subdivision.Head.FirstName + " " + _audit.Subdivision.Head.Patronymic);

            }
            else if (string.Equals(header, "Сопровождающие:"))
            {
                excelWorksheet.Cells[$"E{currentRow}:H{currentRow}"].Merge = true;
                //Add attendees info
                excelWorksheet.Cells[$"E{currentRow}:H{currentRow}"].Value = _audit.Attendees;
            }
            else if (string.IsNullOrEmpty(header))
            {
                excelWorksheet.Cells[$"A{currentRow}:H{currentRow}"].Style.Border.Bottom.Style =
                    ExcelBorderStyle.None;
            }
            excelWorksheet.Row(currentRow).Height = 20.1;

            currentRow++;
            excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Merge = true;
            excelWorksheet.Cells[$"E{currentRow}:G{currentRow}"].Value = Position;
            excelWorksheet.Cells[$"H{currentRow}"].Value = FullName;
            excelWorksheet.Cells[$"E{currentRow}:H{currentRow}"].StyleName = _signsStyle.Name;
            excelWorksheet.Row(currentRow).Height = 9.9;
        }

        private void InitColumnsWidth(ExcelWorksheet excelWorksheet)
        {
            excelWorksheet.Column(1).Width = 3.6640625;
            excelWorksheet.Column(2).Width = 3.6640625;
            excelWorksheet.Column(3).Width = 5.6640625;
            excelWorksheet.Column(4).Width = 5.6640625;
            excelWorksheet.Column(5).Width = 8.6640625;
            excelWorksheet.Column(6).Width = 8.6640625;
            excelWorksheet.Column(7).Width = 8.6640625;
            excelWorksheet.Column(8).Width = 25.6640625;
            excelWorksheet.Column(9).Width = 10.6640625;
            excelWorksheet.Column(10).Width = 10.6640625;
        }

        private void InitExcelStyles(ExcelWorksheet excelWorksheet)
        {
            _documentHeaderStyle = excelWorksheet.Workbook.Styles.CreateNamedStyle("documentHeaderStyle");
            _documentHeaderStyle.Style.Font.Bold = true;
            _documentHeaderStyle.Style.Font.Size = 14;
            _documentHeaderStyle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            _documentHeaderStyle.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            _resposiblePersonsStyle = excelWorksheet.Workbook.Styles.CreateNamedStyle("resposiblePersonsStyle");
            _resposiblePersonsStyle.Style.Font.Size = 12;
            _resposiblePersonsStyle.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            _signsStyle = excelWorksheet.Workbook.Styles.CreateNamedStyle("signsStyle");
            _signsStyle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            _signsStyle.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            _signsStyle.Style.Font.Size = 8;
            _checklistHeaderStyle = excelWorksheet.Workbook.Styles.CreateNamedStyle("checklistHeaderStyle");
            _checklistHeaderStyle.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            _checklistHeaderStyle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            _checklistHeaderStyle.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            _checklistHeaderStyle.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            _checklistHeaderStyle.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            _checklistHeaderStyle.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            _checklistHeaderStyle.Style.Font.Size = 10;
            _checklistHeaderStyle.Style.Font.Bold = true;
            _parametersTableStyle = excelWorksheet.Workbook.Styles.CreateNamedStyle("parametersTableStyle");
            _parametersTableStyle.Style.Font.Size = 10;
            _parametersTableStyle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            _parametersTableStyle.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            _parametersTableStyle.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            _parametersTableStyle.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            _parametersTableStyle.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            _parametersTableStyle.Style.Indent = 1;
            _numbersTableStyle = excelWorksheet.Workbook.Styles.CreateNamedStyle("numbersTableStyle");
            _numbersTableStyle.Style.Font.Size = 10;
            _numbersTableStyle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            _numbersTableStyle.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            _numbersTableStyle.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            _numbersTableStyle.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            _numbersTableStyle.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            _numbersTableStyle.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            _totalsSectionStyle = excelWorksheet.Workbook.Styles.CreateNamedStyle("totalsSectionStyle");
            _totalsSectionStyle.Style.Font.Size = 10;
            _totalsSectionStyle.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            _totalsSectionStyle.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            _totalsSectionStyle.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            _totalsSectionStyle.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            _totalsSectionStyle.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            _totalsSectionStyle.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            _totalsSectionStyle.Style.Font.Bold = true;
        }
    }
}