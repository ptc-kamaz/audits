﻿using System;
using System.Collections.Generic;

namespace Audits.Models.Audits
{
    public class EvaluatedValue
    {
        public int SectionID { get; set; }
        public int ParameterID { get; set; }
        public byte PreviousValue { get; set; }
        public byte? ActualValue { get; set; }
        public string Actions { get; set; }
    }

    public class AuditValidate
    {
        public AuditValidate()
        {
            EvaluatedValues = new List<EvaluatedValue>();
        }

        public string Attendees { get; set; }
        public DateTime DateTime { get; set; }
        public ICollection<EvaluatedValue> EvaluatedValues { get; set; }
    }
}