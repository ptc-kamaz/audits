﻿using System;
using System.Linq;
using System.Web.Mvc;
using Audits.Models.Database_Manager;
using Audits.Models.Reports;
using Audits.Models.Reports.Data;
using Newtonsoft.Json;

namespace Audits.Controllers
{
    public class ReportsController : Controller
    {
        private readonly DatabaseEntities _db = new DatabaseEntities();

        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ComplianceForSectionByMonths()
        {
            ViewBag.Subdivisions = new DatabaseEntities().Subdivisions.Where(sd => sd.Children.Count > 0).ToList();
            ViewBag.Action = nameof(GetComplianceForSectionByMonthsReport);
            ViewBag.Title = "Уровень соответствия по номинации";
            ViewBag.ReportType = ReportType.ComplianceByMonths.ToString();
            return View("ComplianceForSectionByMonths");
        }

        public ActionResult ComplianceLevelForSection()
        {
            ViewBag.Subdivisions = new DatabaseEntities().Subdivisions.Where(sd => sd.Children.Count > 0).ToList();
            ViewBag.Action = nameof(GetComplianceLevelForSectionReport);
            ViewBag.Title = "Уровень соответствия за период";
            ViewBag.ReportType = ReportType.Compliance.ToString();
            return View("ComplianceSectionLevel");
        }

        public ActionResult ComplianceLevelForSubdivisionBar()
        {
            ViewBag.Subdivisions = new DatabaseEntities().Subdivisions.Where(sd => sd.Children.Count > 0).ToList();
            ViewBag.Action = nameof(GetComplianceForSubdivisionBarReport);
            ViewBag.Title = "Уровень соответствия за период";
            ViewBag.ReportType = ReportType.Compliance.ToString();
            return View("ComplianceSubdivisionLevel");
        }

        public ActionResult ComplianceLevelForSubdivisionRadar()
        {
            ViewBag.Subdivisions = new DatabaseEntities().Subdivisions.Where(sd => sd.Children.Count > 0).ToList();
            ViewBag.Action = nameof(GetComplianceForSubdivisionRadarReport);
            ViewBag.Title = "Уровень соответствия за период";
            ViewBag.ReportType = ReportType.Compliance.ToString();
            return View("ComplianceSubdivisionLevel");
        }

        public ActionResult ExecutionSchedulePie()
        {
            ViewBag.Subdivisions = new DatabaseEntities().Subdivisions.Where(sd => sd.Children.Count > 0).ToList();
            ViewBag.Action = nameof(GetExecutionSchedulePieReport);
            ViewBag.Title = "Выполнение графика аудитов";
            ViewBag.ReportType = ReportType.Compliance.ToString();
            return View("ExecutionSchedule");
        }

        public ActionResult ExecutionScheduleBar()
        {
            ViewBag.Subdivisions = new DatabaseEntities().Subdivisions.Where(sd => sd.Children.Count > 0).ToList();
            ViewBag.Action = nameof(GetExecutionScheduleBarReport);
            ViewBag.Title = "Выполнение графика аудитов";
            ViewBag.ReportType = ReportType.Execution.ToString();
            return View("ExecutionSchedule");
        }

        public ActionResult ValidationLevel()
        {
            ViewBag.Subdivisions = new DatabaseEntities().Subdivisions.Where(sd => sd.Children.Count > 0).ToList();
            ViewBag.Action = nameof(GetValidationLevelReport);
            ViewBag.Title = "Уровень подтверждения оценки";
            ViewBag.ReportType = ReportType.Validation.ToString();
            return View("ValidationLevel");
        }

        public ActionResult GetSections(int subdivisionId)
        {
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            if (subdivision == null)
            {
                return null;
            }

            var children = ReportService.GetSubdivisionChildren(subdivision);
            var sections =
                children.SelectMany(c => c.Audits)
                    .SelectMany(a => a.EvaluatedSections)
                    .Select(es => es.Section)
                    .ToList();
            var result = sections.GroupBy(s => s.ID)
                .ToDictionary(section => section.Key.ToString(), section => section.First().Name);

            return Content(JsonConvert.SerializeObject(result));
        }

        public ActionResult GetDataTable(int subdivisionId, int sectionId,
            string startDate, string endDate, string reportType)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);

            var section = sectionId <= 0 ? null : _db.Sections.Find(sectionId);

            var subdivision = _db.Subdivisions.Find(subdivisionId);
            var subdivisionChildren = ReportService.GetSubdivisionChildren(subdivision);

            ReportType type;
            Enum.TryParse(reportType, true, out type);

            var report = new Report(subdivision, subdivisionChildren, startDateTime, endDateTime, section);
            var data = new ReportData(report, type);
            return PartialView("_Table", data);
        }

        public ActionResult GetSubdivisions(int parentId)
        {
            var subdivision = _db.Subdivisions.Find(parentId);
            if (subdivision == null)
            {
                return null;
            }

            var children = ReportService.GetSubdivisionChildren(subdivision);
            var result = children.GroupBy(s => s.ID).ToDictionary(s => s.Key.ToString(), s => s.First().Name);

            return Content(JsonConvert.SerializeObject(result));
        }

        public ActionResult GetComplianceForSectionByMonthsReport(int subdivisionId, int sectionId,
            string startDate, string endDate, bool asPdf)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);
            var section = _db.Sections.Find(sectionId);
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            var outputDir = HttpContext.Server.MapPath("/media/generatedfiles/");

            var reportCreator = new ReportCreator(subdivision, startDateTime, endDateTime, section,  outputDir);
            reportCreator.SaveAsPdf = asPdf;
            var file = reportCreator.CreateComplianceForSectionByMonthsReport();


            return File(file, "application/vnd.ms-excel", ReportService.GetReportFileName(asPdf));
        }

        public ActionResult GetComplianceLevelForSectionReport(int subdivisionId, int sectionId, string startDate, string endDate, bool asPdf)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);
            var section = _db.Sections.Find(sectionId);
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            var outputDir = HttpContext.Server.MapPath("/media/generatedfiles/");

            var reportCreator = new ReportCreator(subdivision, startDateTime, endDateTime, section, outputDir);
            reportCreator.SaveAsPdf = asPdf;
            var file = reportCreator.CreateComplianceLevelForSectionReport();

            return File(file, "application/vnd.ms-excel", ReportService.GetReportFileName(asPdf));
        }

        public ActionResult GetComplianceForSubdivisionBarReport(int subdivisionId, int selectedSubdivisionId,
            string startDate, string endDate, bool asPdf)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            if (subdivision == null)
            {
                return HttpNotFound();
            }
            var selectedSubdivision = _db.Subdivisions.Find(selectedSubdivisionId);
            var outputDir = HttpContext.Server.MapPath("/media/generatedfiles/");

            var reportCreator = new ReportCreator(subdivision, startDateTime, endDateTime, selectedSubdivision, outputDir);
            reportCreator.SaveAsPdf = asPdf;
            var file = reportCreator.CreateComplianceForSubdivisionBarReport();

            return File(file, "application/vnd.ms-excel", ReportService.GetReportFileName(asPdf));
        }

        public ActionResult GetComplianceForSubdivisionRadarReport(int subdivisionId, int selectedSubdivisionId, string startDate, string endDate, bool asPdf)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            if (subdivision == null)
            {
                return HttpNotFound();
            }
            var selectedSubdivision = _db.Subdivisions.Find(selectedSubdivisionId);
            var outputDir = HttpContext.Server.MapPath("/media/generatedfiles/");

            var reportCreator = new ReportCreator(subdivision, startDateTime, endDateTime, selectedSubdivision, outputDir);
            reportCreator.SaveAsPdf = asPdf;
            var file = reportCreator.CreateComplianceForSubdivisionRadarReport();

            return File(file, "application/vnd.ms-excel", ReportService.GetReportFileName(asPdf));
        }

        public ActionResult GetValidationLevelReport(int subdivisionId, string startDate, string endDate, bool asPdf)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            var outputDir = HttpContext.Server.MapPath("/media/generatedfiles/");

            var reportCreator = new ReportCreator(subdivision, startDateTime, endDateTime, outputDir);
            reportCreator.SaveAsPdf = asPdf;
            var file = reportCreator.CreateValidationLevelReport();

            return File(file, "application/vnd.ms-excel", ReportService.GetReportFileName(asPdf));
        }

        public ActionResult GetExecutionSchedulePieReport(int subdivisionId, int selectedSubdivisionId,  string startDate, string endDate, bool asPdf)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            var selectedSubdivision = _db.Subdivisions.Find(selectedSubdivisionId);
            var outputDir = HttpContext.Server.MapPath("/media/generatedfiles/");

            var reportCreator = new ReportCreator(subdivision, startDateTime, endDateTime, selectedSubdivision, outputDir);
            reportCreator.SaveAsPdf = asPdf;
            var file = reportCreator.CreateExecutionSchedulePieReport();

            return File(file, "application/vnd.ms-excel", ReportService.GetReportFileName(asPdf));
        }
        
        public ActionResult GetExecutionScheduleBarReport(int subdivisionId, int selectedSubdivisionId, string startDate, string endDate, bool asPdf)
        {
            var startDateTime = ParseDateTime(startDate);
            var endDateTime = ParseDateTime(endDate, true);
            var subdivision = _db.Subdivisions.Find(subdivisionId);
            var selectedSubdivision = _db.Subdivisions.Find(selectedSubdivisionId);
            var outputDir = HttpContext.Server.MapPath("/media/generatedfiles/");

            var reportCreator = new ReportCreator(subdivision, startDateTime, endDateTime, selectedSubdivision, outputDir);
            reportCreator.SaveAsPdf = asPdf;
            var file = reportCreator.CreateExecutionScheduleBarReport();

            return File(file, "application/vnd.ms-excel", ReportService.GetReportFileName(asPdf));
        }

        private static DateTime ParseDateTime(string date, bool endOfDay = false)
        {
            var startDateTime = DateTime.ParseExact(date, "dd/MM/yyyy", null);
            if (endOfDay)
            {
                startDateTime = startDateTime.AddDays(1).AddMilliseconds(-1);
            }
            return startDateTime;
        }
    }
}