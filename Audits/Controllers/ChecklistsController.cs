﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Audits.Models.Checklists;
using Audits.Models.Database_Manager;
using Audits.Models.Security;
using System.Security.Principal;

namespace Audits.Controllers
{
    [HasPermission(Permission = "ReadChecklist")]
    public class ChecklistsController : Controller
    {
        private readonly DatabaseEntities _db = new DatabaseEntities();
        private readonly WindowsIdentity _identity = WindowsIdentity.GetCurrent();

        public ActionResult Index()
        {
            if (!RoleDepository.HasPermission(_identity, "ReadChecklist"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            return View(_db.Checklists.ToList());
        }
        
        public ActionResult Create()
        {
            if (!RoleDepository.HasPermission(_identity, "EditChecklist"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            ViewBag.Title = "Создать чек-лист";
            ViewBag.Header = "Создание чек-листа";
            return View(new Checklist());
        }

        [HttpPost]
        [HasPermission(Permission = "EditChecklist")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Checklist checklist)
        {
            if (!ModelState.IsValid)
            {
                return View(checklist);
            }

            ChecklistService.Create(checklist, _db);
            return RedirectToAction("Index");
        }
        [HasPermission(Permission = "EditChecklist")]
        public ActionResult AddSection()
        {
            return Request.IsAjaxRequest() ? PartialView("_AddSection", new Section()) : null;
        }

        [HasPermission(Permission = "EditChecklist")]
        public ActionResult AddParameter(string htmlFieldPrefix = "")
        {
            if (!string.IsNullOrEmpty(htmlFieldPrefix))
            {
                ViewData.TemplateInfo.HtmlFieldPrefix = htmlFieldPrefix;
            }
            return Request.IsAjaxRequest() ? PartialView("_AddParameter", new Parameter()) : null;
        }
        
        public ActionResult Edit(int? id)
        {
            if (!RoleDepository.HasPermission(_identity, "EditChecklist"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var checklist = _db.Checklists.Find(id);
            if (checklist == null)
            {
                return HttpNotFound();
            }
            ViewBag.Title = "Редактировать чек-лист";
            ViewBag.Header = "Редактирование чек-листа";
            return View("Create", checklist);
        }

        [HttpPost]
        [HasPermission(Permission = "EditChecklist")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Checklist checklist)
        {
            return !ModelState.IsValid || ChecklistService.Update(checklist, _db) < 0
                ? (ActionResult) View("Create", checklist)
                : RedirectToAction("Index");
        }

        [HttpPost]
        [HasPermission(Permission = "EditChecklist")]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                ChecklistService.Delete(id, _db);
            }
            catch (DbUpdateException)
            {
                ViewData["Message"] = "Невозможно удалить чек-лист, т.к. существуют связанные сущности. "
                    + "Например, аудиты или записи расписания аудитов.";
                return View("~/Views/Error/Index.cshtml");
            }
            return RedirectToAction("Index");
        }

        public ActionResult View(int? id)
        {
            if (!RoleDepository.HasPermission(_identity, "ReadChecklist"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var checklist = _db.Checklists.Find(id);
            if (checklist == null)
            {
                return HttpNotFound();
            }
            return View("View", checklist);
        }

        public FileResult Export(int? id)
        {            
            if (id == null)
            {
                return null;
            }

            var checkList = _db.Checklists.Find(id);
            if (checkList == null)
            {
                return null;
            }

            var exportService = new ChecklistExportService(checkList);
            var bytes = exportService.GenerateXlsxReport();

            return File(bytes, "application/vnd.ms-excel", checkList.Name + ".xlsx");
        }        
        public ActionResult GetParameterWhichsCantBeDeleted(int checklistId)
        {
            if (!Request.IsAjaxRequest())
            {
                return null;
            }

            var evaluatedParameters =
                _db.EvaluatedParameters.Where(ep => ep.Parameter.Section.Checklist.ID == checklistId)
                    .Select(ep => ep.ParameterID)
                    .ToList();
            return Content(string.Join(" ", evaluatedParameters));
        }
        
        public bool CanChecklistBeDeleted(int checklistId)
        {
            if (!Request.IsAjaxRequest())
            {
                return false;
            }

            var esCount = _db.EvaluatedSections.Count(es => es.Section.ChecklistID == checklistId);
            var auditsCount = _db.Audits.Count(a => a.ChecklistID == checklistId);

            return esCount <= 0 && auditsCount <= 0;
        }
    }
}