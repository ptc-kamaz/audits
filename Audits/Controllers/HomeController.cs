﻿using System.Web.Mvc;
using Audits.Models.Security;

namespace Audits.Controllers
{
    public class HomeController : Controller
    {
        [HasPermission(Permission = "ReadAudits")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}