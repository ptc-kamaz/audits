﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Audits.Models.Audits;
using Audits.Models.Database_Manager;
using Audits.Models.Security;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Audits.Models.EmployeesManager;

namespace Audits.Controllers
{
    public class AuditsController : Controller
    {
        private readonly DatabaseEntities _db;
        private readonly WindowsIdentity _identity;

        public AuditsController()
        {
            _db = new DatabaseEntities();
            AuditsService = new AuditsService();
            _identity = WindowsIdentity.GetCurrent();
        }

        private AuditsService AuditsService { get; }

        [HasPermission(Permission = "ReadAudits")]
        public ActionResult Export(int? id, bool mode)
        {
            if (id.HasValue)
            {
                var audit = AuditsService.GetDetails(id);

                if (audit != null)
                {
                    byte[] bytes = null;
                    AuditExportService auditExportService = new AuditExportService(audit);
                    if (mode)
                    {
                        bytes = auditExportService.GenerateXlsxReport();
                    }
                    else
                    {
                        bytes = auditExportService.GenerateXlsxActionsReports();
                    }
                    return File(bytes, "application/vnd.ms-excel", audit.Checklist.Name + ".xlsx");
                }
                return HttpNotFound();
            }
            return View("Error");
        }

        [HasPermission(Permission = "ReadAudits")]
        public ActionResult Details(int? id)
        {
            if (id.HasValue)
            {
                var audit = AuditsService.GetDetails(id);

                if (audit != null)
                {
                    return View(audit);
                }
                return HttpNotFound();
            }
            return View("Error");
        }

        public ActionResult Validate(int? id)
        {
            bool isTrustedAuditor = RoleDepository.HasPermission(WindowsIdentity.GetCurrent(), "ValidateauditTrusted");
            bool isKRPSAuditor = RoleDepository.HasPermission(WindowsIdentity.GetCurrent(), "ValidateauditKrps");

            if (id.HasValue)
            {
                var audit = AuditsService.ValidationPrepare((int)id);
                if (audit != null)
                {
                    if (audit.ValidatedAudit != null && audit.ValidatedAudit.ValidatedAudit == null && isKRPSAuditor)
                    {
                        //Accept prove already proven by trusted auditor audit
                        return View(AuditsService.ValidationPrepare((int)audit.ValidatedAuditID));
                    }
                    else if (audit.ValidatedAudit == null && isTrustedAuditor)
                    {
                        //Accept prove audit by trusted auditor
                        return View(audit);
                    }
                    //If user has not permission or audit already was proven by KRPS auditor the result "Error"
                    return View("Error");
                }
                return HttpNotFound();
            }
            //here need view that audit is already validated
            return View("Error");
        }

        [HttpPost]
        public ActionResult Validate(string response)
        {
            bool isTrustedAuditor = RoleDepository.HasPermission(WindowsIdentity.GetCurrent(), "ValidateauditTrusted");
            bool isKRPSAuditor = RoleDepository.HasPermission(WindowsIdentity.GetCurrent(), "ValidateauditKrps");

            if (isTrustedAuditor || isKRPSAuditor)
            {
                var result = false;
                var values = new JavaScriptSerializer().Deserialize<AuditValidate>(response);

                int auditId;
                int.TryParse(RouteData.Values["id"].ToString(), out auditId);

                Audit audit = AuditsService.GetDetails(auditId);
                if (audit.ValidatedAudit != null)
                {
                    result = AuditsService.Validate(audit.ValidatedAudit, values);
                }
                else
                {
                    result = AuditsService.Validate(audit, values);
                }

                if (result)
                {
                    return Json(new { success = true, url = Url.Action("Details", new { id = auditId }) });
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }


        // GET: Audit/Index
        // Main page. Lists all available Audits in the database supporting the sort by different keys and batch delete.
        [HasPermission(Permission = "ReadAudits")]
        [HttpGet]
        public ActionResult Index(int id = 1, int range = 1, string sort = "")
        {
            AuditsService.LoadAudits(id, sort, range, HttpContext.GetUserAdId(), _identity);

            // if no audits found
            if (AuditsService.Model == null)
            {
                ViewBag.ErrorName = "Данные не найдены";
                ViewBag.ErrorReason = "AuditsManager/Index не смог выгрузить необходимые данные.";
                return View("Error", null);
            }

            return View(AuditsService);
        }

        // POST: Audit/Index
        // Dispatch delete/edit actions on selected Audit items at Index page.
        [HttpPost]
        [ActionName("Index")]
        [HasPermission(Permission = "WriteAudits")]
        [ValidateAntiForgeryToken]
        public ActionResult Index()
        {
            var postKeys = Request.Form.AllKeys; // get all keys from POST data

            // if Delete button is pressed
            if (postKeys.Contains("action_delete"))
                return IndexDeleteItems();

            // if Edit button is pressed
            if (postKeys.Contains("action_edit"))
            {
                foreach (string key in Request.Form) // just get first available number
                {
                    int val;
                    if (int.TryParse(key, out val))
                    {
                        return RedirectToAction("Edit", new { id = val });
                    }
                }
            }

            // try to find if [x] button is pressed along the entry row
            foreach (var key in postKeys)
            {
                if (Regex.IsMatch(key, "^delete_[0-9]+$"))
                {
                    string[] split = key.Split(new char[] { '_' });
                    int id = 0;
                    if (split.Length != 2)
                        break;
                    // try parse delete_<id>
                    if (!int.TryParse(split.ElementAt(1), out id))
                        break;
                    return Delete(id);
                }
            }

            // if no action was found
            ViewBag.ErrorName = "Неверный запрос";
            ViewBag.ErrorReason = "Не удалось найти выбранное вами действие.";
            return View("Error");
        }

        // Aggregate list of Audit IDs from POST data, iterate and delete all of them 
        [NonAction]
        public ActionResult IndexDeleteItems()
        {
            // parse list of <input checkbox name="<id>" />
            var itemsToDelete = new List<int>();
            foreach (string key in Request.Form)
            {
                int val;
                if (int.TryParse(key, out val))
                {
                    itemsToDelete.Add(val);
                }
            }

            // iterate over itemsToDelete, commit deletion on each cycle
            var i = 0;
            foreach (var id in itemsToDelete)
            {
                if (!AuditsService.DeleteById(id))
                {
                    ViewBag.ErrorName = "Ошибка удаления";
                    ViewBag.ErrorReason = $"Не удалось удалить выбранные записи частично/полностю. Удалены первые {i} записей из [{string.Join(", ", itemsToDelete.ToArray())}].";
                    return View("Error");
                }
                i++;
            }

            return Request.UrlReferrer != null ? Redirect(Request.UrlReferrer.ToString()) : Redirect("Audits/Index");
        }

        // POST: Audit/Delete/5
        // Delete Audit by `id`.
        [HttpPost]
        [HasPermission(Permission = "WriteAudits")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            if (!AuditsService.DeleteById(id))
            {
                ViewBag.ErrorName = "Ошибка удаления";
                ViewBag.ErrorReason = $"Не удалось удалить выбранную запись из базы данных.";
                return View("Error");
            }

            return RedirectToAction("Index");
        }

        // GET: Audit/Create
        //[HasPermission(Permission = "WriteAudits")]
        [HttpGet]
        public ActionResult Create()
        {
            var currUser = AuditsService.GetUserDetails(_identity);
            if (currUser == null)
            {
                ViewBag.ErrorName = "Пользователь не найден";
                ViewBag.ErrorReason = "Не удалось распознать текущего пользователя.";
                return View("Error");
            }
            return ViewAudit(new Audit() { AuthorID = currUser.ID, Auditor = currUser, IsEditable = true, InputDate = DateTime.Now, Date = DateTime.Now });
        }

        // POST: Audit/Create
        // Creates new Audit by binding the model with POST data.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [HasPermission(Permission = "WriteAudits")]
        public ActionResult Create(Audit audit)
        {
            // check if current user exists in DB
            var currUser = AuditsService.GetUserDetails(_identity);
            if (currUser == null)
            {
                ModelState.AddModelError("_Summary", "Не удалось распознать текущего пользователя.");
                return ViewAudit(audit);
            }
            audit.Auditor = currUser;

            // [Create] update AuthorID only once and if entry is new!
            if (audit.ID == 0)
                audit.AuthorID = currUser.ID;

            // update Attendees from POST
            AuditsService.AddAttendees(audit, Request, ModelState);

            // update Date and InputDate from POST
            AuditsService.AddDates(audit, Request, ModelState);

            // check if SubdivisionID exists in DB
            audit.Subdivision = _db.Subdivisions.Find(audit.SubdivisionID);
            if (audit.Subdivision == null)
                ModelState.AddModelError("SubdivisionID", "Не удалось распознать выбранное подразделение.");

            // check if ChecklistID exists in DB
            audit.Checklist = _db.Checklists.Find(audit.ChecklistID);
            if (audit.Checklist == null)
                ModelState.AddModelError("ChecklistID", "Не удалось распознать выбранный чек-лист в базе данных.");

            // append EvaluatedSections to audit from Request's POST data
            AuditsService.AddEvaluatedSections(audit, Request, ModelState);

            // final check if there is at least one error
            if (!ModelState.IsValid)
                return ViewAudit(audit);

            // form success message for Edit/Create actions
            TempData["SuccessMessage"] = audit.ID == 0
            ? "Аудит успешно создан! Теперь вы можете вернуться на основную страницу, либо перепроверить и внести поправки в созданный аудит."
            : "Аудит успешно обновлен! Вы можете вернуться на основную страницу.";

            // save audit to DB
            if (!AuditsService.Save(_db, audit))
            {
                ViewBag.ErrorName = "Ошибка сохранения";
                ViewBag.ErrorReason = "Не удалось сохранить созданный/редактируемый Аудит в базе данных.";
                return View("Error");
            }

            // redirect to Audit/Edit action
            return RedirectToAction("Edit", new { id = audit.ID });
        }

        // Returns view for creating new Audit with pre-filled data from arguments
        [HttpGet]
        public ActionResult CreateFromTimetable(int checklistId, int subdivisionId, string dateStr)
        {
            var date = DateTime.Parse(dateStr);

            var currUser = AuditsService.GetUserDetails(_identity);
            var audit = new Audit
            {
                AuthorID = currUser.ID,
                Auditor = currUser,
                ChecklistID = checklistId,
                SubdivisionID = subdivisionId,
                IsEditable = true,
                InputDate = DateTime.Now,
                Date = date
            };
            ViewBag.SubdivisionID = new SelectList(_db.Subdivisions, "ID", "Name", audit.SubdivisionID);
            ViewBag.ChecklistID = new SelectList(_db.Checklists, "ID", "Name", audit.ChecklistID);
            ViewBag.Role = new SelectList(_db.Roles, "ID", "Name");

            return View("Create", audit);
        }

        // GET: Audit/Edit/5
        [HasPermission(Permission = "WriteAudits")]
        public ActionResult Edit(int id)
        {
            var original = _db.Audits.Find(id);
            if (original == null)
            {
                ViewBag.ErrorName = "Аудит не найден";
                ViewBag.ErrorReason = "Не удалось найти выбранную запись [" + id + "].";
                return View("Error");
            }

            // check if there was SuccessMessage from Create action
            ViewBag.SuccessMessage = TempData["SuccessMessage"] == null ? null : TempData["SuccessMessage"].ToString();

            return ViewAudit(original);
        }

        // POST: Audit/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [HasPermission(Permission = "WriteAudits")]
        public ActionResult Edit(Audit audit)
        {
            // if Delete action is requested
            if (Request.Form.AllKeys.Contains("action_delete"))
                return Delete(audit.ID);

            // if given Audit is locked to edit // and not requested to unlock
            var original = _db.Audits.Find(audit.ID);
            if (!original.IsEditable) // && audit.IsEditable)
                return ViewAudit(original);

            // if author requested to edit is not original author
            var currUser = AuditsService.GetUserDetails(_identity);
            if (currUser != null && currUser.ID != original.AuthorID)
                return ViewAudit(original);

            // else update
            return Create(audit);
        }

        // "Shared" action for showing Audit's details on GET Create/Edit requets.
        [NonAction]
        public ActionResult ViewAudit(Audit audit)
        {
            ViewBag.SubdivisionID = new SelectList(_db.Subdivisions, "ID", "Name", audit.SubdivisionID);
            ViewBag.ChecklistID = new SelectList(_db.Checklists, "ID", "Name", audit.ChecklistID);
            ViewBag.Role = new SelectList(_db.Roles, "ID", "Name");

            return View(audit);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}