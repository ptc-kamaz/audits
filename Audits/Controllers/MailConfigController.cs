﻿using System.Configuration;
using System.IO;
using System.Web.Hosting;
using System.Web.Mvc;
using Audits.Models.Notifications;
using Audits.Models.Security;

namespace Audits.Controllers
{
    public class MailConfigController : Controller
    {
        public MailConfigController()
        {
            var configPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath,
                ConfigurationManager.AppSettings["MailServiceConfig"]);
            MailService = new MailService(configPath);
        }

        private MailService MailService { get; }

        [HasPermission(Permission = "WriteEmailConfig")]
        public ActionResult Update()
        {
            return View(MailService.LoadConfig());
        }

        [HasPermission(Permission = "WriteEmailConfig")]
        [HttpPost]
        public ActionResult Update(MailConfig mailConfig)
        {
            if (ModelState.IsValid)
            {
                if (MailService.SetConfig(mailConfig))
                {
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.ErrorName = "";
                ViewBag.ErrorReason = "";
                return View("Error");
            }
            return View(mailConfig);
        }
    }
}