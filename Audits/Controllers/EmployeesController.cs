﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web.Hosting;
using System.Web.Mvc;
using Audits.Models.Database_Manager;
using Audits.Models.EmployeesManager;
using Audits.Models.Notifications;
using Audits.Models.Security;
using PagedList;

namespace Audits.Controllers
{
    [HasPermission(Permission = "ViewUsers")]
    public class EmployeesController : Controller
    {
        public const int PageSize = 15;
        private readonly DatabaseEntities _db = new DatabaseEntities();

        private readonly WindowsIdentity _identity = WindowsIdentity.GetCurrent();

        // GET: Employees
        /// <summary>
        ///     Index page of employee controller. It contains the list of users.
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="page">number of page</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(string searchTerm, int page = 1)
        {
            using (var db = new DatabaseEntities())
            {
                if (!RoleDepository.HasPermission(_identity, "ViewUsers"))
                {
                    return View("~/Views/Error/Unauthorised.cshtml");
                }
                if (!RoleDepository.HasPermission(_identity, "EditUsers"))
                {
                    ViewData["EditUsers"] = "No";
                }
                if (Request.IsAjaxRequest())
                {
                    var searchresult = db.Employees.Include(e => e.Subdivision).Include(e => e.Role).Where(
                        e => searchTerm == null || e.FirstName.Contains(searchTerm) || e.LastName.Contains(searchTerm));
                    var l2 = searchresult.ToPagedList(page, PageSize);
                    return PartialView("PartialViews/EmployeesView", l2);
                }

                var employees = db.Employees.Include(e => e.Subdivision).Include(e => e.Role).Where(
                        e => searchTerm == null || e.FirstName.Contains(searchTerm) || e.LastName.Contains(searchTerm))
                    .OrderBy(e => e.ID).ToPagedList(page, PageSize);

                ViewBag.CurrentPage = page;
                return View(employees);
            }
        }

        // GET: Employees/Details/5
        /// <summary>
        ///     Method for detail information about account.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!RoleDepository.HasPermission(_identity, "EditUsers"))
            {
                ViewData["EditUsers"] = "No";
            }
            var employee = _db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        ///     Method for create new employee.
        /// </summary>
        /// <param name="searchTag"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create(string searchTag)
        {
            if (!RoleDepository.HasPermission(_identity, "EditUsers"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            if (Request.IsAjaxRequest() )
            {
                var records = Enumerable.Empty<ActiveDirectoryRecord>();
                
                if (!String.IsNullOrEmpty(searchTag))
                {
                    var activeDirectoryManager = new ActiveDirectoryManager();
                    try
                    {
                        records = activeDirectoryManager.LoadAd(searchTag);
                    }
                    catch (DirectoryServicesCOMException e)
                    {
                        Console.WriteLine(e);
                    }
                }
                return PartialView("PartialViews/ActiveDirectorySearchTableView", records);
            }
            ViewBag.SubdivisionID = new SelectList(_db.Subdivisions, "ID", "Name");
            ViewBag.Role = new SelectList(_db.Roles, "ID", "Name");
            return View();
        }

        /// <summary>
        ///     Method for saving of new Employee entity in data base.
        /// </summary>
        /// <param name="employee">All input paramateres bind into new Employee entity, except ID</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "ID")] Employee employee)
        {
            using (var db = new DatabaseEntities())
            {
                if (!RoleDepository.HasPermission(_identity, "EditUsers"))
                {
                    return View("~/Views/Error/Unauthorised.cshtml");
                }
                var lastId = db.Employees.Max(e => e.ID);
                employee.ID = lastId + 1;
                if (ModelState.IsValid && db.Employees.Find(employee.ID) == null)
                {
                    SaveToDBEmployeesRoleRelation(db, employee, employee.Role.First().ID);
                    var empl = db.Employees.FirstOrDefault(e => e.ID == employee.ID);
                    SaveToDBEmployeesRoleRelation(db, empl, employee.Role.First().ID);
                    if (empl != null)
                    {
                        NotifyEmployee(empl.Email, $"Здравствуйте, {empl.FirstName} {empl.Patronymic}.\n"
                                                   + $"Вы были добавлены в систему управления Аудитами. Вам присвоена роль {empl.Role.First()}.");
                    }
                    return RedirectToAction("Index");
                }

                ViewBag.SubdivisionID = new SelectList(db.Subdivisions, "ID", "Name", employee.SubdivisionID);
                ViewBag.Role = new SelectList(db.Roles, "ID", "Name", employee.Role);
                return View(employee);
            }
        }

        // GET: Employees/Edit/5
        /// <summary>
        ///     Method for edit the entity of Eployee with id which is number of account in database
        /// </summary>
        /// <param name="id">Employee.Id</param>
        /// <param name="searchTag"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int? id, string searchTag)
        {
            if (!RoleDepository.HasPermission(_identity, "EditUsers"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (Request.IsAjaxRequest())
            {
                var records = Enumerable.Empty<ActiveDirectoryRecord>();
                if (!string.IsNullOrEmpty(searchTag))
                {
                    var activeDirectoryManager = new ActiveDirectoryManager();
                    try
                    {
                        records = activeDirectoryManager.LoadAd(searchTag);
                    }
                    catch (DirectoryServicesCOMException e)
                    {
                        Console.WriteLine(e);
                    }
                }
                return PartialView("PartialViews/ActiveDirectorySearchTableView", records);
            }

            var employee = _db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }

            ViewBag.SubdivisionID = new SelectList(_db.Subdivisions, "ID", "Name", employee.SubdivisionID);
            ViewBag.Role = new SelectList(_db.Roles, "ID", "Name", employee.Role.FirstOrDefault()?.ID);
            return View(employee);
        }

        // POST: Employees/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        ///     Method is for saving changes of entity of Employee in database.
        /// </summary>
        /// <param name="employee"> All input parameters bind into employee entity</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include =
                "ID,LastName,FirstName,Patronymic,SubdivisionID,EstablishedPositionID,EstablishedPosition,ActualPosition,Email,Phone,PersonnelNumber,AD_ID,Role")]
            Employee employee)
        {
            using (var db = new DatabaseEntities())
            {
                if (!RoleDepository.HasPermission(_identity, "EditUsers"))
                {
                    return View("~/Views/Error/Unauthorised.cshtml");
                }
                if (ModelState.IsValid)
                {
                    var e = db.Employees.FirstOrDefault(em => em.ID == employee.ID);
                    if (e != null)
                    {
                        DeleteFromDbEmployeesRoleRelation(db, e);
                        SaveToDBEmployeesRoleRelation(db, employee, employee.Role.First().ID);
                        NotifyEmployee(e.Email, $"Здравствуйте, {e.FirstName} {e.Patronymic}"
                                                + $".\nВаши данные в ИС Управление аудитами были изменены. Ваша текущая роль в ИС {e.Role.First().Name}.");
                    }
                    return RedirectToAction("Index");
                }
                ViewBag.SubdivisionID = new SelectList(db.Subdivisions, "ID", "Name", employee.SubdivisionID);
                ViewBag.Role = new SelectList(db.Roles, "ID", "Name", employee.Role.FirstOrDefault()?.ID);
                return View(employee);
            }
        }

        // GET: Employees/Delete/5
        /// <summary>
        ///     Method for delete employee from data base.
        /// </summary>
        /// <param name="id">Employee.ID is number of account in database</param>
        /// <returns>Employee/Delete</returns>
        public ActionResult Delete(int? id)
        {
            if (!RoleDepository.HasPermission(_identity, "EditUsers"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = _db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        /// <summary>
        ///     Method for confirmed delete operation of Empoyee from data base.
        /// </summary>
        /// <param name="id">Employee.ID is number of account in database</param>
        /// <returns>Employee/Index view</returns>
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!RoleDepository.HasPermission(_identity, "EditUsers"))
            {
                return View("~/Views/Error/Unauthorised.cshtml");
            }
            using (var db = new DatabaseEntities())
            {
                var employee = db.Employees.Find(id);
                if (employee != null)
                {
                    var roleDb = employee.Role.FirstOrDefault();
                    db.Roles.FirstOrDefault(role => role.ID == roleDb.ID)?.Employees.Remove(employee);
                    db.Employees.Remove(employee);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        ///     Method for saving to DB and changing the entities in many-to-many relations
        ///     between Employee and Role.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="employeeEntry"></param>
        /// <param name="roleID"></param>
        private static void SaveToDBEmployeesRoleRelation(DatabaseEntities dbContext, Employee employeeEntry, int roleID)
        {
            var employee = dbContext.Employees.FirstOrDefault(empl => empl.ID == employeeEntry.ID);
            if (employee == null)
            {
                employee = employeeEntry;
                dbContext.Employees.Attach(employee);
                dbContext.Employees.Add(employee);
            }
            else
            {
                var newrole = dbContext.Roles.FirstOrDefault(role => role.ID == roleID);
                if (newrole != null)
                {
                    employee.Role.Add(newrole);
                    newrole.Employees.Add(employee);
                    dbContext.Employees.Attach(employee);
                    dbContext.Roles.Attach(newrole);
                }
            }

            employee.LastName = employeeEntry.LastName;
            employee.FirstName = employeeEntry.FirstName;
            employee.Email = employeeEntry.Email;
            employee.AD_ID = employeeEntry.AD_ID;
            employee.ActualPosition = employeeEntry.ActualPosition;
            employee.EstablishedPosition = employeeEntry.EstablishedPosition;
            employee.EstablishedPositionID = employeeEntry.EstablishedPositionID;
            employee.Patronymic = employeeEntry.Patronymic;
            employee.PersonnelNumber = employeeEntry.PersonnelNumber;
            employee.Phone = employeeEntry.Phone;
            employee.Subdivision = dbContext.Subdivisions.First(sub => sub.ID == employeeEntry.Subdivision.ID);
            employee.SubdivisionID = employeeEntry.SubdivisionID;

            dbContext.SaveChanges();
        }

        /// <summary>
        ///     Method for correct delete operation virtual entity from many-to-many relation
        ///     between Employee and Role.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="employeeEntry"></param>
        private static void DeleteFromDbEmployeesRoleRelation(DatabaseEntities dbContext, Employee employeeEntry)
        {
            var e = dbContext.Employees.FirstOrDefault(em => em.ID == employeeEntry.ID);
            if (e?.Role.FirstOrDefault() == null)
            {
                return;
            }
            var firstOrDefault = e.Role.FirstOrDefault();
            if (firstOrDefault != null)
            {
                var id = firstOrDefault.ID;
                var ro = dbContext.Roles.FirstOrDefault(role => role.ID == id);
                e.Role.Remove(ro);
                ro?.Employees.Remove(e);
            }
            dbContext.SaveChanges();
        }

        [HasPermission(Permission = "EditUsers")]
        public ActionResult GetResult(
            [Bind(Include = "Id,FirstName,LastName,Email,Department,samaccountname, DisplayName")]
            ActiveDirectoryRecord record)
        {
            if (Request.IsAjaxRequest())
            {
                var activeDirectoryManager = new ActiveDirectoryManager();
                var sid = activeDirectoryManager.LoadGroups(record);
                ViewBag.SID = sid;
                return PartialView("PartialViews/ADTextEditorView");
            }
            throw new InvalidOperationException();
        }

        private void NotifyEmployee(string email, string bodyText)
        {
            var configPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath,
                ConfigurationManager.AppSettings["MailServiceConfig"]);
            var mailService = new MailService(configPath);
            var mail = new Mail
            {
                Body = bodyText,
                To = email,
                Subject = "ИС Управление аудитами"
            };
            mailService.Send(mail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}