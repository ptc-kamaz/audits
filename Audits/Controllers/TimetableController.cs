﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Audits.Models.Database_Manager;
using Audits.Models.EmployeesManager;
using Audits.Models.Security;
using Audits.Models.Subdivisions;
using Audits.Models.Timetable;
using NLog;

namespace Audits.Controllers
{
    [HasPermission(Permission = "ReadTimetable")]
    public class TimetableController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly DatabaseEntities _db = new DatabaseEntities();

        public ActionResult Index()
        {
            ViewBag.CanWriteTimetable =
                RoleDepository.HasPermission(HttpContext.GetWindowsIdentity(), "WriteTimetable");
            return View(Enumerable.Empty<TimetableRecord>());
        }

        public ActionResult GetDayValues(int year, int month)
        {
            bool[] daysWithRecords;
            try
            {
                var subdivisionService = new SubdivisionService();
                var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions(HttpContext.GetUserAdId());
                var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

                daysWithRecords = timetableService.DaysWithRecords(year, month);
            }
            catch (Exception)
            {
                daysWithRecords = Enumerable.Empty<bool>().ToArray();
            }
            return Json(daysWithRecords, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int? year, int? month, int? day)
        {
            if (year == null || month == null || day == null)
            {
                ViewBag.CanWriteTimetable =
                    RoleDepository.HasPermission(HttpContext.GetWindowsIdentity(), "WriteTimetable");
                return PartialView("PartialViews/ListOfScheduledAudits", Enumerable.Empty<TimetableRecord>());
            }

            try
            {
                var subdivisionService = new SubdivisionService();
                var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions(HttpContext.GetUserAdId());
                var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

                var records = timetableService.LoadRecords(year.Value, month.Value, day.Value);

                ViewBag.CanWriteTimetable =
                    RoleDepository.HasPermission(HttpContext.GetWindowsIdentity(), "WriteTimetable");
                return PartialView("PartialViews/ListOfScheduledAudits", records);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                ViewBag.ErrorName = "Ошибка";
                ViewBag.ErrorReason = ex.Message;
                return View("Error");
            }
        }

        public ActionResult TimetableRecordDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                var timetableRecord = _db.TimetableRecords.Find(id.Value);

                return View("Details", timetableRecord);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                ViewBag.ErrorName = "Ошибка";
                ViewBag.ErrorReason = ex.Message;
                return View("Error");
            }
        }

        [HasPermission(Permission = "WriteTimetable")]
        public ActionResult Create()
        {
            try
            {
                var subdivisionService = new SubdivisionService();
                var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions(HttpContext.GetUserAdId());

                ViewBag.ChecklistID = new SelectList(_db.Checklists, "ID", "Name");
                ViewBag.SubdivisionID = new SelectList(subdivisions, "ID", "Name");

                return View();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                ViewBag.ErrorName = "Ошибка";
                ViewBag.ErrorReason = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HasPermission(Permission = "WriteTimetable")]
        public ActionResult Create(
            [Bind(Include = "Date,ChecklistID,ID,SubdivisionID")] TimetableRecord timetableRecord)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _db.TimetableRecords.Add(timetableRecord);
                    _db.SaveChanges();
                    Logger.Info("User {" + HttpContext.GetUserAdId() + "} has created new Timetable record.");
                    return RedirectToAction("Index");
                }

                var subdivisionService = new SubdivisionService();
                var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions(HttpContext.GetUserAdId());

                ViewBag.ChecklistID = new SelectList(_db.Checklists, "ID", "Name", timetableRecord.ChecklistID);
                ViewBag.SubdivisionID = new SelectList(subdivisions, "ID", "Name", timetableRecord.SubdivisionID);

                return View(timetableRecord);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                ViewBag.ErrorName = "Ошибка";
                ViewBag.ErrorReason = ex.Message;
                return View("Error");
            }
        }

        [HasPermission(Permission = "WriteTimetable")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var timetableRecord = _db.TimetableRecords.Find(id);
            if (timetableRecord == null)
            {
                return HttpNotFound("Указанная запись расписания не была найдена.");
            }

            try
            {
                var subdivisionService = new SubdivisionService();
                var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions(HttpContext.GetUserAdId());

                ViewBag.ChecklistID = new SelectList(_db.Checklists, "ID", "Name", timetableRecord.ChecklistID);
                ViewBag.SubdivisionID = new SelectList(subdivisions, "ID", "Name", timetableRecord.SubdivisionID);

                return View(timetableRecord);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                ViewBag.ErrorName = "Ошибка";
                ViewBag.ErrorReason = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HasPermission(Permission = "WriteTimetable")]
        public ActionResult Edit([Bind(Include = "Date,ChecklistID,ID,SubdivisionID")] TimetableRecord timetableRecord)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(timetableRecord).State = EntityState.Modified;
                _db.SaveChanges();
                Logger.Info("User {" + HttpContext.GetUserAdId() + "} has changed Timetable record with Id " +
                            timetableRecord.ID + ".");
                return RedirectToAction("Index");
            }

            try
            {
                var subdivisionService = new SubdivisionService();
                var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions(HttpContext.GetUserAdId());

                ViewBag.ChecklistID = new SelectList(_db.Checklists, "ID", "Name", timetableRecord.ChecklistID);
                ViewBag.SubdivisionID = new SelectList(subdivisions, "ID", "Name", timetableRecord.SubdivisionID);
                return View(timetableRecord);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                ViewBag.ErrorName = "Ошибка";
                ViewBag.ErrorReason = ex.Message;
                return View("Error");
            }
        }

        [HasPermission(Permission = "WriteTimetable")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var timetableRecord = _db.TimetableRecords.Find(id);
            if (timetableRecord == null)
            {
                return HttpNotFound();
            }
            return View(timetableRecord);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [HasPermission(Permission = "WriteTimetable")]
        public ActionResult DeleteConfirmed(int id)
        {
            var timetableRecord = _db.TimetableRecords.Find(id);
            if (timetableRecord != null)
            {
                _db.TimetableRecords.Remove(timetableRecord);
                _db.SaveChanges();
                Logger.Info("User {" + HttpContext.GetUserAdId() + "} has removed Timetable record with Id " +
                            timetableRecord.ID + ".");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}