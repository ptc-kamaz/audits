﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Audits.Models.Database_Manager;
using Audits.Models.EmployeesManager;
using Audits.Models.Security;
using Audits.Models.Subdivisions;
using Newtonsoft.Json;
using NLog;

namespace Audits.Controllers
{
    public class SubdivisionsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly DatabaseEntities _db = new DatabaseEntities();

        [HasPermission(Permission = "ReadStructure")]
        public ActionResult Index()
        {
            return View();
        }

        [JsonNetFilter]
        [HasPermission(Permission = "ReadStructure")]
        public ActionResult JsonTree()
        {
            TreeNode<Subdivision> root;
            try
            {
                root = TreeFactory.GenerateTree(_db.Subdivisions.AsNoTracking());
            }
            catch (RootAbsenceException ex)
            {
                // Only default ASCII characters are supported by http responses.
                Logger.Error(ex);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }
            catch (RootSurfeitException ex)
            {
                // Only default ASCII characters are supported by http responses.
                Logger.Error(ex);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }
            return Json(root, JsonRequestBehavior.AllowGet);
        }

        [JsonNetFilter]
        [HasPermission(Permission = "ReadStructure")]
        public ActionResult JsonSubdivisionEmployees(int id)
        {
            var employees = _db.Employees
                .AsNoTracking()
                .Where(e => e.SubdivisionID == id)
                .Select(e => new
                {
                    e.ID,
                    e.LastName,
                    e.FirstName,
                    e.Patronymic,
                    e.SubdivisionID,
                    e.Email
                }).ToList();
            return Json(employees, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JsonNetFilter]
        [HasPermission(Permission = "WriteStructure")]
        public ActionResult SaveJsonTree()
        {
            // Read the content of the response.
            string str;
            using (var reader = new StreamReader(Request.InputStream))
            {
                str = reader.ReadToEnd();
            }
            if (string.IsNullOrWhiteSpace(str))
            {
                Logger.Error("BadRequest. The input data can't be empty!");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "The input data can't be empty!");
            }

            var service = new SubdivisionService();
            // Serialize the content of the response into the Tree of objects.
            var result = JsonConvert.DeserializeObject<TreeNode<Subdivision>[]>(str);
            // Save and/or update nodes in the database.
            var subdivisions = TreeFactory.GenerateSubdivisions(result[0]);

            service.SaveOrUpdate(subdivisions);
            Logger.Info("User {" + HttpContext.GetUserAdId() + "} added/changed " + subdivisions.Count + " elements.");

            // Remove the nodes from the database.
            var removalList = new List<Subdivision>();
            for (var i = 1; i < result.Length; ++i)
            {
                removalList.AddRange(TreeFactory.GenerateSubdivisions(result[i]));
            }
            service.Remove(removalList);
            Logger.Info("User {" + HttpContext.GetUserAdId() + "} removed " + subdivisions.Count + " elements.");

            return View("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}