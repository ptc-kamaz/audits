﻿function updateSections() {
    var subdivisionId = $("#report-subdivision").val();
    $.ajax({
        url: "/Reports/GetSections",
        data: {
            subdivisionId: subdivisionId
        },
        success: function(sections) {
            var data = $.parseJSON(sections);
            $("#report-section")
                .find("option")
                .remove()
                .end();
            if (!$.isEmptyObject(data)) {
                $.each(data,
                    function(key, value) {
                        $("#report-section")
                            .append("<option value=" + key + ">" + value + "</option>");
                    });
            } else {
                $("#report-section")
                    .append("<option value=0>Нет доступных номинаций</option>");
            }

        },
        error: function() {
            alert("Ошибка сервера");
        }
    });
}

function updateSubdivisions() {
    var subdivisionId = $("#report-subdivision").val();
    $.ajax({
        url: "/Reports/GetSubdivisions",
        data: {
            parentId: subdivisionId
        },
        success: function(subdivisions) {
            var data = $.parseJSON(subdivisions);
            $("#selected-subdivision")
                .find("option")
                .remove()
                .end();
            if (!$.isEmptyObject(data)) {
                $.each(data,
                    function(key, value) {
                        $("#selected-subdivision")
                            .append("<option value=" + key + ">" + value + "</option>");
                    });
            } else {
                $("#selected-subdivision")
                    .append("<option value=0>Нет доступных подразделений</option>");
            }

        },
        error: function() {
            alert("Ошибка сервера");
        }
    });
}

function updateTable() {
    var subdivisionId = $("#report-subdivision").val();
    var sectionId = $("#sectionId").val();
    sectionId = (sectionId == null) ? 0 : sectionId;
    subdivisionId = (subdivisionId == null) ? 0 : subdivisionId;
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var reportType = $("#reportType").val();

    $.ajax({
        url: $("#getDataTable").val(),
        data: {
            subdivisionId: subdivisionId,
            sectionId: sectionId,
            startDate: startDate,
            endDate: endDate,
            reportType: reportType
        },
        success: function(table) {
            $(".js-data-table").html(table);
        },
        error: function() {
            alert("Ошибка сервера");
        }
    });
}

function initDatepicker() {
    $(".input-daterange input").each(function() {
        $(this).datepicker({
            format: "dd/mm/yyyy",
            startView: "days",
            minViewMode: "days",
            language: "ru"
        });
    });
}

$(document)
    .ready(function() {
        $(".js-report-input")
            .on("change",
                function() {
                    updateTable();
                });
    });