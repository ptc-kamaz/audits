﻿$(document)
    .ready(function() {
        init();
    });

function init() {
    reorderAll();
    disableItemsCantBeDeleted();
    initSectionAddition();
    initParameterAddition();
    initParameterDeletion();
    initSectionDeletion();
    initChecklistDeletion();
}

function disableItemsCantBeDeleted() {
    if ($(checklist.index).length > 0) {
        disableEvalutedChecklistDeletion();
    } else {
        disableUnremovableItems();
    }
}

function disableEvalutedChecklistDeletion() {
    $(checklist.id)
        .each(function() {
            var $removeButton = $(this).closest(checklist.index).find(checklist.class.remove);
            var checklistId = $(this).attr("value");
            $.ajax({
                url: "/Checklists/CanChecklistBeDeleted/",
                data: {
                    checklistId: checklistId
                },
                success: function(answer) {
                    if (answer === "False") {
                        $removeButton.removeClass("js-remove-checklist");
                        $removeButton.addClass("disabled");
                    }
                },
                error: function() {
                    alert("Ошибка! Выбранный чек-лист не может быть удалён.");
                }
            });
        });
}

function disableUnremovableItems() {
    var checklistId = $(checklist.id).attr("value");
    if (checklistId) {
        $.ajax({
            url: "/Checklists/GetParameterWhichsCantBeDeleted/",
            data: {
                checklistId: checklistId
            },
            success: function(parameters) {
                var unremovedParemetersId = parameters.split(" ");
                $(parameter.class.self).each(function() {
                    var parameterId = $(this).find(parameter.id).attr("value");
                    if ($.inArray(parameterId, unremovedParemetersId) > 0) {
                        var $parameter = $(this);
                        //disable parameter deletion
                        makeItemConstantlyDisabled($parameter);
                        //disable section deletion
                        var $section = $parameter.closest(section.class.self);
                        makeItemConstantlyDisabled($section);
                    }
                });
            },
            error: function() {
                alert("Ошибка сервера!");
            }
        });
    }
}

function makeItemConstantlyDisabled(item) {
    $(item.find(common.class.removing)[0]).addClass("disabled");
    item.data("disabled", "true");
}

function initSectionAddition() {
    $(section.class.add)
        .on("click",
            function() {
                $.ajax({
                    url: "/Checklists/AddSection/",
                    success: function(newSection) {
                        $(section.container.id).append(newSection);
                        reorderAll();
                    },
                    error: function() {
                        alert("Ошибка! Не удалось добавить раздел чек-листа.");
                    }
                });
            });
}

function initParameterAddition() {
    $(section.container.id)
        .on("click",
            parameter.class.add,
            function(event) {
                var $this = $(event.target);
                var htmlFieldPrefix = $this.closest(parameter.class.block)
                    .find(parameter.class.self)
                    .data("html-field-prefix");
                $.ajax({
                    url: "/Checklists/AddParameter/",
                    data: {
                        htmlFieldPrefix: htmlFieldPrefix
                    },
                    success: function(newParameter) {
                        var $parametersBody = $this.closest(parameter.class.block).find(parameter.class.body);
                        $parametersBody.append(newParameter);
                        reorderAll();
                    },
                    error: function() {
                        alert("Ошибка! Не удалось добавить параметр.");
                    }
                });
            });
}

function initParameterDeletion() {
    $(section.container.id)
        .on("click",
            parameter.class.remove,
            function(event) {
                var $parameter = ($(event.target).hasClass(parameter.class.remove))
                    ? $(event.target)
                    : $(event.target).closest(parameter.class.remove);
                if (!$parameter.hasClass("disabled")) {
                    confirmParametersDeletion($parameter);
                }
            });
}

function initSectionDeletion() {
    $(section.container.id)
        .on("click",
            section.class.remove,
            function(event) {
                var $section = $(event.target);
                if (!$section.hasClass("disabled")) {
                    confirmSectionDeletion($section);
                }
            });
}

function initChecklistDeletion() {
    $(checklist.container.id)
        .on("click",
            checklist.class.remove,
            function(event) {
                var $target = $(event.target);
                if (!$target.hasClass(checklist.class.remove)) {
                    $target = $target.closest(checklist.class.remove);
                }
                var $form = $target.nextAll(checklist.form.delete);
                confirmChecklistDeletion($form);
            });
}

function reorderAll() {
    var sections = $(section.class.self);
    setOrderForItems(sections, section.class.ordinal);
    sections.each(function() {
        setOrderForItems($(this).find(parameter.class.self), parameter.class.ordinal);
    });
}

function setOrderForItems(items, className) {
    var index = 1;
    switchDisableStateForRemovingButton(items);
    items.each(function() {
        $(this).find(className).attr("value", index++);
    });
}

function switchDisableStateForRemovingButton(items) {
    if (items.length === 1) {
        $($(items[0]).find(common.class.removing)[0]).addClass("disabled");
    } else {
        items = removeConstantlyDisabledItems(items);
        items.each(function() {
            $(this).find(common.class.removing).removeClass("disabled");
        });
    }
}

function removeConstantlyDisabledItems(items) {
    var i = 0;
    while (i < items.length) {
        if ($(items[i]).data("disabled") === "true") {
            items.splice(i, 1);
        } else {
            ++i;
        }
    }
    return items;
}

function setOrderForParameters(parameters) {
    setOrderForItems(parameters, parameter.class.ordinal);
}

function setOrderForSections(sections) {
    setOrderForItems(sections, section.class.ordinal);
}

function deleteParameters(items) {
    items.each(function() {
        var $this = $(this);
        var parameterBlock = $this.closest(parameter.class.self);
        parameterBlock.remove();
    });
}

function confirmParametersDeletion($parameters) {
    bootbox.confirm({
        message: "Удалить параметр?",
        buttons: bootboxButtons,
        callback: function(result) {
            if (result) {
                deleteParameters($parameters.closest(parameter.class.self));
                reorderAll();
            }
        }
    });
}

function confirmSectionDeletion($section) {
    bootbox.confirm({
        message: "Удалить раздел?",
        buttons: bootboxButtons,
        callback: function(result) {
            if (result) {
                $section.closest(section.class.self).remove();
                reorderAll();
            }
        }
    });
}

function confirmChecklistDeletion($deletionForm) {
    bootbox.confirm({
        message: "Удалить чеклист?",
        buttons: bootboxButtons,
        callback: function(result) {
            if (result) {
                $deletionForm.submit();
            }
        }
    });
}

var parameter = {
    get "class"() {
        return {
            ordinal: ".js-parameter-ordinal-number",
            add: ".js-add-parameter",
            block: ".js-parameters-block",
            body: ".js-parameters-body",
            self: ".js-parameter",
            remove: ".js-remove-parameter"
        };
    },
    get "id"() {
        return ".js-parameter-id-field";
    }
};


var section = {
    get "class"() {
        return {
            ordinal: ".js-section-ordinal-number",
            add: "#add-section",
            self: ".js-section",
            remove: ".js-remove-section"

        };
    },
    get container() {
        return {
            id: "#section-container"
        };
    }
};

var checklist = {
    get "class"() {
        return {
            remove: ".js-remove-checklist"
        };
    },
    get container() {
        return {
            id: "#checklist-table-container"
        };
    },
    get form() {
        return {
            delete: ".js-checklist-deletion-form"
        };
    },
    get id() {
        return ".js-checklist-id-field";
    },
    get index() {
        return ".js-checklist-index";
    }
};

var common = {
    get "class"() {
        return {
            "removing": ".js-removing"
        };
    }
};
var bootboxButtons = {
    confirm: {
        label: "Да",
        className: "btn-success"
    },
    cancel: {
        label: "Нет",
        className: "btn-danger"
    }
};