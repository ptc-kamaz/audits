﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Audits.Models.Database_Manager;

namespace Audits.Tests.Models.Subdivisions
{
    internal class SubdivisionFactory
    {
        private static readonly Random rand = new Random();

        /// <summary>
        ///     Generates specified amount of Subdivision instances whereas each next element
        ///     of the list is a chlid of a current.
        /// </summary>
        /// <param name="count">Amount of Subdivision instances to be generated</param>
        /// <returns>List of generated Subdivisions.</returns>
        public static List<Subdivision> GenerateSubdivisions(int count)
        {
            Subdivision subdivision;
            Subdivision previous = null;
            var list = new List<Subdivision>();
            for (var i = 0; i < count; ++i)
            {
                subdivision = new Subdivision
                {
                    // 40 is the maximum length of a subdivion's name.
                    Name = GenerateRandomString(40),
                    Parent = previous
                };
                previous = subdivision;
                list.Add(subdivision);
            }
            return list;
        }

        public static void CleanupSubdivisions(List<Subdivision> list)
        {
            using (var db = new DatabaseEntities())
            {
                foreach (var item in list)
                {
                    var subdivision = db.Subdivisions.FirstOrDefault(
                        s => item.Name.Equals(s.Name) && item.SAP_ID.Equals(s.SAP_ID));
                    if (subdivision != null)
                    {
                        db.Subdivisions.Remove(subdivision);
                    }
                }
                db.SaveChanges();
            }
        }

        public static string GenerateRandomString(int length)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < length; ++i)
            {
                sb.Append((char) rand.Next(65, 91));
            }
            return sb.ToString();
        }
    }
}