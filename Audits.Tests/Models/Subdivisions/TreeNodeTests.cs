﻿using System.Linq;
using Audits.Models.Subdivisions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Models.Subdivisions
{
    [TestClass]
    public class TreeNodeTests
    {
        [TestMethod]
        public void TreeNodeTest()
        {
            var node = new TreeNode<string>();
            Assert.AreEqual(0, node.ID);
            Assert.AreEqual(null, node.Text);
            Assert.AreEqual(null, node.Data);
        }

        [TestMethod]
        public void TreeNodeTest1()
        {
            var node = new TreeNode<string>(0, "text");
            Assert.AreEqual(0, node.ID);
            Assert.AreEqual("text", node.Text);
            Assert.AreEqual(null, node.Data);
        }

        [TestMethod]
        public void TreeNodeTest2()
        {
            var node = new TreeNode<string>(0, "text", "data");
            Assert.AreEqual(0, node.ID);
            Assert.AreEqual("text", node.Text);
            Assert.AreEqual("data", node.Data);
        }


        [TestMethod]
        public void TreeNodeTest3()
        {
            var node = new TreeNode<string>(0, "text", "data");
            Assert.AreEqual(0, node.Children.Count);

            var child1 = new TreeNode<string>(0, "text1", "data1");
            var child2 = new TreeNode<string>(0, "text2", "data2");
            node.Children.Add(child1);
            node.Children.Add(child2);

            Assert.AreEqual(child1, node.Children.ElementAt(0));
            Assert.AreEqual(child2, node.Children.ElementAt(1));
        }
    }
}