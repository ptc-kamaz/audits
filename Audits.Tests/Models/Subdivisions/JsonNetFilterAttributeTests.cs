﻿using System;
using System.Web;
using System.Web.Mvc;
using Audits.Models.Database_Manager;
using Audits.Models.Subdivisions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace Audits.Tests.Models.Subdivisions
{
	[TestClass]
	public class JsonNetFilterAttributeTests
	{
		[TestMethod]
		public void OnActionExecutedTest1()
		{
			var attribute = new JsonNetFilterAttribute();
			var context = new ActionExecutedContext();
			var result = new ViewResult();
			context.Result = result;

			attribute.OnActionExecuted(context);

			Assert.AreEqual(result, context.Result);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void OnActionExecutedTest2()
		{
			var attribute = new JsonNetFilterAttribute();
			// Setup context for the attribute execution.
			var context = new ActionExecutedContext();
			var subdivision = new Subdivision
			{
				Name = "Subdivision Name",
				SAP_ID = "SAP-ID__SAP-ID"
			};
			var result = new JsonResult {Data = JsonConvert.SerializeObject(subdivision)};
			context.Result = result;
			// Execute action and obtain a result.
			attribute.OnActionExecuted(context);
			// Setup parameters of Json result.
			var jsonResult = context.Result as JsonResult;
			jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

			jsonResult.ExecuteResult(null);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void OnActionExecutedTest3()
		{
			var attribute = new JsonNetFilterAttribute();
			// Setup context for the attribute execution.
			var context = new ActionExecutedContext();
			var subdivision = new Subdivision
			{
				Name = "Subdivision Name",
				SAP_ID = "SAP-ID__SAP-ID"
			};
			var result = new JsonResult {Data = JsonConvert.SerializeObject(subdivision)};
			context.Result = result;
			// Execute action and obtain a result.
			attribute.OnActionExecuted(context);
			// Setup parameters of Json result.
			var jsonResult = context.Result as JsonResult;
			jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

			var httpRequestBaseMock = new Mock<HttpRequestBase>();
			var httpContextBaseMock = new Mock<HttpContextBase>();
			var controllerContextMock = new Mock<ControllerContext>();

			httpContextBaseMock
				.SetupGet(x => x.Request)
				.Returns(httpRequestBaseMock.Object);

			controllerContextMock
				.SetupGet(x => x.HttpContext)
				.Returns(httpContextBaseMock.Object);

			jsonResult.ExecuteResult(controllerContextMock.Object);
			
			Assert.AreEqual(result.Data, jsonResult.Data);
		}

		[TestMethod]
		public void OnActionExecutedTest4()
		{
			var attribute = new JsonNetFilterAttribute();
			// Setup context for the attribute execution.
			var context = new ActionExecutedContext();
			var subdivision = new Subdivision
			{
				Name = "Subdivision Name",
				SAP_ID = "SAP-ID__SAP-ID"
			};
			var result = new JsonResult {Data = JsonConvert.SerializeObject(subdivision)};
			context.Result = result;
			// Execute action and obtain a result.
			attribute.OnActionExecuted(context);
			// Setup parameters of Json result.
			var jsonResult = context.Result as JsonResult;
			jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

			var httpRequestBaseMock = new Mock<HttpRequestBase>();
			var httpResponseBaseMock = new Mock<HttpResponseBase>();
			var httpContextBaseMock = new Mock<HttpContextBase>();
			var controllerContextMock = new Mock<ControllerContext>();

			httpRequestBaseMock
				.SetupGet(x => x.HttpMethod)
				.Returns("GET");

			httpContextBaseMock
				.SetupGet(x => x.Request)
				.Returns(httpRequestBaseMock.Object);
			httpContextBaseMock
				.SetupGet(x => x.Response)
				.Returns(httpResponseBaseMock.Object);

			controllerContextMock
				.SetupGet(x => x.HttpContext)
				.Returns(httpContextBaseMock.Object);

			jsonResult.ExecuteResult(controllerContextMock.Object);

			Assert.AreEqual(result.Data, jsonResult.Data);
		}
	}
}