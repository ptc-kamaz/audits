﻿using System.Linq;
using Audits.Models.Database_Manager;
using Audits.Models.Subdivisions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Models.Subdivisions
{
    [TestClass()]
    public class TreeFactoryTests
    {
        private readonly int subdivisionsCount = 5;

        [TestMethod()]
        public void GenerateTreeTest()
        {
            var list = SubdivisionFactory.GenerateSubdivisions(subdivisionsCount);
            SubdivisionService service = new SubdivisionService();
            service.SaveOrUpdate(list);

            list.First().HeadID = -1;
            list.First().ParentID = -1;
            var root = TreeFactory.GenerateTree(list);
            var collection = TreeFactory.GenerateSubdivisions(root);

            GenerateTreeTest(root);
            Assert.AreEqual(list.Count, collection.Count);
            Assert.IsTrue(list.All(collection.Contains));

            service.Remove(list);
        }

        private void GenerateTreeTest(TreeNode<Subdivision> node)
        {
            foreach (var item in node.Children)
            {
                Assert.AreEqual(node.Data, item.Data.Parent);
                GenerateTreeTest(item);
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(RootSurfeitException))]
        public void GenerateTreeRootSurfeitExceptionTest()
        {
            var list = SubdivisionFactory.GenerateSubdivisions(subdivisionsCount);
            list.First().Parent = null;
            list.Last().Parent = null;
            TreeFactory.GenerateTree(list);
        }

        [TestMethod()]
        [ExpectedException(typeof(RootAbsenceException))]
        public void GenerateTreeRootAbsenceExceptionTest()
        {
            var list = SubdivisionFactory.GenerateSubdivisions(subdivisionsCount);
            list.First().Parent = list.Last();
            TreeFactory.GenerateTree(list);
        }
    }
}