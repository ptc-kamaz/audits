﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Audits.Models.Database_Manager;
using Audits.Models.Subdivisions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Audits.Tests.Models.Subdivisions
{
    [TestClass]
    public class SubdivisionServiceTests
    {
        [TestMethod]
        public void SaveOrUpdateTest()
        {
            var service = new SubdivisionService();
            var list = SubdivisionFactory.GenerateSubdivisions(10);
            // Save new entities for the first time.
            Assert.AreEqual(0, service.SaveOrUpdate(new List<Subdivision>()));
            Assert.AreEqual(list.Count, service.SaveOrUpdate(list));
            using (var db = new DatabaseEntities())
            {
                foreach (var item in list)
                {
                    var subdivision = db.Subdivisions.FirstOrDefault(
                        s => item.Name.Equals(s.Name) && item.SAP_ID.Equals(s.SAP_ID));
                    Assert.IsNotNull(subdivision);
                }
            }
            // Save entities for the second time.
            Assert.AreEqual(list.Count, service.SaveOrUpdate(list));
            using (var db = new DatabaseEntities())
            {
                foreach (var item in list)
                {
                    var subdivision = db.Subdivisions.FirstOrDefault(
                        s => item.Name.Equals(s.Name) && item.SAP_ID.Equals(s.SAP_ID));
                    Assert.IsNotNull(subdivision);
                }
            }
            SubdivisionFactory.CleanupSubdivisions(list);
        }

        [TestMethod]
        public void RemoveTest()
        {
            var service = new SubdivisionService();
            var list = SubdivisionFactory.GenerateSubdivisions(10);
            Assert.AreEqual(0, service.Remove(new List<Subdivision>()));
            Assert.AreEqual(list.Count, service.SaveOrUpdate(list));
            Assert.AreEqual(list.Count, service.Remove(list));
            using (var db = new DatabaseEntities())
            {
                foreach (var item in list)
                {
                    var subdivision = db.Subdivisions.FirstOrDefault(
                        s => item.Name.Equals(s.Name) && item.SAP_ID.Equals(s.SAP_ID));
                    Assert.IsNull(subdivision);
                }
            }
            SubdivisionFactory.CleanupSubdivisions(list);
        }

        [TestMethod]
        public void LoadListOfSubordiateSubdivisions()
        {
            var service = new SubdivisionService();
            
            var subdivisions = service.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");

            Assert.AreEqual(6, subdivisions.Count);
        }
    }
}