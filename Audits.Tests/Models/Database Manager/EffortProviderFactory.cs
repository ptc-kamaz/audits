﻿using System.Data.Common;
using System.Data.Entity.Infrastructure;
using Effort;

namespace Audits.Tests.Models.Database_Manager
{
    public class EffortProviderFactory : IDbConnectionFactory
    {
        private static DbConnection _connection;
        private static readonly object LockObject = new object();

        public DbConnection CreateConnection(string nameOrConnectionString)
        {
            lock (LockObject)
            {
                if (_connection == null)
                {
                    _connection = EntityConnectionFactory.CreateTransient(
                        "metadata='res://*/Models.Database Manager.DatabaseModel.csdl|" +
                        "res://*/Models.Database Manager.DatabaseModel.ssdl|" +
                        "res://*/Models.Database Manager.DatabaseModel.msl';" +
                        "provider=System.Data.SqlClient;provider connection string='data source=188.130.155.206;" +
                        "initial catalog=АСА;persist security info=True;user id=sa;password=cxfcnmt23k.ltq;" +
                        "multipleactiveresultsets=True;application name=EntityFramework'");
                }
                return _connection;
            }
        }

        public static void ResetDb()
        {
            lock (LockObject)
            {
                _connection = null;
            }
        }
    }
}