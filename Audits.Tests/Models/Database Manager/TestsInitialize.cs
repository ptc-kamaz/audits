﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Models.Database_Manager
{
    [TestClass]
    public class TestsInitialize
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
        }
    }
}
