﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Audits.Models.Database_Manager;

namespace Audits.Tests.Models.Database_Manager
{
    [TestClass]
    public class EmployeeTest
    {
        [TestMethod]
        public void TestAdIdLength()
        {
            Employee employee = new Employee();
            string adId = "123456789ABCDEF-123456789ABCDEF-123456789ABCDEF-123456789ABCDEF-"
                    + "123456789ABCDEF-123456789ABCDEF-123456789ABCDEF-123456789ABCDEF-"
                    + "123456789ABCDEF-123456789ABCDEF-123456789ABCDEF-123456789ABCDEF-"
                    + "123456789ABCDEF-123456789ABCDEF-123456789ABCDEF-0123456789ABCDEF";
            using (var db = new DatabaseEntities())
            {
                var subdivision = db.Subdivisions.First();
                employee.AD_ID = adId;
                employee.FirstName = "Firstname";
                employee.LastName = "Lastname";
                employee.Patronymic = "Patronymic";
                employee.Phone = "phone";
                employee.Email = "email@email.com";
                employee.Subdivision = subdivision;

                db.Employees.Add(employee);
                db.SaveChanges();
            }
            using (var db = new DatabaseEntities())
            {
                var res = db.Employees.First(e => string.Equals(adId, e.AD_ID));
                Assert.AreEqual(res, employee);
                db.Employees.Remove(res);
                db.SaveChanges();
            }
        }
    }
}
