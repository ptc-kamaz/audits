﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Audits.Models.Audits;
using Audits.Models.Database_Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Models
{
    [TestClass]
    public class AuditsServiceTest
    {
        [TestMethod]
        public void TestGetDetails()
        {
            AuditsService auditsService = new AuditsService();
            Assert.AreEqual(236, auditsService.GetDetails(236).ID);
        }

        [TestMethod]
        public void TestValidate()
        {
            AuditsService auditsService = new AuditsService();
            Assert.IsFalse(auditsService.Validate(1, new AuditValidate()
            {
                DateTime = System.DateTime.Now,
                EvaluatedValues = new List<EvaluatedValue>()
            }));
        }

        [TestMethod]
        public void CreateAudits()
        {
            using (var db = new DatabaseEntities())
            {
                // Checkhov Anton's identity
                var employee = db.Employees.FirstOrDefault(e => string.Equals(e.AD_ID, "S-1-5-21-3098789859-1521663269-1550652469-1001"));

                // Чек-лист оценки PSK-КАМАЗ
                var checkList = db.Checklists.Find(1);

                // Save audit to db
                Audit audit = new Audit()
                {
                    Attendees = "Федоров Григорий Александрович",
                    AuthorID = employee.ID,
                    EvaluatedSections = new List<EvaluatedSection>(),
                    ChecklistID = checkList.ID,
                    Date = DateTime.Today,
                    InputDate = DateTime.Today,
                    IsEditable = true,
                    SubdivisionID = employee.SubdivisionID
                };
                db.Audits.Add(audit);
                db.SaveChanges();

                // Get new added audit from db
                Audit eAudit = db.Audits
                    .Where(a => a.Attendees == audit.Attendees)
                    .Where(a => a.AuthorID == audit.AuthorID)
                    .Where(a => a.ChecklistID == audit.ChecklistID)
                    .Where(a => a.InputDate == audit.InputDate)
                    .FirstOrDefault();

                var eSections = new List<EvaluatedSection>();
                foreach (var section in checkList.Sections)
                {
                    var nSection = new EvaluatedSection()
                    {
                        AuditID = eAudit.ID,
                        SectionID = section.ID,
                        Photos = new List<Photo>(),
                        // Test Data
                        MaxGrade = 3,
                        // Empty sections
                        EvaluatedParameters = new List<EvaluatedParameter>(),
                        // Test Data
                        Grade = 3
                    };

                    // Save new template of evaluated section to db
                    db.EvaluatedSections.Add(nSection);
                    db.SaveChanges();

                    // Get section's id from db (for id)
                    EvaluatedSection eSection = db.EvaluatedSections
                        .Where(s => s.AuditID == nSection.AuditID)
                        .Where(s => s.SectionID == nSection.SectionID)
                        .FirstOrDefault();

                    eSection.EvaluatedParameters = new List<EvaluatedParameter>();
                    db.SaveChanges();

                    // Add new parameters
                    var eParameters = new List<EvaluatedParameter>();
                    foreach (var parameter in section.Parameters)
                    {
                        eParameters.Add(new EvaluatedParameter()
                        {
                            Actions = "Наладить работу запуска Космического КАМАЗа",
                            ParameterID = parameter.ID,
                            Value = parameter.MaxValue,
                            EvaluatedSectionID = eSection.ID
                        });
                    }

                    // Update section with parameters
                    eSection.EvaluatedParameters = eParameters;
                    db.SaveChanges();

                    // Add updated section to sections
                    eSections.Add(db.EvaluatedSections.Find(eSection.ID));
                }

                // Update evaluated audit values
                eAudit.EvaluatedSections = eSections;
                db.SaveChanges();
                
            }
        }
    }
}
