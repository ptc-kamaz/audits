﻿using System;
using System.Collections.Generic;
using System.Linq;
using Audits.Models.Subdivisions;
using Audits.Models.Timetable;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Models.Timetable
{
    [TestClass]
    public class TimetableServiceTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor()
        {
            new TimetableService(null);

            Assert.Fail();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void LoadRecordsTest_ExceptionForYear()
        {
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");
            var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

            timetableService.LoadRecords(1899, 1, 4);

            Assert.Fail();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void LoadRecordsTest_ExceptionForMonth()
        {
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");
            var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

            timetableService.LoadRecords(2010, 0, 4);

            Assert.Fail();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void LoadRecordsTest_ExceptionForDay()
        {
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");
            var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

            timetableService.LoadRecords(2017, 2, 29);

            Assert.Fail();
        }

        [TestMethod]
        public void LoadRecordsTest()
        {
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");
            var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

            var records = timetableService.LoadRecords(2017, 1, 4);
            Assert.AreEqual(0, records.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void DaysWithRecordsTest_ExceptionForYear()
        {
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");
            var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

            timetableService.DaysWithRecords(1899, 1);

            Assert.Fail();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void DaysWithRecordsTest_ExceptionForMonth()
        {
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");
            var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

            timetableService.DaysWithRecords(2017, 0);

            Assert.Fail();
        }

        [TestMethod]
        public void DaysWithRecordsTest()
        {
            var subdivisionService = new SubdivisionService();
            var subdivisions = subdivisionService.LoadListOfSubordiateSubdivisions("AVERKII-POPOV");
            var timetableService = new TimetableService(subdivisions.Select(x => x.ID));

            var records = timetableService.DaysWithRecords(2017, 1);
            Assert.IsFalse(records.Any(x => x));
        }
    }
}