﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Audits.Models.Checklists;
using Audits.Models.Database_Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Models.Checklists
{
    [TestClass]
    public class ChecklistServiceTest
    {
        private DatabaseEntities _db;

        [TestInitialize]
        public void Initialize()
        {
            _db = new DatabaseEntities();
        }

        [TestMethod]
        public void ChecklistCreation()
        {
            var checklist = GetTestChecklist();
            var id = ChecklistService.Create(checklist, _db);
            var dbChecklist = _db.Checklists.Find(id);
            Assert.IsTrue(dbChecklist != null && dbChecklist.Name.Equals(checklist.Name));
        }

        [TestMethod]
        public void ChecklistDeletion()
        {
            var id = ChecklistService.Create(GetTestChecklist(), _db);
            Assert.IsTrue(ChecklistService.Delete(id, _db));
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateException))]
        public void ParameterWithoutOrdinalException()
        {
            var checklist = new Checklist
            {
                Name = "Test Checklist",
                Sections = new List<Section>
                {
                    new Section
                    {
                        Name = "Section 1",
                        OrdinalNumber = 1,
                        Parameters = new List<Parameter>
                        {
                            new Parameter
                            {
                                Name = "Parameter 1",
                                MaxValue = 3,
                                SignificanceFactor = 4
                            }
                        }
                    }
                }
            };
            var id = ChecklistService.Create(checklist, _db);
        }

        [TestCleanup]
        public void Cleanup()
        {
            foreach (var checklist in _db.Checklists.Where(c => c.Name == "Test Checklist").ToList())
                ChecklistService.Delete(checklist.ID, _db);
        }

        private Checklist GetTestChecklist()
        {
            return new Checklist
            {
                Name = "Test Checklist",
                Sections = new List<Section>
                {
                    new Section
                    {
                        Name = "Section 1",
                        OrdinalNumber = 1,
                        Parameters = new List<Parameter>
                        {
                            new Parameter
                            {
                                Name = "Parameter 1",
                                OrdinalNumber = 1,
                                MaxValue = 3,
                                SignificanceFactor = 4
                            }
                        }
                    }
                }
            };
        }
    }
}
