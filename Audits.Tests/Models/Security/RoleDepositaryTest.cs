﻿using System.Linq;
using System.Security.Principal;
using Audits.Models.Database_Manager;
using Audits.Models.Security;
using Audits.Tests.Models.Database_Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Models.Security
{
    [TestClass]
    public class RoleDepositaryTest
    {
        [TestInitialize]
        public void Initialize()
        {
            EffortProviderFactory.ResetDb();
        }

        [TestMethod]
        public void HasPermission1()
        {
            // arrange
            var identity = WindowsIdentity.GetCurrent();
            RoleDepository.LoadRoles();

            // assert
            Assert.IsTrue(RoleDepository.HasPermission(identity, "ReadAudits"));
            Assert.IsTrue(RoleDepository.HasPermission(identity, "WriteAudits"));
            Assert.IsTrue(RoleDepository.HasPermission(identity, "ReadChecklist"));
            Assert.IsTrue(RoleDepository.HasPermission(identity, "WriteChecklist"));
            Assert.IsFalse(RoleDepository.HasPermission(identity, null));
            Assert.IsFalse(RoleDepository.HasPermission(null as WindowsIdentity, null));
            Assert.IsFalse(RoleDepository.HasPermission(null as WindowsIdentity, "ReadAudits"));
        }

        [TestMethod]
        public void HasPermission2()
        {
            // arrange
            using (var db = new DatabaseEntities())
            {
                var role = db.Roles.FirstOrDefault(r => r.ID == 1);
                RoleDepository.LoadRoles();
                if (role != null)
                {
                    // assert
                    Assert.IsTrue(RoleDepository.HasPermission(role, "ReadAudits"));
                    Assert.IsFalse(RoleDepository.HasPermission(role, "WriteAudits"));
                    Assert.IsFalse(RoleDepository.HasPermission(role, "ReadChecklist"));
                    Assert.IsFalse(RoleDepository.HasPermission(role, "WriteChecklist"));
                    Assert.IsFalse(RoleDepository.HasPermission(role, null));
                    Assert.IsFalse(RoleDepository.HasPermission(null as Role, null));
                    Assert.IsFalse(RoleDepository.HasPermission(null as Role, "ReadAudits"));
                }
            }
        }
    }
}