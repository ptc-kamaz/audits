﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Audits.Controllers;
using Audits.Models.Database_Manager;
using Audits.Models.EmployeesManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Audits.Tests.Models.EmployeesManager
{
    [TestClass]
    public class UserAdIdHelperTests
    {
        [TestMethod]
        public void GetUserAdIdTest()
        {
            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(WindowsIdentity.GetCurrent());

            var httpContextBase = new Mock<HttpContextBase>();
            httpContextBase.SetupGet(x => x.User).Returns(principal.Object);

            var userAdId = UserAdIdHelper.GetUserAdId(httpContextBase.Object);

            Assert.AreEqual(WindowsIdentity.GetCurrent().User.Value, userAdId);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetWindowsIdentityTest_NullWindowsIdentity()
        {
            var windowsIdentity = UserAdIdHelper.GetWindowsIdentity(null);

            Assert.AreEqual(WindowsIdentity.GetCurrent().User.Value, windowsIdentity.User.Value);
        }

        [TestMethod]
        [ExpectedException(typeof(NoNullAllowedException))]
        public void GetWindowsIdentityTest_NullUser()
        {
            var httpContextBase = new Mock<HttpContextBase>();
            httpContextBase.SetupGet(x => x.User).Returns(null as IPrincipal);

            var windowsIdentity = UserAdIdHelper.GetWindowsIdentity(httpContextBase.Object);

            Assert.AreEqual(WindowsIdentity.GetCurrent().User.Value, windowsIdentity.User.Value);
        }

        [TestMethod]
        public void GetWindowsIdentityTest()
        {
            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(WindowsIdentity.GetCurrent());

            var httpContextBase = new Mock<HttpContextBase>();
            httpContextBase.SetupGet(x => x.User).Returns(principal.Object);

            var windowsIdentity = UserAdIdHelper.GetWindowsIdentity(httpContextBase.Object);

            Assert.AreEqual(WindowsIdentity.GetCurrent().User.Value, windowsIdentity.User.Value);
        }
    }
}