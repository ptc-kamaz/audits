﻿using Audits.Models.Notifications;
using Audits.Tests.Models.Notifications;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Services
{
    [TestClass]
    public class MailServiceTest
    {
        private string ConfigPath = "App_Data/MailConfig.xml";

        private Mail GetTestMail(string To)
        {
            Mail mail = new Mail()
            {
                Subject = "Test Message from Kamaz",
                To = To,
                Body = "Test Message Body from "
            };
            return mail;
        }

        private Mail GetTestMailCC(string To)
        {
            Mail mail = GetTestMail(To);
            for (int i = 0; i < 2; i++)
            {
                mail.CC.Add("test@test.ru");
            }
            return mail;
        }

        [TestMethod]
        public void SendEmail()
        {
            ServerPathProvider server = new ServerPathProvider();
            MailService mailController = new MailService(server.MapPath(ConfigPath));
            bool result = mailController.Send(GetTestMail("msitse@outlook.com"));
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void SendMultipleEmail()
        {
            ServerPathProvider server = new ServerPathProvider();
            MailService mailController = new MailService(server.MapPath("App_Data/MailConfig.xml"));
            bool result = mailController.Send(GetTestMailCC("msitse@outlook.com"));
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CreateConfig()
        {
            MailConfig mailConfig = new MailConfig();
            mailConfig.Address = "projectkamaz@gmail.com";
            mailConfig.Host = "smtp.gmail.com";
            mailConfig.Password = "projectkamaz123.";
            mailConfig.UserName = "projectkamaz";
            mailConfig.Port = 587;
            ServerPathProvider server = new ServerPathProvider();
            MailService mailService = new MailService(server.MapPath("App_Data/MailConfig.xml"));
            Assert.IsTrue(mailService.SetConfig(mailConfig));
        }

        [TestMethod]
        public void LoadConfig()
        {
            ServerPathProvider server = new ServerPathProvider();
            MailService mailService = new MailService(server.MapPath("App_Data/MailConfig.xml"));
            Assert.AreEqual("projectkamaz", mailService.LoadConfig().UserName);
        }
    }
}
