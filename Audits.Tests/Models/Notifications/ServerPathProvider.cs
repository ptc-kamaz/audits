﻿using System;
using System.IO;
using System.Web;

namespace Audits.Tests.Models.Notifications
{
    public class ServerPathProvider
    {
        public string MapPath(string path)
        {
            return HttpContext.Current == null
                        ? Path.Combine(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).FullName).FullName, path)
                        : HttpContext.Current.Server.MapPath(path);
        }
    }
}
