﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Audits.Models.Database_Manager;
using Audits.Models.Reports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeOpenXml;

namespace Audits.Tests.Models.Reports
{
    [TestClass]
    public class ReportsTest
    {
        private static Audit CreateTestData()
        {
            var audit = new Audit();
            audit.ID = 1;

            audit.Auditor = new Employee
            {
                ID = 1
            };

            audit.Date = DateTime.Now;

            audit.Checklist = new Checklist();
            audit.Checklist.ID = 1;
            audit.Checklist.Name = "Чек-лист";
            audit.Checklist.Sections = new List<Section>();
            audit.Checklist.Sections.Add(new Section
            {
                ID = 1,
                Name = "Секция 1",
                Checklist = audit.Checklist,
                ChecklistID = audit.ChecklistID,
                OrdinalNumber = 1,
                Parameters = new List<Parameter>()
            });
            audit.Checklist.Sections.Add(new Section
            {
                ID = 2,
                Name = "Секция 2",
                Checklist = audit.Checklist,
                ChecklistID = audit.ChecklistID,
                OrdinalNumber = 2,
                Parameters = new List<Parameter>()
            });
            audit.Checklist.Sections.Add(new Section
            {
                ID = 3,
                Name = "Секция 3",
                Checklist = audit.Checklist,
                ChecklistID = audit.ChecklistID,
                OrdinalNumber = 3,
                Parameters = new List<Parameter>()
            });
            audit.Checklist.Sections.Add(new Section
            {
                ID = 4,
                Name = "Секция 4",
                Checklist = audit.Checklist,
                ChecklistID = audit.ChecklistID,
                OrdinalNumber = 4,
                Parameters = new List<Parameter>()
            });

            var parametrsId = 1;
            foreach (var section in audit.Checklist.Sections)
            {
                section.Parameters = new List<Parameter>
                {
                    new Parameter()
                    {
                        ID = parametrsId,
                        Name = $"Параметр {parametrsId}",
                        Section = section,
                        OrdinalNumber = parametrsId++,
                        SectionID = section.ID,
                        SignificanceFactor = 3,
                        MaxValue = 3
                    },
                    new Parameter
                    {
                        ID = parametrsId,
                        Name = $"Параметр {parametrsId}",
                        Section = section,
                        OrdinalNumber = parametrsId++,
                        SectionID = section.ID,
                        SignificanceFactor = 3,
                        MaxValue = 3
                    },
                    new Parameter
                    {
                        ID = parametrsId,
                        Name = $"Параметр {parametrsId}",
                        Section = section,
                        OrdinalNumber = parametrsId++,
                        SectionID = section.ID,
                        SignificanceFactor = 3,
                        MaxValue = 3
                    }
                };
            }

            audit.EvaluatedSections = new List<EvaluatedSection>();
            var sectionId = 1;
            foreach (var section in audit.Checklist.Sections)
            {
                var es = new EvaluatedSection
                {
                    ID = sectionId++,
                    Section = section,
                    SectionID = section.ID,
                    Audit = audit,
                    AuditID = audit.ID,
                    MaxGrade = section.Parameters.Sum(p => p.MaxValue),
                    EvaluatedParameters = new List<EvaluatedParameter>()
                };
                byte parameterValue = 1;
                foreach (var parameter in section.Parameters)
                {
                    es.EvaluatedParameters.Add(new EvaluatedParameter
                    {
                        ID = parameter.ID,
                        Value = parameterValue++,
                        Actions = parameter.Name,
                        Parameter = parameter,
                        EvaluatedSection = es,
                        EvaluatedSectionID = es.ID,
                        ParameterID = parameter.ID
                    });

                }
                audit.EvaluatedSections.Add(es);
            }

            audit.Subdivision = new Subdivision
            {
                ID = 1,
                Name = "Родительский цех",
                Audits = new List<Audit>(),
                Children = new List<Subdivision>(),
                Parent = null
            };
            audit.Subdivision.Children.Add(new Subdivision
            {
                ID = 2,
                Name = "Цех 1",
                Audits = new List<Audit> { audit },
                Children = new List<Subdivision>()
            });
            audit.Subdivision.Children.Add(new Subdivision
            {
                ID = 3,
                Name = "Цех 2",
                Audits = new List<Audit> { audit },
                Children = new List<Subdivision>()
            });

            audit.ValidatedAudit = new Audit();
            audit.ValidatedAudit.Date = audit.Date.AddDays(1);
            audit.ValidatedAudit.EvaluatedSections = new List<EvaluatedSection>();
            foreach (var auditEvaluatedSection in audit.EvaluatedSections)
            {
                var es = new EvaluatedSection
                {
                    Audit = auditEvaluatedSection.Audit,
                    AuditID = auditEvaluatedSection.AuditID,
                    Section = auditEvaluatedSection.Section,
                    MaxGrade = auditEvaluatedSection.MaxGrade,
                    SectionID = auditEvaluatedSection.SectionID
                };
                es.EvaluatedParameters = new List<EvaluatedParameter>();
                foreach (var epar in auditEvaluatedSection.EvaluatedParameters)
                {
                    es.EvaluatedParameters.Add(new EvaluatedParameter
                    {
                        EvaluatedSection = es,
                        Parameter = epar.Parameter,
                        Value = 1,
                    });
                }
                audit.ValidatedAudit.EvaluatedSections.Add(es);
            }

            return audit;
        }
        [TestMethod]
        public void TestCreateComplianceForSectionByMonthsReport()
        {
            var testData = CreateTestData();
            var fisrstEs = testData.EvaluatedSections.First();
            fisrstEs.EvaluatedParameters.Remove(fisrstEs.EvaluatedParameters.Last());
            var startDate = DateTime.Now.AddMonths(-1);
            var endDate = DateTime.Now;
            var bytes = new ReportCreator(testData.Subdivision, startDate, endDate,
                testData.Checklist.Sections.First(), "").CreateComplianceForSectionByMonthsReport();
            var package = new ExcelPackage(new MemoryStream(bytes));
            var worksheet =
                package.Workbook.Worksheets.First();
            var value = (double) worksheet.Cells[3, 5].Value;
            Assert.IsTrue(Math.Abs(value-0.666) < 0.001);
        }

        [TestMethod]
        public void TestCreateComplianceLevelForSectionReport()
        {
            var testData = CreateTestData();
            var fisrstEs = testData.EvaluatedSections.First();
            fisrstEs.EvaluatedParameters.Remove(fisrstEs.EvaluatedParameters.Last());

            var startDate = DateTime.Now.AddMonths(-1);
            var endDate = DateTime.Now;

            var bytes = new ReportCreator(testData.Subdivision, startDate, endDate,
                testData.Checklist.Sections.First(), "").CreateComplianceLevelForSectionReport();
            var package = new ExcelPackage(new MemoryStream(bytes));
            var worksheet =
                package.Workbook.Worksheets.First();

            var value = (double)worksheet.Cells[3, 9].Value;
            Assert.IsTrue(Math.Abs(value - 0.916) < 0.001);

            value = (double)worksheet.Cells[3, 5].Value;
            Assert.IsTrue(Math.Abs(value - 0.666) < 0.001);
        }

        [TestMethod]
        public void TestCreateComplianceForSubdivisionBarReport()
        {
            var testData = CreateTestData();
            var fisrstEs = testData.EvaluatedSections.First();
            fisrstEs.EvaluatedParameters.Remove(fisrstEs.EvaluatedParameters.Last());

            var startDate = DateTime.Now.AddMonths(-1);
            var endDate = DateTime.Now;

            var bytes = new ReportCreator(testData.Subdivision, startDate, endDate, testData.Subdivision.Children.First(), "").CreateComplianceForSubdivisionBarReport();
            var package = new ExcelPackage(new MemoryStream(bytes));
            var worksheet =
                package.Workbook.Worksheets.First();

            var value = (double)worksheet.Cells[3, 9].Value;
            Assert.IsTrue(Math.Abs(value - 0.916) < 0.001);

            value = (double)worksheet.Cells[3, 5].Value;
            Assert.IsTrue(Math.Abs(value - 0.666) < 0.001);
        }

        [TestMethod]
        public void TestCreateComplianceForSubdivisionRadarReport()
        {
            var testData = CreateTestData();
            var fisrstEs = testData.EvaluatedSections.First();
            fisrstEs.EvaluatedParameters.Remove(fisrstEs.EvaluatedParameters.Last());

            var startDate = DateTime.Now.AddMonths(-1);
            var endDate = DateTime.Now;

            var bytes = new ReportCreator(testData.Subdivision, startDate, endDate, testData.Subdivision.Children.First(), "").CreateComplianceForSubdivisionRadarReport();

            var package = new ExcelPackage(new MemoryStream(bytes));
            var worksheet =
                package.Workbook.Worksheets.First();

            var value = (double)worksheet.Cells[3, 9].Value;
            Assert.IsTrue(Math.Abs(value - 0.916) < 0.001);

            value = (double)worksheet.Cells[3, 5].Value;
            Assert.IsTrue(Math.Abs(value - 0.666) < 0.001);
        }

        [TestMethod]
        public void TestCreateValidationLevelReport()
        {
            var testData = CreateTestData();

            var startDate = DateTime.Now.AddMonths(-1);
            var endDate = DateTime.Now;

            var bytes = new ReportCreator(testData.Subdivision, startDate, endDate, "").CreateValidationLevelReport();
            var package = new ExcelPackage(new MemoryStream(bytes));
            var worksheet =
                package.Workbook.Worksheets.First();

            var value = (double)worksheet.Cells[3, 5].Value;
            Assert.IsTrue(Math.Abs(value - 0.5) < 0.001);
        }
    }
}
