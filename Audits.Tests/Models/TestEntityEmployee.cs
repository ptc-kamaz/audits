﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Audits.Tests.Models
{
    [Serializable]
    [XmlRoot("employee")]
    public class TestEntityEmployee
    {
        [XmlElement("id")]
        public int ID { get; set; }
        [Required]
        [XmlElement("firstname")]
        public string FirstName { get; set; }
        [Required]
        [XmlElement("lastname")]
        public string LastName { get; set; }
        [Required]
        [XmlElement("patronomyc")]
        public string Patronomyc { get; set; }
        [Required]
        [XmlElement("subdivison")]
        public int Subdivision { get; set; }
        [Required]
        [XmlElement("role")]
        public int Role { get; set; }
        [Required]
        [XmlElement("establishid")]
        public string EstablishedPositionID { get; set; }
        [Required]
        [XmlElement("establishposition")]
        public string EstablishedPosition { get; set; }
        [Required]
        [XmlElement("actualposition")]
        public string ActualPosition { get; set; }
        [Required]
        [XmlElement("email")]
        public string Email { get; set; }
        [Required]
        [XmlElement("phone")]
        public string Phone { get; set; }
        [Required]
        [XmlElement("privateid")]
        public string PrivateID { get; set; }
        [Required]
        [XmlElement("ad_id")]
        public string AD_ID { get; set; }
    }
}
