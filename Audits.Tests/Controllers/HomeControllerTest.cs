﻿using System.Web.Mvc;
using Audits.Controllers;
using Audits.Tests.Models.Database_Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestInitialize]
        public void Initialize()
        {
            EffortProviderFactory.ResetDb();
        }


        [TestMethod]
        public void About()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }
    }
}