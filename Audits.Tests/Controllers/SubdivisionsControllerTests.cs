﻿using System.Collections;
using System.Linq;
using System.Web.Mvc;
using Audits.Controllers;
using Audits.Models.Database_Manager;
using Audits.Models.Subdivisions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Controllers
{
    [TestClass]
    public class SubdivisionsControllerTests
    {
        [TestMethod]
        public void IndexTest()
        {
            using (var controller = new SubdivisionsController())
            {
                controller.Index();

                Assert.IsFalse(controller.ViewData.Any());
            }
        }

        [TestMethod]
        public void JsonTreeTest()
        {
            using (var controller = new SubdivisionsController())
            {
                var result = controller.JsonTree();
                var jsonResult = result as JsonResult;

                Assert.AreEqual(typeof(JsonResult), result.GetType());
                Assert.AreEqual(JsonRequestBehavior.AllowGet, jsonResult.JsonRequestBehavior);
                Assert.AreEqual(typeof(TreeNode<Subdivision>), jsonResult.Data.GetType());
            }
        }

        [TestMethod]
        public void JsonSubdivisionEmployeesTest()
        {
            const int subdivisionId = 2;
            using (var controller = new SubdivisionsController())
            {
                var result = controller.JsonSubdivisionEmployees(subdivisionId);
                var jsonResult = result as JsonResult;
                var collection = jsonResult?.Data as IEnumerable;
                var enumerator = collection?.GetEnumerator();
                enumerator?.MoveNext();
                var current = enumerator?.Current;
                var properties = current?
                    .GetType()
                    .GetProperties();

                Assert.AreEqual(typeof(JsonResult), result.GetType());
                Assert.AreEqual(JsonRequestBehavior.AllowGet, jsonResult?.JsonRequestBehavior);
                Assert.AreEqual(6, properties?.Length);
                Assert.IsTrue(properties != null && properties.Any(p => p.Name.Equals("ID")));
                Assert.IsTrue(properties.Any(p => p.Name.Equals("LastName")));
                Assert.IsTrue(properties.Any(p => p.Name.Equals("FirstName")));
                Assert.IsTrue(properties.Any(p => p.Name.Equals("Patronymic")));
                Assert.IsTrue(properties.Any(p => p.Name.Equals("SubdivisionID")));
                Assert.IsTrue(properties.Any(p => p.Name.Equals("Email")));
            }
        }
    }
}