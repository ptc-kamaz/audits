﻿using System.Web.Mvc;
using Audits.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Audits.Tests.Controllers
{
    [TestClass]
    public class AuditsControllerTest
    {
        [TestMethod]
        public void TestRightGetDetails()
        {
            var auditsController = new AuditsController();
            var result = auditsController.Details(1) as ViewResult;
            Assert.AreEqual("", result?.ViewName);
        }

        [TestMethod]
        public void TestWrongGetDetails()
        {
            var auditsController = new AuditsController();
            var result = auditsController.Details(null) as ViewResult;
            Assert.AreEqual("Error", result?.ViewName);
        }

        [TestMethod]
        public void TestGetValidateExisted()
        {
            var auditsController = new AuditsController();
            var result = auditsController.Validate(1) as ViewResult;
            Assert.AreEqual("", result?.ViewName);
        }

        [TestMethod]
        public void TestGetValidateUnexisted()
        {
            var auditsController = new AuditsController();
            var result = auditsController.Validate(-1) as HttpNotFoundResult;
            Assert.AreEqual(404, result?.StatusCode);
        }
    }
}