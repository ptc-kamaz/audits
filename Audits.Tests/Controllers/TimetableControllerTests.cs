﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using Audits.Controllers;
using Audits.Models.Database_Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Audits.Tests.Controllers
{
    [TestClass]
    public class TimetableControllerTests
    {
        [TestMethod]
        public void IndexTest()
        {
            var httpRequest = new HttpRequest("", "http://url", "query string");
            var httpResponse = new HttpResponse(new StringWriter(new StringBuilder("")));
            var httpContext = new HttpContext(httpRequest, httpResponse);
            var httpContextBase = new Mock<HttpContextWrapper>(httpContext);
            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(WindowsIdentity.GetCurrent());
            httpContextBase.SetupGet(x => x.User).Returns(principal.Object);
            
            var controller = new TimetableController();
            controller.ControllerContext = new ControllerContext(httpContextBase.Object, new RouteData(), controller);

            var actionResult = controller.Index() as ViewResult;
            var timetableRecords = actionResult.Model as IEnumerable<TimetableRecord>;

            controller.Dispose();

            Assert.IsNotNull(timetableRecords);
            Assert.AreEqual(0, timetableRecords.Count());
        }

        [TestMethod]
        public void GetDayValuesTest()
        {
            var httpRequest = new HttpRequest("", "http://url", "query string");
            var httpResponse = new HttpResponse(new StringWriter(new StringBuilder("")));
            var httpContext = new HttpContext(httpRequest, httpResponse);
            var httpContextBase = new Mock<HttpContextWrapper>(httpContext);
            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(WindowsIdentity.GetCurrent());
            httpContextBase.SetupGet(x => x.User).Returns(principal.Object);

            var controller = new TimetableController();
            controller.ControllerContext = new ControllerContext(httpContextBase.Object, new RouteData(), controller);

            var actionResult = controller.GetDayValues(2000, 1) as JsonResult;
            var bools = actionResult.Data as bool[];

            controller.Dispose();
            
            Assert.AreEqual(31, bools.Length);
            Assert.IsFalse(bools.Any(x => x));
        }

        [TestMethod]
        public void DetailsTest_BadRequest()
        {
            var httpRequest = new HttpRequest("", "http://url", "query string");
            var httpResponse = new HttpResponse(new StringWriter(new StringBuilder("")));
            var httpContext = new HttpContext(httpRequest, httpResponse);
            var httpContextBase = new Mock<HttpContextWrapper>(httpContext);
            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(WindowsIdentity.GetCurrent());
            httpContextBase.SetupGet(x => x.User).Returns(principal.Object);

            var controller = new TimetableController();
            controller.ControllerContext = new ControllerContext(httpContextBase.Object, new RouteData(), controller);

            var actionResult = controller.Details(2000, 1, null) as PartialViewResult;
            var enumerable = actionResult.Model as IEnumerable<TimetableRecord>;

            controller.Dispose();

            Assert.IsNotNull(enumerable);
            Assert.AreEqual(0, enumerable.Count());
        }

        [TestMethod]
        public void DetailsTest()
        {
            var httpRequest = new HttpRequest("", "http://url", "query string");
            var httpResponse = new HttpResponse(new StringWriter(new StringBuilder("")));
            var httpContext = new HttpContext(httpRequest, httpResponse);
            var httpContextBase = new Mock<HttpContextWrapper>(httpContext);
            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(WindowsIdentity.GetCurrent());
            httpContextBase.SetupGet(x => x.User).Returns(principal.Object);

            var controller = new TimetableController();
            controller.ControllerContext = new ControllerContext(httpContextBase.Object, new RouteData(), controller);

            var actionResult = controller.Details(2000, 1, 1) as PartialViewResult;
            var timetableRecords = actionResult.Model as TimetableRecord[];

            controller.Dispose();

            Assert.IsNotNull(timetableRecords);
            Assert.AreEqual(0, timetableRecords.Length);
        }
    }
}