﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Xml.Serialization;
using Audits.Controllers;
using Audits.Models.Database_Manager;
using Audits.Models.Security;
using Audits.Tests.Models;
using Audits.Tests.Models.Database_Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Audits.Tests.Controllers
{
    [TestClass]
    public class EmployeesControllerTest
    {
        private readonly By actualPosition = By.XPath("//input[@id='ActualPosition']");
        private readonly By ad_id = By.XPath("//input[@id='sid-text-editor']");
        private readonly By button_create = By.XPath("//a[@href='/Employees/Create']");
        private readonly By email = By.XPath("//input[@id='Email']");
        private readonly By establishedPsition = By.XPath("//input[@id='EstablishedPosition']");
        private readonly By firstName = By.XPath("//input[@id='FirstName']");
        private readonly By lastName = By.XPath("//input[@id='LastName']");
        private readonly By patr = By.XPath("//input[@id='Patronymic']");
        private readonly By phone = By.XPath("//input[@id='Phone']");
        private readonly By positionID = By.XPath("//input[@id='EstablishedPositionID']");
        private readonly By privateID = By.XPath("//input[@id='PersonnelNumber']");
        private readonly By role = By.XPath("//select[@id='Role']");

        private readonly By save = By.XPath("//input[@value='Сохранить']");
        private readonly By subdivisionID = By.XPath("//select[@id='SubdivisionID']");

        private int id;

        private TestEntityEmployee testEmployee;

        [TestInitialize]
        public void Initialize()
        {
            EffortProviderFactory.ResetDb();
            var configPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName,
                ConfigurationManager.AppSettings["TestData"]);
            using (var fileStream = new FileStream(configPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                //XmlRootAttribute xRoot = new XmlRootAttribute();
                //xRoot.ElementName = "activedirectory";
                //xRoot.IsNullable = true;
                var xml = new XmlSerializer(typeof(TestEntityEmployee));
                testEmployee = (TestEntityEmployee) xml.Deserialize(fileStream);
                fileStream.Close();
                fileStream.Dispose();
            }
        }

        private void SerializingTestEmployee()
        {
            var configPath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName,
                ConfigurationManager.AppSettings["TestData"]);
            using (var fileStream = new FileStream(configPath, FileMode.Open, FileAccess.ReadWrite,
                FileShare.ReadWrite))
            {
                //XmlRootAttribute xRoot = new XmlRootAttribute();
                //xRoot.ElementName = "activedirectory";
                //xRoot.IsNullable = true;
                var xml = new XmlSerializer(typeof(TestEntityEmployee));
                xml.Serialize(fileStream, testEmployee);
                //testEmployee = (TestEntityEmployee)xml.Deserialize(fileStream);
                fileStream.Close();
                fileStream.Dispose();
            }
        }

        /// <summary>
        ///     Method tests the
        /// </summary>
        [TestMethod]
        public void Index()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = "http://localhost:80/Employees/Index";
            Assert.AreEqual(true, driver.FindElement(By.ClassName("table_employees")).Enabled);
            driver.Quit();
        }

        [TestMethod]
        public void EmployeeControllerTest1_Create()
        {
            var identity = WindowsIdentity.GetCurrent();
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            if (RoleDepository.HasPermission(identity, "EditUsers"))
            {
                driver.Url = "http://localhost/Employees/Index";
                driver.FindElement(button_create).Click();
                Assert.AreEqual("http://localhost/Employees/Create", driver.Url);

                driver.FindElement(firstName).SendKeys(testEmployee.FirstName);
                driver.FindElement(lastName).SendKeys(testEmployee.LastName);
                driver.FindElement(patr).SendKeys(testEmployee.Patronomyc);
                var subdivisionSelect = new SelectElement(driver.FindElement(subdivisionID));
                subdivisionSelect.SelectByIndex(testEmployee.Subdivision);
                driver.FindElement(positionID).SendKeys(testEmployee.EstablishedPositionID);
                var roleSelect = new SelectElement(driver.FindElement(role));
                roleSelect.SelectByIndex(testEmployee.Role);
                driver.FindElement(establishedPsition).SendKeys(testEmployee.EstablishedPosition);
                driver.FindElement(actualPosition).SendKeys(testEmployee.ActualPosition);
                driver.FindElement(email).SendKeys(testEmployee.Email);
                driver.FindElement(phone).SendKeys(testEmployee.Phone);
                driver.FindElement(privateID).SendKeys(testEmployee.PrivateID);
                driver.FindElement(ad_id).SendKeys(testEmployee.AD_ID);

                driver.FindElement(save).Click();

                Thread.Sleep(2000);
                using (var db = new DatabaseEntities())
                {
                    var er = db.Employees.Where(e => e.FirstName.Equals(testEmployee.FirstName) &&
                                                     e.LastName.Equals(testEmployee.LastName)
                                                     && e.Patronymic.Equals(testEmployee.Patronomyc) &&
                                                     e.PersonnelNumber.Equals(testEmployee.PrivateID)
                                                     && e.Phone.Equals(testEmployee.Phone) &&
                                                     e.EstablishedPosition.Equals(testEmployee.EstablishedPosition)
                                                     && e.AD_ID.Equals(testEmployee.AD_ID)
                    );
                    id = er.First().ID;
                    testEmployee.ID = id;
                    SerializingTestEmployee();
                    Assert.AreEqual(er.Count(), 1);
                }
                Assert.AreEqual("http://localhost/Employees", driver.Url);
            }
            driver.Close();
        }

        /// <summary>
        ///     Method tests the link to edit page if user has a EditUsers permission.
        ///     After clicking on link it compares the title.
        /// </summary>
        [TestMethod]
        public void EmployeeControllerTest2_Edit()
        {
            var identity = WindowsIdentity.GetCurrent();
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            if (RoleDepository.HasPermission(identity, "EditUsers"))
            {
                using (var db = new DatabaseEntities())
                {
                    var count = db.Employees.Count();
                    var page = count / EmployeesController.PageSize + 1;
                    driver.Url = "http://localhost/Employees/Index?page=" + page;
                    Thread.Sleep(2000);
                    var button = driver.FindElement(By.XPath("//a[@href='/Employees/Edit/" + testEmployee.ID + "']"));
                    button.Click();
                    Assert.AreEqual(driver.FindElement(firstName).GetAttribute("value"), testEmployee.FirstName);
                    Assert.AreEqual(driver.FindElement(lastName).GetAttribute("value"), testEmployee.LastName);
                    Assert.AreEqual(driver.FindElement(patr).GetAttribute("value"), testEmployee.Patronomyc);
                    Assert.AreEqual(driver.FindElement(phone).GetAttribute("value"), testEmployee.Phone);
                    Assert.AreEqual(driver.FindElement(positionID).GetAttribute("value"),
                        testEmployee.EstablishedPositionID);
                    Assert.AreEqual(driver.FindElement(establishedPsition).GetAttribute("value"),
                        testEmployee.EstablishedPosition);
                    Assert.AreEqual(driver.FindElement(email).GetAttribute("value"), testEmployee.Email);
                    Assert.AreEqual(driver.FindElement(ad_id).GetAttribute("value"), testEmployee.AD_ID);
                    Assert.AreEqual(driver.FindElement(actualPosition).GetAttribute("value"),
                        testEmployee.ActualPosition);

                    var newValue = "Директор Завода";
                    driver.FindElement(actualPosition).Clear();
                    driver.FindElement(actualPosition).SendKeys(newValue);
                    driver.FindElement(save).Click();
                    Thread.Sleep(1000);
                    var empl = db.Employees.Find(testEmployee.ID);
                    Assert.AreEqual(empl.ActualPosition, newValue);
                }
            }
            else
            {
                var links = driver.FindElements(By.TagName("a")).ToList();
                if (links.Any(x => x.Text.Equals("Редактировать")))
                    Assert.Fail();
            }
            driver.Close();
        }

        [TestMethod]
        public void EmployeeControllerTest3_Delete()
        {
            var identity = WindowsIdentity.GetCurrent();
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            if (RoleDepository.HasPermission(identity, "EditUsers"))
            {
                using (var db = new DatabaseEntities())
                {
                    var count = db.Employees.Count();
                    var page = count / EmployeesController.PageSize + 1;
                    driver.Url = "http://localhost/Employees/Index?page=" + page;
                    Thread.Sleep(2000);
                    var button = driver.FindElement(By.XPath("//a[@href='/Employees/Delete/" + testEmployee.ID + "']"));
                    button.Click();
                    Thread.Sleep(500);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[1]")).Text,
                        testEmployee.LastName);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[2]")).Text,
                        testEmployee.FirstName);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[3]")).Text,
                        testEmployee.Patronomyc);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[4]")).Text,
                        testEmployee.EstablishedPositionID);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[5]")).Text,
                        testEmployee.EstablishedPosition);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[7]")).Text,
                        testEmployee.Email);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[8]")).Text,
                        testEmployee.Phone);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[9]")).Text,
                        testEmployee.PrivateID);
                    Assert.AreEqual(driver.FindElement(By.XPath("//dl[@class='dl-horizontal']/dd[10]")).Text,
                        testEmployee.AD_ID);

                    driver.FindElement(By.XPath("//input[@value='Удалить']")).Click();
                    Thread.Sleep(2000);
                    var empl = db.Employees.Find(testEmployee.ID);
                    Assert.AreEqual(null, empl);
                }
            }
            else
            {
                var links = driver.FindElements(By.TagName("a")).ToList();
                if (links.Any(x => x.Text.Equals("Удалить")))
                    Assert.Fail();
            }
            driver.Close();
        }
    }
}