USE [АСА]
GO
SET IDENTITY_INSERT [dbo].[тРоль] ON 

INSERT [dbo].[тРоль] ([КодРоли], [Роль]) VALUES (10, N'Аудитор')
INSERT [dbo].[тРоль] ([КодРоли], [Роль]) VALUES (11, N'Доверенный аудитор')
INSERT [dbo].[тРоль] ([КодРоли], [Роль]) VALUES (12, N'Аудитор КРПС')
INSERT [dbo].[тРоль] ([КодРоли], [Роль]) VALUES (13, N'Администратор КРПС')
INSERT [dbo].[тРоль] ([КодРоли], [Роль]) VALUES (14, N'Пользователь')
SET IDENTITY_INSERT [dbo].[тРоль] OFF
