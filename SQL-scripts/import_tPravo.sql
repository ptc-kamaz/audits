USE [АСА]
GO
SET IDENTITY_INSERT [dbo].[тПраво] ON 

INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (1, N'ReadAudits')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (2, N'WriteAudits')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (3, N'ReadChecklist')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (4, N'WriteChecklist')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (11, N'EditUsers')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (12, N'ViewUsers')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (13, N'ReadStructure')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (14, N'WriteStructure')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (15, N'WriteEmailConfig')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (17, N'ReadTimetable')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (18, N'WriteTimetable')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (19, N'ReadAuditsTrusted')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (20, N'ReadAuditsKrps')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (21, N'ValidateAuditTrusted')
INSERT [dbo].[тПраво] ([КодПрава], [Право]) VALUES (22, N'ValidateAuditKrps')
SET IDENTITY_INSERT [dbo].[тПраво] OFF
