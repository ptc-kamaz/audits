USE [master]
GO
/****** Object:  Database [АСА]    Script Date: 07.08.2017 20:24:16 ******/
CREATE DATABASE [АСА]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'АСА', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\АСА.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'АСА_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\АСА_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [АСА] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [АСА].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [АСА] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [АСА] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [АСА] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [АСА] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [АСА] SET ARITHABORT OFF 
GO
ALTER DATABASE [АСА] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [АСА] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [АСА] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [АСА] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [АСА] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [АСА] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [АСА] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [АСА] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [АСА] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [АСА] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [АСА] SET  DISABLE_BROKER 
GO
ALTER DATABASE [АСА] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [АСА] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [АСА] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [АСА] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [АСА] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [АСА] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [АСА] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [АСА] SET RECOVERY FULL 
GO
ALTER DATABASE [АСА] SET  MULTI_USER 
GO
ALTER DATABASE [АСА] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [АСА] SET DB_CHAINING OFF 
GO
ALTER DATABASE [АСА] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [АСА] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [АСА]
GO
/****** Object:  Table [dbo].[тЗначениеПарамПров]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тЗначениеПарамПров](
	[Значение] [tinyint] NOT NULL,
	[КодРезультатаСекции] [int] NOT NULL,
	[КодПараметрПроверки] [int] NOT NULL,
	[КодЗначениеПарамПров] [int] IDENTITY(1,1) NOT NULL,
	[Мероприятия] [varchar](500) NOT NULL,
 CONSTRAINT [XPKтЗначениеПарамПров] PRIMARY KEY CLUSTERED 
(
	[КодЗначениеПарамПров] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тПараметрПроверки]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тПараметрПроверки](
	[КодПараметрПроверки] [int] IDENTITY(1,1) NOT NULL,
	[Вопрос] [varchar](250) NOT NULL,
	[НПП] [int] NOT NULL,
	[КодСекции] [int] NOT NULL,
	[МаксЗначение] [tinyint] NOT NULL,
	[КоэфЗначимости] [tinyint] NOT NULL,
 CONSTRAINT [XPKтПараметрПроверки] PRIMARY KEY CLUSTERED 
(
	[КодПараметрПроверки] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тПодразделение]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тПодразделение](
	[КодПодразделения] [int] IDENTITY(1,1) NOT NULL,
	[КодРодителя] [int] NULL,
	[Наименование] [varchar](40) NOT NULL,
	[КодРуководителя] [int] NULL,
	[КодSAP] [varchar](12) NULL,
 CONSTRAINT [XPKтПодразделение] PRIMARY KEY CLUSTERED 
(
	[КодПодразделения] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тПраваРоли]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[тПраваРоли](
	[КодРоли] [int] NOT NULL,
	[КодПрава] [int] NOT NULL,
 CONSTRAINT [XPKтПраваРоли] PRIMARY KEY CLUSTERED 
(
	[КодРоли] ASC,
	[КодПрава] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[тПраво]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тПраво](
	[КодПрава] [int] IDENTITY(1,1) NOT NULL,
	[Право] [varchar](80) NOT NULL,
 CONSTRAINT [XPKтПраво] PRIMARY KEY CLUSTERED 
(
	[КодПрава] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тРаздел]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тРаздел](
	[КодСекции] [int] IDENTITY(1,1) NOT NULL,
	[Название] [varchar](100) NOT NULL,
	[НПП] [int] NOT NULL,
	[КодЧекЛиста] [int] NOT NULL,
 CONSTRAINT [XPKтРаздел] PRIMARY KEY CLUSTERED 
(
	[КодСекции] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тРезультатРаздела]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[тРезультатРаздела](
	[КодРезультатаСекции] [int] IDENTITY(1,1) NOT NULL,
	[КодЧекЛистаАудита] [int] NOT NULL,
	[Балл] [int] NOT NULL,
	[МаксБалл] [int] NOT NULL,
	[КодСекции] [int] NULL,
 CONSTRAINT [XPKтРезультатРаздела] PRIMARY KEY CLUSTERED 
(
	[КодРезультатаСекции] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[тРолиСотрудника]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[тРолиСотрудника](
	[КодСотрудника] [int] NOT NULL,
	[КодРоли] [int] NOT NULL,
 CONSTRAINT [XPKтРолиСотрудника] PRIMARY KEY CLUSTERED 
(
	[КодСотрудника] ASC,
	[КодРоли] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[тРоль]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тРоль](
	[КодРоли] [int] IDENTITY(1,1) NOT NULL,
	[Роль] [varchar](20) NOT NULL,
 CONSTRAINT [XPKтРоль] PRIMARY KEY CLUSTERED 
(
	[КодРоли] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тСотрудник]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тСотрудник](
	[КодСотрудника] [int] IDENTITY(1,1) NOT NULL,
	[Фамилия] [varchar](40) NOT NULL,
	[Имя] [varchar](40) NOT NULL,
	[Отчество] [varchar](40) NOT NULL,
	[КодПодразделения] [int] NOT NULL,
	[КодШтатнойДолжности] [int] NULL,
	[ШтатнаяДолжность] [varchar](40) NULL,
	[ФактическаяДолжность] [varchar](40) NULL,
	[Email] [varchar](241) NOT NULL,
	[Телефон] [varchar](20) NULL,
	[ТабНомер] [char](8) NULL,
	[AD_ID] [varchar](256) NOT NULL,
 CONSTRAINT [XPKтСотрудник] PRIMARY KEY CLUSTERED 
(
	[КодСотрудника] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тФото]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тФото](
	[КодФото] [int] IDENTITY(1,1) NOT NULL,
	[Расположение] [varchar](150) NOT NULL,
	[КодРезультатаСекции] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[КодФото] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тЧекЛист]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тЧекЛист](
	[КодЧекЛиста] [int] IDENTITY(1,1) NOT NULL,
	[Название] [varchar](80) NOT NULL,
 CONSTRAINT [XPKтЧекЛист] PRIMARY KEY CLUSTERED 
(
	[КодЧекЛиста] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тЧекЛистАудита]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[тЧекЛистАудита](
	[КодЧекЛиста] [int] NOT NULL,
	[ДатаФактПроведения] [datetime] NOT NULL,
	[КодЧекЛистаАудита] [int] IDENTITY(1,1) NOT NULL,
	[ДатаВвода] [datetime] NOT NULL,
	[КодАудитора] [int] NOT NULL,
	[КодПодразделения] [int] NOT NULL,
	[Редактируемый] [bit] NOT NULL,
	[КодПодтверждаемогоАудита] [int] NULL,
	[КодЧекЛистаРасписания] [int] NULL,
	[Сопровождающие] [varchar](350) NULL,
 CONSTRAINT [XPKтЧекЛистыАудита] PRIMARY KEY CLUSTERED 
(
	[КодЧекЛистаАудита] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[тЧекЛистыРасписания]    Script Date: 07.08.2017 20:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[тЧекЛистыРасписания](
	[КодЧекЛиста] [int] NOT NULL,
	[КодЧекЛистаРасписания] [int] IDENTITY(1,1) NOT NULL,
	[КодПодразделения] [int] NOT NULL,
	[Дата] [date] NOT NULL,
 CONSTRAINT [XPKтЧекЛистыРасписания] PRIMARY KEY CLUSTERED 
(
	[КодЧекЛистаРасписания] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_AD_ID]    Script Date: 07.08.2017 20:24:16 ******/
CREATE NONCLUSTERED INDEX [IDX_AD_ID] ON [dbo].[тСотрудник]
(
	[AD_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NonClusteredUniqueIndex-AD_ID]    Script Date: 07.08.2017 20:24:16 ******/
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredUniqueIndex-AD_ID] ON [dbo].[тСотрудник]
(
	[AD_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_Дата]    Script Date: 07.08.2017 20:24:16 ******/
CREATE NONCLUSTERED INDEX [IDX_Дата] ON [dbo].[тЧекЛистыРасписания]
(
	[Дата] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_КодПодразделения]    Script Date: 07.08.2017 20:24:16 ******/
CREATE NONCLUSTERED INDEX [IDX_КодПодразделения] ON [dbo].[тЧекЛистыРасписания]
(
	[КодПодразделения] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[тЗначениеПарамПров]  WITH CHECK ADD  CONSTRAINT [R_76] FOREIGN KEY([КодРезультатаСекции])
REFERENCES [dbo].[тРезультатРаздела] ([КодРезультатаСекции])
GO
ALTER TABLE [dbo].[тЗначениеПарамПров] CHECK CONSTRAINT [R_76]
GO
ALTER TABLE [dbo].[тЗначениеПарамПров]  WITH CHECK ADD  CONSTRAINT [R_77] FOREIGN KEY([КодПараметрПроверки])
REFERENCES [dbo].[тПараметрПроверки] ([КодПараметрПроверки])
GO
ALTER TABLE [dbo].[тЗначениеПарамПров] CHECK CONSTRAINT [R_77]
GO
ALTER TABLE [dbo].[тПараметрПроверки]  WITH CHECK ADD  CONSTRAINT [R_31] FOREIGN KEY([КодСекции])
REFERENCES [dbo].[тРаздел] ([КодСекции])
GO
ALTER TABLE [dbo].[тПараметрПроверки] CHECK CONSTRAINT [R_31]
GO
ALTER TABLE [dbo].[тПодразделение]  WITH CHECK ADD  CONSTRAINT [R_2] FOREIGN KEY([КодРуководителя])
REFERENCES [dbo].[тСотрудник] ([КодСотрудника])
GO
ALTER TABLE [dbo].[тПодразделение] CHECK CONSTRAINT [R_2]
GO
ALTER TABLE [dbo].[тПодразделение]  WITH CHECK ADD  CONSTRAINT [R_22] FOREIGN KEY([КодРодителя])
REFERENCES [dbo].[тПодразделение] ([КодПодразделения])
GO
ALTER TABLE [dbo].[тПодразделение] CHECK CONSTRAINT [R_22]
GO
ALTER TABLE [dbo].[тПраваРоли]  WITH CHECK ADD  CONSTRAINT [R_60] FOREIGN KEY([КодРоли])
REFERENCES [dbo].[тРоль] ([КодРоли])
GO
ALTER TABLE [dbo].[тПраваРоли] CHECK CONSTRAINT [R_60]
GO
ALTER TABLE [dbo].[тПраваРоли]  WITH CHECK ADD  CONSTRAINT [R_61] FOREIGN KEY([КодПрава])
REFERENCES [dbo].[тПраво] ([КодПрава])
GO
ALTER TABLE [dbo].[тПраваРоли] CHECK CONSTRAINT [R_61]
GO
ALTER TABLE [dbo].[тРаздел]  WITH CHECK ADD  CONSTRAINT [R_30] FOREIGN KEY([КодЧекЛиста])
REFERENCES [dbo].[тЧекЛист] ([КодЧекЛиста])
GO
ALTER TABLE [dbo].[тРаздел] CHECK CONSTRAINT [R_30]
GO
ALTER TABLE [dbo].[тРезультатРаздела]  WITH CHECK ADD  CONSTRAINT [R_75] FOREIGN KEY([КодЧекЛистаАудита])
REFERENCES [dbo].[тЧекЛистАудита] ([КодЧекЛистаАудита])
GO
ALTER TABLE [dbo].[тРезультатРаздела] CHECK CONSTRAINT [R_75]
GO
ALTER TABLE [dbo].[тРезультатРаздела]  WITH CHECK ADD  CONSTRAINT [R_78] FOREIGN KEY([КодСекции])
REFERENCES [dbo].[тРаздел] ([КодСекции])
GO
ALTER TABLE [dbo].[тРезультатРаздела] CHECK CONSTRAINT [R_78]
GO
ALTER TABLE [dbo].[тРолиСотрудника]  WITH CHECK ADD  CONSTRAINT [R_58] FOREIGN KEY([КодСотрудника])
REFERENCES [dbo].[тСотрудник] ([КодСотрудника])
GO
ALTER TABLE [dbo].[тРолиСотрудника] CHECK CONSTRAINT [R_58]
GO
ALTER TABLE [dbo].[тРолиСотрудника]  WITH CHECK ADD  CONSTRAINT [R_59] FOREIGN KEY([КодРоли])
REFERENCES [dbo].[тРоль] ([КодРоли])
GO
ALTER TABLE [dbo].[тРолиСотрудника] CHECK CONSTRAINT [R_59]
GO
ALTER TABLE [dbo].[тСотрудник]  WITH CHECK ADD  CONSTRAINT [R_3] FOREIGN KEY([КодПодразделения])
REFERENCES [dbo].[тПодразделение] ([КодПодразделения])
GO
ALTER TABLE [dbo].[тСотрудник] CHECK CONSTRAINT [R_3]
GO
ALTER TABLE [dbo].[тФото]  WITH CHECK ADD FOREIGN KEY([КодРезультатаСекции])
REFERENCES [dbo].[тРезультатРаздела] ([КодРезультатаСекции])
GO
ALTER TABLE [dbo].[тЧекЛистАудита]  WITH CHECK ADD  CONSTRAINT [FK_тЧекЛистАудита_тСотрудник] FOREIGN KEY([КодАудитора])
REFERENCES [dbo].[тСотрудник] ([КодСотрудника])
GO
ALTER TABLE [dbo].[тЧекЛистАудита] CHECK CONSTRAINT [FK_тЧекЛистАудита_тСотрудник]
GO
ALTER TABLE [dbo].[тЧекЛистАудита]  WITH CHECK ADD  CONSTRAINT [FK_тЧекЛистАудита_тЧекЛистыРасписания] FOREIGN KEY([КодЧекЛистаРасписания])
REFERENCES [dbo].[тЧекЛистыРасписания] ([КодЧекЛистаРасписания])
GO
ALTER TABLE [dbo].[тЧекЛистАудита] CHECK CONSTRAINT [FK_тЧекЛистАудита_тЧекЛистыРасписания]
GO
ALTER TABLE [dbo].[тЧекЛистАудита]  WITH CHECK ADD  CONSTRAINT [R_36] FOREIGN KEY([КодЧекЛиста])
REFERENCES [dbo].[тЧекЛист] ([КодЧекЛиста])
GO
ALTER TABLE [dbo].[тЧекЛистАудита] CHECK CONSTRAINT [R_36]
GO
ALTER TABLE [dbo].[тЧекЛистАудита]  WITH CHECK ADD  CONSTRAINT [R_71] FOREIGN KEY([КодПодразделения])
REFERENCES [dbo].[тПодразделение] ([КодПодразделения])
GO
ALTER TABLE [dbo].[тЧекЛистАудита] CHECK CONSTRAINT [R_71]
GO
ALTER TABLE [dbo].[тЧекЛистАудита]  WITH CHECK ADD  CONSTRAINT [R_81] FOREIGN KEY([КодПодтверждаемогоАудита])
REFERENCES [dbo].[тЧекЛистАудита] ([КодЧекЛистаАудита])
GO
ALTER TABLE [dbo].[тЧекЛистАудита] CHECK CONSTRAINT [R_81]
GO
ALTER TABLE [dbo].[тЧекЛистыРасписания]  WITH CHECK ADD  CONSTRAINT [FK_тЧекЛистыРасписания_тПодразделение] FOREIGN KEY([КодПодразделения])
REFERENCES [dbo].[тПодразделение] ([КодПодразделения])
GO
ALTER TABLE [dbo].[тЧекЛистыРасписания] CHECK CONSTRAINT [FK_тЧекЛистыРасписания_тПодразделение]
GO
ALTER TABLE [dbo].[тЧекЛистыРасписания]  WITH CHECK ADD  CONSTRAINT [FK_тЧекЛистыРасписания_тЧекЛист] FOREIGN KEY([КодЧекЛиста])
REFERENCES [dbo].[тЧекЛист] ([КодЧекЛиста])
GO
ALTER TABLE [dbo].[тЧекЛистыРасписания] CHECK CONSTRAINT [FK_тЧекЛистыРасписания_тЧекЛист]
GO
ALTER TABLE [dbo].[тЗначениеПарамПров]  WITH CHECK ADD  CONSTRAINT [VR_GE_0_1909864953] CHECK  (([Значение]>=(0)))
GO
ALTER TABLE [dbo].[тЗначениеПарамПров] CHECK CONSTRAINT [VR_GE_0_1909864953]
GO
ALTER TABLE [dbo].[тПараметрПроверки]  WITH CHECK ADD  CONSTRAINT [VR_GT_0_1499982078] CHECK  (([МаксЗначение]>(0)))
GO
ALTER TABLE [dbo].[тПараметрПроверки] CHECK CONSTRAINT [VR_GT_0_1499982078]
GO
ALTER TABLE [dbo].[тПараметрПроверки]  WITH CHECK ADD  CONSTRAINT [VR_GT_0_1573445520] CHECK  (([КоэфЗначимости]>(0)))
GO
ALTER TABLE [dbo].[тПараметрПроверки] CHECK CONSTRAINT [VR_GT_0_1573445520]
GO
ALTER TABLE [dbo].[тПараметрПроверки]  WITH CHECK ADD  CONSTRAINT [VR_GT_0_717557241] CHECK  (([НПП]>(0)))
GO
ALTER TABLE [dbo].[тПараметрПроверки] CHECK CONSTRAINT [VR_GT_0_717557241]
GO
USE [master]
GO
ALTER DATABASE [АСА] SET  READ_WRITE 
GO
